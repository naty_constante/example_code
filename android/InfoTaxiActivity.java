package app.taxi_android.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import app.taxi_android.R;
import app.taxi_android.data.RideData;
import app.taxi_android.data.TaxiAssociatedData;
import app.taxi_android.models.Ride;
import app.taxi_android.models.TaxiAssociated;
import app.taxi_android.util.AndroidMultiPartEntity;
import app.taxi_android.util.GPSTracker;
import app.taxi_android.util.Util;

public class InfoTaxiActivity extends ActionBarActivity {

    private String current_ride_id;
    private Button button_cancel_ride;
    private Button button_next;
    //info taxi
    private TextView lbl_content_name_associated;
    private TextView lbl_content_brand;
    private TextView lbl_content_model;
    private TextView lbl_content_color;
    private TextView lbl_content_company_name;
    private TextView lbl_content_plate;
    private TextView lbl_content_registration_number;

    //variables send to server
    private ProgressBar progressBar;
    private TextView txtPercentage;
    long totalSize = 0;
    private ProgressDialog progDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_taxi);

        current_ride_id = getIntent().getStringExtra("current_ride_id");

        loadTaxiInfo();

        button_cancel_ride = (Button) findViewById(R.id.button_cancel_ride);
        button_next = (Button) findViewById(R.id.button_next);
        progDialog = new ProgressDialog(this, ProgressDialog.THEME_TRADITIONAL);

        //send to server
        txtPercentage = (TextView) findViewById(R.id.txtPercentage);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        button_cancel_ride.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                cancelFinalSurveyRide(InfoTaxiActivity.this);
            }
        });

        button_next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent intent = new Intent(InfoTaxiActivity.this, SurveyActivity.class);
                intent.putExtra("current_ride_id", current_ride_id);
                startActivity(intent);
            }
        });

    }

    private void cancelFinalSurveyRide(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, AlertDialog.THEME_TRADITIONAL);
        AlertDialog alert;

        builder.setTitle(R.string.app_name);
        builder.setMessage(Html.fromHtml(getResources().getString(R.string.message_cancel_ride)))
                .setCancelable(true)
                .setPositiveButton(getResources().getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                RideData ride_data = new RideData(getApplication().getApplicationContext());
                                Ride obj_ride = new Ride();

                                obj_ride = ride_data.getRide(current_ride_id);

                                obj_ride.setStatus("CANCELLED");
                                ride_data.save(obj_ride);
                                if (Util.hasInternetConnectivity(InfoTaxiActivity.this)) {
                                    saveInServerCancelledSurveys();
                                } else {
                                    ActivityCompat.finishAffinity(InfoTaxiActivity.this);
                                    Intent intent = new Intent(InfoTaxiActivity.this, MainActivity.class);
                                    intent.putExtra("message", getResources().getString(R.string.message_cancelled_ride));
                                    startActivity(intent);
                                    finish();
                                }
                                dialog.cancel();


                            }
                        });
        builder.setNegativeButton(getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        // finish used for destroyed activity
                    }
                });
        alert = builder.create();
        alert.show();

    }

    private void loadTaxiInfo() {
        lbl_content_name_associated = (TextView) findViewById(R.id.lbl_content_name_associated);
        lbl_content_brand = (TextView) findViewById(R.id.lbl_content_brand);
        lbl_content_model = (TextView) findViewById(R.id.lbl_content_model);
        lbl_content_color = (TextView) findViewById(R.id.lbl_content_color);
        lbl_content_company_name = (TextView) findViewById(R.id.lbl_content_company_name);
        lbl_content_plate = (TextView) findViewById(R.id.lbl_content_plate);
        lbl_content_registration_number = (TextView) findViewById(R.id.lbl_content_registration_number);

        RideData ride_data = new RideData(this);
        Ride obj_ride = new Ride();
        obj_ride = ride_data.getRide(current_ride_id);

        //load info taxi from database
        TaxiAssociatedData taxi_associated_data = new TaxiAssociatedData(this);
        TaxiAssociated taxi_associated_obj = new TaxiAssociated();
        taxi_associated_obj = taxi_associated_data.getTaxiAssociated(obj_ride.getTaxi_associated_id());

        if (taxi_associated_obj.getAsociated() != null)
            lbl_content_name_associated.setText(taxi_associated_obj.getAsociated());
        if (taxi_associated_obj.getBrand() != null)
            lbl_content_brand.setText(taxi_associated_obj.getBrand());
        if (taxi_associated_obj.getModel() != null)
            lbl_content_model.setText(taxi_associated_obj.getModel());
        if (taxi_associated_obj.getColor() != null)
            lbl_content_color.setText(taxi_associated_obj.getColor());
        if (taxi_associated_obj.getCompany_name() != null)
            lbl_content_company_name.setText(taxi_associated_obj.getCompany_name());
        if (taxi_associated_obj.getPlate() != null)
            lbl_content_plate.setText(taxi_associated_obj.getPlate());
        if (taxi_associated_obj.getRegistry_number() != null)
            lbl_content_registration_number.setText(taxi_associated_obj.getRegistry_number());
    }


    //save in server
    private void saveInServerCancelledSurveys() {

        //  Util.showProgressDialog(progDialog, "Detectando ...");

        progDialog = ProgressDialog.show(this,
                getResources().getString(R.string.loading_save_cancel_ride_title),
                getResources().getString(R.string.loading_save_cancel_ride),
                true);
        new ReadSaveCancelledSurveyJSONFeedTask().execute();

    }

    private class ReadSaveCancelledSurveyJSONFeedTask extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            progressBar.setProgress(0);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Making progress bar visible
            progressBar.setVisibility(View.VISIBLE);

            // updating progress bar value
            progressBar.setProgress(progress[0]);

            // updating percentage value
            txtPercentage.setText(String.valueOf(progress[0]) + "%");
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                return uploadWorkReport();
            } catch (Exception ex) {
                Util.logException(ex);
            }
            return null;
        }

        @SuppressWarnings("deprecation")
        private String uploadWorkReport() {
            String responseString;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(getResources().getString(R.string.url_web_services) + "ws/taxi/save");

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });


                Charset chars = Charset.forName("UTF-8");

                //reported problems
                RideData rides_data = new RideData(getApplication().getApplicationContext());
                Ride inf_ride = new Ride();
                inf_ride = rides_data.getRide(current_ride_id);


                entity.addPart("start_date", new StringBody(inf_ride.getStart_date(), chars));
                if (inf_ride.getFinish_date() != null)
                    entity.addPart("finish_date", new StringBody(inf_ride.getFinish_date(), chars));
                entity.addPart("start_location", new StringBody(inf_ride.getStart_location(), chars));
                if (inf_ride.getFinish_location() != null)
                    entity.addPart("finish_location", new StringBody(inf_ride.getFinish_location(), chars));
                if (inf_ride.getQuality() != null)
                    entity.addPart("quality", new StringBody(inf_ride.getQuality(), chars));
                if (inf_ride.getTaximeter() != null)
                    entity.addPart("taximeter", new StringBody(inf_ride.getTaximeter(), chars));
                if (inf_ride.getStatus() != null)
                    entity.addPart("status", new StringBody(inf_ride.getStatus(), chars));
                if (inf_ride.getTaxi_id() != null)
                    entity.addPart("taxi_id", new StringBody(inf_ride.getTaxi_id(), chars));


                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        protected void onPostExecute(String result) {

            String message = "";
            String status_transaction = "";
            progDialog.dismiss();
            try {
                JSONObject response = new JSONObject(result);
                message = response.getString("message");
                status_transaction = response.getString("status_transaction");

                if (status_transaction.equals("SUCCESS")) {
                    /*
                        DELETE RIDES
                     */
                    TaxiAssociatedData taxi_associated_data = new TaxiAssociatedData(getApplication().getApplicationContext());
                    RideData ride_data = new RideData(getApplication().getApplicationContext());
                    Ride ride_o = new Ride();
                    ride_o = ride_data.getRide(current_ride_id);
                    //delete taxi assoc
                    taxi_associated_data.deleteByKey(ride_o.getTaxi_associated_id());
                    //delete ride
                    ride_data.deleteByKey(current_ride_id);

                    ActivityCompat.finishAffinity(InfoTaxiActivity.this);
                    Intent intent = new Intent(InfoTaxiActivity.this, MainActivity.class);
                    intent.putExtra("message", message);
                    startActivity(intent);
                    finish();

                } else {

                    if (status_transaction.equals(" FAILED_TAXI_ID")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(InfoTaxiActivity.this, AlertDialog.THEME_TRADITIONAL);
                        AlertDialog alert;

                        builder.setTitle(R.string.app_name);
                        builder.setMessage(Html.fromHtml(message))
                                .setCancelable(false)
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                ActivityCompat.finishAffinity(InfoTaxiActivity.this);
                                                Intent intent = new Intent(InfoTaxiActivity.this, MainActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        });
                        alert = builder.create();
                        alert.show();
                    }else{
                        Util.showAlert(InfoTaxiActivity.this, "Ok", getResources().getString(R.string.cancel_ride));
                        progressBar.setVisibility(View.GONE);
                        txtPercentage.setText("");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Util.showAlert(InfoTaxiActivity.this, "Ok", getResources().getString(R.string.cancel_ride));
            }


            super.onPostExecute(result);

        }
    }


}
