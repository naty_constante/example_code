package app.fatosla.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import app.fatosla.R;
import app.fatosla.models.FieldsRevisionControlPoint;

public class WorRepRevisionControlPointActivity extends ActionBarActivity {

    final static String ID_0 = "id0";
    final static String NEW_CONTROL_POINT_LIST = "newControlPointList";

    private String id0 = null;
    private ArrayList<FieldsRevisionControlPoint> newControlPointList = null;

    public String getId0() {
        return id0;
    }

    public void setId0(String id0) {
        this.id0 = id0;
    }

    public ArrayList<FieldsRevisionControlPoint> getNewControlPointList() {
        return newControlPointList;
    }

    public void setNewControlPointList(ArrayList<FieldsRevisionControlPoint> newControlPointList) {
        this.newControlPointList = newControlPointList;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        if (id0 != null) {
            savedInstanceState.putString(ID_0, id0);
        }
        if(newControlPointList != null){
            savedInstanceState.putParcelableArrayList(NEW_CONTROL_POINT_LIST, newControlPointList);
        }
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_report);

        if (savedInstanceState != null) {
            id0 = savedInstanceState.getString(ID_0);
            // Restore value of members from saved state
            newControlPointList = savedInstanceState.getParcelableArrayList(NEW_CONTROL_POINT_LIST);
        }

        // Set a toolbar to replace the action bar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            WorRepRevisionControlPointActivityFragment worRepRevisionControlPointActivityFragment = WorRepRevisionControlPointActivityFragment.newInstance(getIntent().getExtras().getString("service_request_id"));
            ft.replace(R.id.container_wor_rep_basic_data, worRepRevisionControlPointActivityFragment);
            ft.commit();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(WorRepRevisionControlPointActivity.this, WorkReportHomeActivity.class);
        intent.putExtra("service_request_id", getIntent().getExtras().getString("service_request_id"));
        intent.putExtra("message", "");
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
        startActivity(intent);
        this.finish();
    }

}