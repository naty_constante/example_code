package app.fatosla.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import app.fatosla.models.ControlPointServiceRequest;
import app.fatosla.util.FatoslaDataBase;

/**
 * Created by Naty on 5/22/2015.
 */
public class ControlPointServiceRequestData extends FatoslaDataBase {

    public ControlPointServiceRequestData(Context context) {
        super(context);
    }

    /*
    * search control point services by key
     */
    public ControlPointServiceRequest getControlPointServiceRequest(String searchByControlPointServiceRequestKey) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery(
                "SELECT * FROM control_point_service_request WHERE id = '"
                        + searchByControlPointServiceRequestKey + "'", null);

        if (c.getCount() > 0) {
            c.moveToFirst();
            ControlPointServiceRequest control_point_service_request = new ControlPointServiceRequest();
            control_point_service_request.setId(c.getString(0));
            control_point_service_request.setControl_point_id(c.getString(1));
            control_point_service_request.setService_request_id(c.getString(2));
            control_point_service_request.setUtil_life(c.getString(3));
            control_point_service_request.setComment(c.getString(4));
            control_point_service_request.setPhoto(c.getString(5));

            db.close();
            c.close();

            return control_point_service_request;
        } else {
            return null;
        }
    }

    /*
    * save control point object in DB
    */
    public void save(ControlPointServiceRequest control_point_service_request) {
        ContentValues values = new ContentValues();
        values.put("id", control_point_service_request.getId());
        values.put("control_point_id", control_point_service_request.getControl_point_id());
        values.put("service_request_id", control_point_service_request.getService_request_id());
        values.put("util_life", control_point_service_request.getUtil_life());
        values.put("comment", control_point_service_request.getComment());
        values.put("photo", control_point_service_request.getPhoto());

        ControlPointServiceRequest searchControlPointServiceRequest = this.getControlPointServiceRequest(control_point_service_request.getId());

        SQLiteDatabase db = getReadableDatabase();

        if (db != null) {

            if (searchControlPointServiceRequest == null) {
                db.insert("control_point_service_request", null, values);
            } else {
                db.update("control_point_service_request", values, "id=" + control_point_service_request.getId(), null);
            }

            db.close();
        }
    }

    /*
    * delete all control points
     */
    public void deleteAll() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete("control_point_service_request", null, null);
        db.close();
    }

    /*
     delete control points by key
       */
    public void deleteByServiceRequest(String service_request_id) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete("control_point_service_request", "service_request_id=?", new String[]{""
                + service_request_id});
        db.close();
    }

    /*
      Get a list all control points
     */
    public ArrayList<ControlPointServiceRequest> getsControlPointsByServiceRequest(String service_request_id) {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<ControlPointServiceRequest> srhControlPoint = new ArrayList<ControlPointServiceRequest>();

        String query = "SELECT control_point_service_request.* FROM control_point_service_request";
        query += " WHERE service_request_id = '" + service_request_id;
        query += "' order by control_point_id asc";

        Log.e("controlpointquery", query);
        Cursor c = db.rawQuery(query, null);

        if (c.getCount() > 0) {

            c.moveToFirst();
            do {
                ControlPointServiceRequest control_point_service_request = new ControlPointServiceRequest();
                control_point_service_request.setId(c.getString(0));
                control_point_service_request.setControl_point_id(c.getString(1));
                control_point_service_request.setService_request_id(c.getString(2));
                control_point_service_request.setUtil_life(c.getString(3));
                control_point_service_request.setComment(c.getString(4));
                control_point_service_request.setPhoto(c.getString(5));

                srhControlPoint.add(control_point_service_request);

            } while (c.moveToNext());

            db.close();
            c.close();

            return srhControlPoint;
        } else {
            return null;
        }

    }
}
