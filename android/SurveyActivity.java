package app.taxi_android.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import app.taxi_android.R;
import app.taxi_android.data.RideData;
import app.taxi_android.data.TaxiAssociatedData;
import app.taxi_android.models.Ride;
import app.taxi_android.util.AndroidMultiPartEntity;
import app.taxi_android.util.GPSTracker;
import app.taxi_android.util.Util;

public class SurveyActivity extends ActionBarActivity {

    private Button btnFinishRide;

    private GPSTracker gps;
    private ProgressDialog progDialog;
    private String current_ride_id;
    private Button button_cancel_ride_survey;
    private LinearLayout ly_return;
    private TextView finish_location;

    private Button btn_good;
    private Button btn_regular;
    private Button btn_bad;

    private Button btn_taximeter_yes;
    private Button btn_taximeter_no;
    private String[] quality_values = {"GOOD", "REGULAR", "BAD"};
    private String[] taximeter_values = {"YES", "NO"};

    private TextView hdn_quality;
    private TextView hdn_taximeter;

    //variables send to server
    private ProgressBar progressBar;
    private TextView txtPercentage;
    long totalSize = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);

        current_ride_id = getIntent().getStringExtra("current_ride_id");
        Log.e(" id n SurveyA", current_ride_id);
        button_cancel_ride_survey = (Button) findViewById(R.id.button_cancel_ride_survey);
        btnFinishRide = (Button) findViewById(R.id.button_finish_ride);
        finish_location = (TextView) findViewById(R.id.hdn_finish_ride);
        ly_return = (LinearLayout) findViewById(R.id.ly_return);

        //btn quality
        btn_good = (Button) findViewById(R.id.btn_good);
        btn_regular = (Button) findViewById(R.id.btn_regular);
        btn_bad = (Button) findViewById(R.id.btn_bad);
        hdn_quality = (TextView) findViewById(R.id.hdn_quality);

        //btn taximeter
        btn_taximeter_yes = (Button) findViewById(R.id.btn_taximeter_yes);
        btn_taximeter_no = (Button) findViewById(R.id.btn_taximeter_no);
        hdn_taximeter = (TextView) findViewById(R.id.hdn_taximeter);

        //send to server
        txtPercentage = (TextView) findViewById(R.id.txtPercentage);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        ly_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        button_cancel_ride_survey.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                cancelFinalSurveyRide(SurveyActivity.this);
            }
        });

        btnFinishRide.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Boolean validation_survey = true;
                getlocation(SurveyActivity.this);

                if (!finish_location.getText().toString().isEmpty()) {

                    //validation quality
                    if (hdn_quality.getText().toString().isEmpty()) {
                        validation_survey = false;
                        Util.showAlert(SurveyActivity.this, "Ok", getResources().getString(R.string.error_get_quality));
                    } else {
                        //validation taximeter
                        if (hdn_taximeter.getText().toString().isEmpty()) {
                            validation_survey = false;
                            Util.showAlert(SurveyActivity.this, "Ok", getResources().getString(R.string.error_get_taximeter));
                        }
                    }

                    //send to save in database and web service
                    if (validation_survey) {
                        saveFinishRideData(SurveyActivity.this);

                    }
                } else {
                    Util.showAlert(SurveyActivity.this, "Ok", getResources().getString(R.string.error_get_location));
                }
            }
        });

        btn_good.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                hdn_quality.setText(quality_values[0]);
                btn_bad.setBackgroundResource(R.drawable.buttons_bad);
                btn_regular.setBackgroundResource(R.drawable.buttons_regular);
                btn_good.setBackgroundResource(R.drawable.buttons_good_selected);

            }
        });

        btn_regular.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                hdn_quality.setText(quality_values[1]);
                btn_bad.setBackgroundResource(R.drawable.buttons_bad);
                btn_regular.setBackgroundResource(R.drawable.buttons_regular_selected);
                btn_good.setBackgroundResource(R.drawable.buttons_good);
            }
        });

        btn_bad.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                hdn_quality.setText(quality_values[2]);
                btn_bad.setBackgroundResource(R.drawable.buttons_bad_selected);
                btn_regular.setBackgroundResource(R.drawable.buttons_regular);
                btn_good.setBackgroundResource(R.drawable.buttons_good);
            }
        });

        btn_taximeter_yes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                hdn_taximeter.setText(taximeter_values[0]);
                btn_taximeter_no.setBackgroundResource(R.drawable.buttons_bad);
                btn_taximeter_yes.setBackgroundResource(R.drawable.buttons_good_selected);
            }
        });

        btn_taximeter_no.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                hdn_taximeter.setText(taximeter_values[1]);
                btn_taximeter_no.setBackgroundResource(R.drawable.buttons_bad_selected);
                btn_taximeter_yes.setBackgroundResource(R.drawable.buttons_good);
            }
        });

    }


    public void getlocation(Context context) {

        gps = new GPSTracker(context);
        progDialog = new ProgressDialog(context, ProgressDialog.THEME_TRADITIONAL);
        Util.showProgressDialog(progDialog, "Detectando ...");
        Util.updateLocation(gps, finish_location, progDialog);


    }


    private void saveFinishRideData(Context context) {
        RideData ride_data = new RideData(context);
        Ride obj_ride = new Ride();

        Calendar c = Calendar.getInstance();
        c.getTime();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String finish_date = format1.format(c.getTime());

        obj_ride = ride_data.getRide(current_ride_id);
        Log.e(" update id infotaxiA", current_ride_id);

        obj_ride.setFinish_date(finish_date);
        obj_ride.setQuality(hdn_quality.getText().toString());
        Log.e(" update quality", hdn_quality.getText().toString());
        obj_ride.setTaximeter(hdn_taximeter.getText().toString());
        Log.e(" update taximeter", hdn_taximeter.getText().toString());
        obj_ride.setStatus("COMPLETED");
        obj_ride.setFinish_location(finish_location.getText().toString());
        ride_data.save(obj_ride);

        Log.e("finish location", obj_ride.getFinish_location());

        Log.e("internet", String.valueOf(Util.hasInternetConnectivity(this)));
        //check internet conectivity to save in server
        if (Util.hasInternetConnectivity(this)) {
            saveInServerSurveys();
        } else {
            ActivityCompat.finishAffinity(SurveyActivity.this);
            Intent intent = new Intent(SurveyActivity.this, MainActivity.class);
            intent.putExtra("message", getResources().getString(R.string.message_save_database_survey));
            startActivity(intent);
            finish();
        }
    }

    private void cancelFinalSurveyRide(Context context) {


        AlertDialog.Builder builder = new AlertDialog.Builder(context, AlertDialog.THEME_TRADITIONAL);
        AlertDialog alert;

        builder.setTitle(R.string.app_name);
        builder.setCancelable(true)
                .setPositiveButton(getResources().getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                RideData ride_data = new RideData(getApplication().getApplicationContext());
                                Ride obj_ride = new Ride();

                                obj_ride = ride_data.getRide(current_ride_id);
                                Log.e("update cancel surveyA", current_ride_id);

                                obj_ride.setStatus("CANCELLED");
                                ride_data.save(obj_ride);
                                if (Util.hasInternetConnectivity(SurveyActivity.this)) {
                                    saveInServerCancelledSurveys();
                                } else {
                                    Log.e("dd", String.valueOf(Util.hasInternetConnectivity(SurveyActivity.this)));
                                    ActivityCompat.finishAffinity(SurveyActivity.this);
                                    Intent intent = new Intent(SurveyActivity.this, MainActivity.class);
                                    intent.putExtra("message", getResources().getString(R.string.message_cancelled_ride));
                                    startActivity(intent);
                                    finish();
                                }
                                dialog.cancel();

                            }
                        });
        builder.setNegativeButton(getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        // finish used for destroyed activity
                    }
                });

        builder.setMessage(Html.fromHtml(getResources().getString(R.string.message_cancel_ride)));
        alert = builder.create();
        alert.show();

    }

    //save in server
    private void saveInServerSurveys() {

        progDialog = ProgressDialog.show(this,
                getResources().getString(R.string.loading_save_server_survey_title),
                getResources().getString(R.string.loading_save_server_survey),
                true);
        new ReadSaveSurveyJSONFeedTask().execute();

    }

    private class ReadSaveSurveyJSONFeedTask extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            progressBar.setProgress(0);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Making progress bar visible
            progressBar.setVisibility(View.VISIBLE);

            // updating progress bar value
            progressBar.setProgress(progress[0]);

            // updating percentage value
            txtPercentage.setText(String.valueOf(progress[0]) + "%");
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                return uploadWorkReport();
            } catch (Exception ex) {
                Util.logException(ex);
            }
            return null;
        }

        @SuppressWarnings("deprecation")
        private String uploadWorkReport() {
            String responseString;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(getResources().getString(R.string.url_web_services) + "ws/taxi/save");

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });


                Charset chars = Charset.forName("UTF-8");

                //reported problems
                RideData rides_data = new RideData(getApplication().getApplicationContext());
                Ride inf_ride = new Ride();

                inf_ride = rides_data.getRide(current_ride_id);


                entity.addPart("start_date", new StringBody(inf_ride.getStart_date(), chars));
                if (inf_ride.getFinish_date() != null)
                    entity.addPart("finish_date", new StringBody(inf_ride.getFinish_date(), chars));
                entity.addPart("start_location", new StringBody(inf_ride.getStart_location(), chars));
                if (inf_ride.getFinish_location() != null)
                    entity.addPart("finish_location", new StringBody(inf_ride.getFinish_location(), chars));
                if (inf_ride.getQuality() != null)
                    entity.addPart("quality", new StringBody(inf_ride.getQuality(), chars));
                if (inf_ride.getTaximeter() != null)
                    entity.addPart("taximeter", new StringBody(inf_ride.getTaximeter(), chars));
                if (inf_ride.getStatus() != null)
                    entity.addPart("status", new StringBody(inf_ride.getStatus(), chars));
                if (inf_ride.getTaxi_id() != null)
                    entity.addPart("taxi_id", new StringBody(inf_ride.getTaxi_id(), chars));


                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        protected void onPostExecute(String result) {
            Log.e("TAG", "Response from server: " + result);

            String message = "";
            String status_transaction = "";
            progDialog.dismiss();
            try {
                JSONObject response = new JSONObject(result);
                message = response.getString("message");
                status_transaction = response.getString("status_transaction");

                if (status_transaction.equals("SUCCESS")) {

                    /*
                        DELETE RIDES
                     */
                    TaxiAssociatedData taxi_associated_data = new TaxiAssociatedData(getApplication().getApplicationContext());
                    RideData ride_data = new RideData(getApplication().getApplicationContext());
                    Ride ride_o = new Ride();
                    ride_o = ride_data.getRide(current_ride_id);
                    //delete taxi assoc
                    taxi_associated_data.deleteByKey(ride_o.getTaxi_associated_id());
                    //delete ride
                    ride_data.deleteByKey(current_ride_id);
                    ActivityCompat.finishAffinity(SurveyActivity.this);
                    Intent intent = new Intent(SurveyActivity.this, MainActivity.class);
                    intent.putExtra("message", message);
                    startActivity(intent);
                    finish();

                } else {

                    if (status_transaction.equals(" FAILED_TAXI_ID")) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(SurveyActivity.this, AlertDialog.THEME_TRADITIONAL);
                        AlertDialog alert;

                        builder.setTitle(R.string.app_name);
                        builder.setMessage(Html.fromHtml(message))
                                .setCancelable(false)
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                ActivityCompat.finishAffinity(SurveyActivity.this);
                                                Intent intent = new Intent(SurveyActivity.this, MainActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        });
                        alert = builder.create();
                        alert.show();
                    } else {
                        Util.showAlert(SurveyActivity.this, "Ok", getResources().getString(R.string.cancel_ride));
                        progressBar.setVisibility(View.GONE);
                        txtPercentage.setText("");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Util.showAlert(SurveyActivity.this, "Ok", getResources().getString(R.string.invalid_send_ride));
            }

            super.onPostExecute(result);

        }
    }


    //save in server
    private void saveInServerCancelledSurveys() {

        progDialog = ProgressDialog.show(this,
                getResources().getString(R.string.loading_save_cancel_ride_title),
                getResources().getString(R.string.loading_save_cancel_ride),
                true);
        new ReadSaveCancelledSurveyJSONFeedTask().execute();

    }

    private class ReadSaveCancelledSurveyJSONFeedTask extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            progressBar.setProgress(0);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Making progress bar visible
            progressBar.setVisibility(View.VISIBLE);

            // updating progress bar value
            progressBar.setProgress(progress[0]);

            // updating percentage value
            txtPercentage.setText(String.valueOf(progress[0]) + "%");
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                return uploadWorkReport();
            } catch (Exception ex) {
                Util.logException(ex);
            }
            return null;
        }

        @SuppressWarnings("deprecation")
        private String uploadWorkReport() {
            String responseString;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(getResources().getString(R.string.url_web_services) + "ws/taxi/save");

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });


                Charset chars = Charset.forName("UTF-8");

                //reported problems
                RideData rides_data = new RideData(getApplication().getApplicationContext());
                Ride inf_ride = new Ride();
                inf_ride = rides_data.getRide(current_ride_id);


                entity.addPart("start_date", new StringBody(inf_ride.getStart_date(), chars));
                if (inf_ride.getFinish_date() != null)
                    entity.addPart("finish_date", new StringBody(inf_ride.getFinish_date(), chars));
                entity.addPart("start_location", new StringBody(inf_ride.getStart_location(), chars));
                if (inf_ride.getFinish_location() != null)
                    entity.addPart("finish_location", new StringBody(inf_ride.getFinish_location(), chars));
                if (inf_ride.getQuality() != null)
                    entity.addPart("quality", new StringBody(inf_ride.getQuality(), chars));
                if (inf_ride.getTaximeter() != null)
                    entity.addPart("taximeter", new StringBody(inf_ride.getTaximeter(), chars));
                if (inf_ride.getStatus() != null)
                    entity.addPart("status", new StringBody(inf_ride.getStatus(), chars));
                if (inf_ride.getTaxi_id() != null)
                    entity.addPart("taxi_id", new StringBody(inf_ride.getTaxi_id(), chars));


                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        protected void onPostExecute(String result) {
            Log.e("TAG", "Response from server: " + result);

            String message = "";
            String status_transaction = "";
            progDialog.dismiss();
            try {
                JSONObject response = new JSONObject(result);
                message = response.getString("message");
                status_transaction = response.getString("status_transaction");

                if (status_transaction.equals("SUCCESS")) {

                    /*
                        DELETE RIDES
                     */
                    TaxiAssociatedData taxi_associated_data = new TaxiAssociatedData(getApplication().getApplicationContext());
                    RideData ride_data = new RideData(getApplication().getApplicationContext());
                    Ride ride_o = new Ride();
                    ride_o = ride_data.getRide(current_ride_id);
                    //delete taxi assoc
                    taxi_associated_data.deleteByKey(ride_o.getTaxi_associated_id());
                    //delete ride
                    ride_data.deleteByKey(current_ride_id);

                    ActivityCompat.finishAffinity(SurveyActivity.this);
                    Intent intent = new Intent(SurveyActivity.this, MainActivity.class);
                    intent.putExtra("message", message);
                    startActivity(intent);
                    finish();

                } else {

                    if (status_transaction.equals(" FAILED_TAXI_ID")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(SurveyActivity.this, AlertDialog.THEME_TRADITIONAL);
                        AlertDialog alert;

                        builder.setTitle(R.string.app_name);
                        builder.setMessage(Html.fromHtml(message))
                                .setCancelable(false)
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                ActivityCompat.finishAffinity(SurveyActivity.this);
                                                Intent intent = new Intent(SurveyActivity.this, MainActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        });
                        alert = builder.create();
                        alert.show();
                    } else {
                        Util.showAlert(SurveyActivity.this, "Ok", getResources().getString(R.string.cancel_ride));
                        progressBar.setVisibility(View.GONE);
                        txtPercentage.setText("");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Util.showAlert(SurveyActivity.this, "Ok", getResources().getString(R.string.cancel_ride));
            }

            super.onPostExecute(result);

        }
    }


}
