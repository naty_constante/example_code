package app.fatosla.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import app.fatosla.R;
import app.fatosla.data.ControlPointData;
import app.fatosla.data.ControlPointServiceRequestData;
import app.fatosla.data.ValidationServiceRequestData;
import app.fatosla.models.ControlPoint;
import app.fatosla.models.ControlPointServiceRequest;
import app.fatosla.models.FieldsRevisionControlPoint;
import app.fatosla.models.Suggestion;
import app.fatosla.util.Util;

/**
 * A placeholder fragment containing a simple view.
 */
public class WorRepRevisionControlPointActivityFragment extends Fragment {

    private LinearLayout LinearLayoutControlPoints;
    private String service_request_id;
    private ControlPointData control_point_data;
    private ArrayList<ControlPoint> inf_control_point;
    private ArrayList<String> spinnerPercentage;
    private String current_button_control_point;
    private Button button_save_revision_control_points;
    private Button button_save_finish_revision_control_points;
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    //object to save all data in form
    private ArrayList<FieldsRevisionControlPoint> field_control_point_list;
    private FieldsRevisionControlPoint field_control_point_obj;
    private WorRepRevisionControlPointActivity parentActivity;

    private ImageView ivImage;

    public WorRepRevisionControlPointActivityFragment() {
    }

    /**
     * Static factory method
     *
     * @param serviceRequestId
     * @return
     */
    public static WorRepRevisionControlPointActivityFragment newInstance(String serviceRequestId) {
        WorRepRevisionControlPointActivityFragment fragment = new WorRepRevisionControlPointActivityFragment();
        Bundle args = new Bundle();
        args.putString("service_request_id", serviceRequestId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_wor_rep_revision_control_point, container, false);
        button_save_revision_control_points = (Button) rootView.findViewById(R.id.button_save_revision_control_points);
        button_save_finish_revision_control_points = (Button) rootView.findViewById(R.id.button_save_finish_revision_control_points);
        service_request_id = getArguments().getString("service_request_id");
        parentActivity = (WorRepRevisionControlPointActivity) getActivity();

        //initialice control point arraylist
        field_control_point_list = new ArrayList<FieldsRevisionControlPoint>();

        if (parentActivity.getNewControlPointList() != null)
            Log.e("parent array on create", String.valueOf(parentActivity.getNewControlPointList().size()));


        if (parentActivity.getNewControlPointList() != null) {
            Log.e("parent fiels load", parentActivity.getNewControlPointList().toString());

            ArrayList<FieldsRevisionControlPoint> fieldcontrolPointList = parentActivity.getNewControlPointList();
            loadControlPoint(rootView, inflater, service_request_id, fieldcontrolPointList);
        } else {
            loadControlPoint(rootView, inflater, service_request_id, null);
        }
        //save control point  on click listener
        button_save_finish_revision_control_points.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Boolean validation_correct = true;
                if (field_control_point_list != null) {

                    WorRepRevisionControlPointActivity activity = (WorRepRevisionControlPointActivity) getActivity();
                    //validation
                    for (Iterator<FieldsRevisionControlPoint> iterator_validation = field_control_point_list.iterator(); iterator_validation.hasNext(); ) {
                        FieldsRevisionControlPoint iter_control_points_validator = iterator_validation.next();
                        iter_control_points_validator.getComment().setError(null);
                        if (iter_control_points_validator.getUtil_life().getSelectedItem().toString().equals("- Seleccione -")) {
                            iter_control_points_validator.getComment().requestFocus();
                            Util.showAlert(activity, "Ok", "Vida &uacute;til obligatoria");
                            validation_correct = false;
                            break;
                        } else {
                            if (Integer.valueOf(iter_control_points_validator.getUtil_life().getSelectedItem().toString()) <= 40) {
                                if (iter_control_points_validator.getPhoto().getDrawable() == null) {
                                    iter_control_points_validator.getComment().requestFocus();
                                    Util.showAlert(activity, "Ok", "Al seleccionar una vida &uacute;til inferior al 50% el campo imagen es obligatorio");
                                    validation_correct = false;
                                    break;
                                }
                                if (iter_control_points_validator.getComment().getText().toString().isEmpty()) {
                                    Log.e("error validation", iter_control_points_validator.getUtil_life().getSelectedItem().toString());
                                    iter_control_points_validator.getComment().requestFocus();
                                    iter_control_points_validator.getComment().setError(getResources().getString(R.string.error_empty_comment));
                                    Util.showAlert(activity, "Ok", "Al seleccionar una vida &uacute;til inferior al 50% el campo comentario es obligatorio");
                                    validation_correct = false;
                                    break;
                                }

                            }
                        }
                    }

                    if (validation_correct) {
                        saveControlPoint(getActivity(), "FINISHED");
                    }
                }
            }
        });

        button_save_revision_control_points.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                saveControlPoint(getActivity(), "PARTIAL");
            }
        });

        return rootView;
    }
/*
Load dynamically control points in main view
 */
    public void loadControlPoint(View rootView, LayoutInflater inflater, String service_request_id, final ArrayList<FieldsRevisionControlPoint> fiels_control_points) {

        //add LinearLayout to scroll view
        LinearLayoutControlPoints = (LinearLayout) rootView.findViewById(R.id.containerRevisionControlPoints);

        //get data from control point
        control_point_data = new ControlPointData(getActivity().getApplicationContext());
        inf_control_point = control_point_data.getAllControlPoint();

        spinnerPercentage = new ArrayList<String>();
        spinnerPercentage.add("- Seleccione -");
        spinnerPercentage.add("0");
        spinnerPercentage.add("10");
        spinnerPercentage.add("20");
        spinnerPercentage.add("30");
        spinnerPercentage.add("40");
        spinnerPercentage.add("50");
        spinnerPercentage.add("60");
        spinnerPercentage.add("70");
        spinnerPercentage.add("80");
        spinnerPercentage.add("90");
        spinnerPercentage.add("100");

        if (parentActivity.getNewControlPointList() != null) {
            current_button_control_point = parentActivity.getId0();
        }

        if (inf_control_point != null) {

            //rotation
            for (Iterator<ControlPoint> iterator = inf_control_point.iterator(); iterator.hasNext(); ) {
                ControlPoint controlPoint = iterator.next();
                String id_control_point_serv_req = service_request_id + "_" + controlPoint.getId();

                ControlPointServiceRequestData control_point_to_show_data = new ControlPointServiceRequestData(getActivity().getApplicationContext());
                ControlPointServiceRequest control_point_to_show = new ControlPointServiceRequest();

                if (fiels_control_points != null) {
                    if (fiels_control_points.size() > 0) {
                        for (Iterator<FieldsRevisionControlPoint> iterator_c_p = fiels_control_points.iterator(); iterator_c_p.hasNext(); ) {
                            FieldsRevisionControlPoint fieldsCPParent = iterator_c_p.next();
                            if (fieldsCPParent.getId().equals(id_control_point_serv_req)) {
                                field_control_point_obj = fieldsCPParent;
                                control_point_to_show = new ControlPointServiceRequest();
                                control_point_to_show.setId(fieldsCPParent.getId());
                                control_point_to_show.setComment(fieldsCPParent.getComment().getText().toString());
                                control_point_to_show.setUtil_life(fieldsCPParent.getUtil_life().getSelectedItem().toString());
                                if (fieldsCPParent.getPhoto_path() != null)
                                    control_point_to_show.setPhoto(fieldsCPParent.getPhoto_path().toString());

                                break;
                            }
                        }
                    }

                } else {

                    control_point_to_show = control_point_to_show_data.getControlPointServiceRequest(id_control_point_serv_req);

                    if (control_point_to_show == null)
                        control_point_to_show = new ControlPointServiceRequest();

                    //field revision control point to save later in database
                    field_control_point_obj = new FieldsRevisionControlPoint();
                    field_control_point_obj.setId(id_control_point_serv_req);
                    field_control_point_obj.setControl_point_id(controlPoint.getId());
                    field_control_point_obj.setService_request_id(service_request_id);
                }

                LinearLayout row = new LinearLayout(getActivity().getApplicationContext());
                row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                params.setMargins(0, 0, 0, 10);
                row.setLayoutParams(params);
                row.setOrientation(LinearLayout.VERTICAL);
                row.setPadding(16, 16, 16, 16);
                row.setBackgroundColor(Color.WHITE);

                // title control point
                TextView title_control_point = new TextView(getActivity().getApplicationContext());
                title_control_point.setText("Punto " + controlPoint.getId() + ": " + controlPoint.getName());
                title_control_point.setBackgroundResource(R.drawable.title_tab_problem_style);
                title_control_point.setTextColor(Color.parseColor("#000000"));
                title_control_point.setTypeface(null, Typeface.BOLD);
                title_control_point.setTextSize(23);

                row.addView(title_control_point);

                //title util life points
                TextView title_util_life = new TextView(getActivity().getApplicationContext());
                title_util_life.setText(Html.fromHtml("Vida &Uacute;til:"));
                title_util_life.setTextSize(16);
                title_util_life.setTypeface(null, Typeface.BOLD);
                row.addView(title_util_life);

                Spinner spinner = new Spinner(getActivity().getApplicationContext());
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.spiner_item_list, spinnerPercentage); //selected item will look like a spinner set from XML
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                Integer selected_index = spinnerPercentage.indexOf(control_point_to_show.getUtil_life());
                spinner.setAdapter(spinnerArrayAdapter);
                spinner.setSelection(selected_index);
                row.addView(spinner);
                field_control_point_obj.setUtil_life(spinner);

                //title util life points
                TextView title_photo = new TextView(getActivity().getApplicationContext());
                title_photo.setText(Html.fromHtml("Imagen:"));
                title_photo.setTextSize(16);
                title_photo.setTypeface(null, Typeface.BOLD);
                row.addView(title_photo);

                //show photo
                ivImage = new ImageView(getActivity().getApplicationContext());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(400, 300);
                layoutParams.gravity = Gravity.CENTER;
                ivImage.setLayoutParams(layoutParams);
                ivImage.setVisibility(View.GONE);
                ivImage.setPadding(10, 10, 10, 10);

                //load image
                if (control_point_to_show.getPhoto() != null) {
                    String selectedImagePath = control_point_to_show.getPhoto();
                    Util.setFullImageFromFilePath(selectedImagePath, ivImage);
                    field_control_point_obj.setPhoto_path(selectedImagePath);
                    ivImage.setVisibility(View.VISIBLE);
                }
                row.addView(ivImage);
                field_control_point_obj.setPhoto(ivImage);

                //button select image
                LinearLayout.LayoutParams layoutParamsButtonSelectPhoto = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParamsButtonSelectPhoto.gravity = Gravity.CENTER;

                Button button_select_photo = new Button(getActivity().getApplicationContext());
                button_select_photo.setText("Seleccionar Imagen");
                button_select_photo.setTag(controlPoint.getId());
                row.addView(button_select_photo);

                button_select_photo.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        selectImage();
                        parentActivity.setId0(v.getTag().toString());
                        current_button_control_point = v.getTag().toString();
                    }
                });

                //title comentario
                TextView title_comment = new TextView(getActivity().getApplicationContext());
                title_comment.setText(Html.fromHtml("Comentario:"));
                title_comment.setTextSize(16);
                title_comment.setTypeface(null, Typeface.BOLD);
                row.addView(title_comment);

                //edit text work done
                EditText comment = new EditText(getActivity().getApplicationContext());
                comment.setSingleLine(false);
                comment.setTextColor(Color.parseColor("#000000"));
                comment.setBackgroundResource(R.drawable.edit_text_style);

                comment.setText(control_point_to_show.getComment());
                row.addView(comment);
                field_control_point_obj.setComment(comment);
                field_control_point_list.add(field_control_point_obj);

                //set array in parent
                parentActivity.setNewControlPointList(field_control_point_list);

                //enter
                TextView space_net_table = new TextView(getActivity().getApplicationContext());
                space_net_table.setText("\n");
                row.addView(space_net_table);
                LinearLayoutControlPoints.addView(row);
            }
        }

    }

    /*
    allow to get a image from galery or take a picture
     */
    private void selectImage() {
        final CharSequence[] items = {"Tomar Foto", Html.fromHtml("Seleccionar de Galer&iacute;a"),
                "Cancelar"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Agregar Imagen!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (item == 1) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Seleccionar Imagen"),
                            SELECT_FILE);
                } else if (item == 2) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Integer current_control_point = (Integer.valueOf(current_button_control_point.toString()));
        if (field_control_point_list.size() > 0) {
            for (Iterator<FieldsRevisionControlPoint> iterator = field_control_point_list.iterator(); iterator.hasNext(); ) {
                FieldsRevisionControlPoint iter_control_point = iterator.next();
                if (iter_control_point.getId().equals(service_request_id + "_" + current_control_point)) {
                    if (resultCode == Activity.RESULT_OK) {
                        if (requestCode == SELECT_FILE) {
                            iter_control_point.setPhoto_path(Util.onSelectFromGalleryResult(data, iter_control_point.getPhoto(), getActivity()));
                        } else if (requestCode == REQUEST_CAMERA)
                            iter_control_point.setPhoto_path(Util.onCaptureImageResult(data, iter_control_point.getPhoto()));
                        break;
                    }
                }
            }
        }


    }

    /*
    Allow to save control point in DB and save images in a defined directory in the device.
     */
    public void saveControlPoint(Context context, String action) {

        ControlPointServiceRequestData control_poi_data = new ControlPointServiceRequestData(context);
        ControlPointServiceRequest control_point;

        //delete previous control point in service request
        control_poi_data.deleteByServiceRequest(service_request_id);

        for (Iterator<FieldsRevisionControlPoint> iterator = field_control_point_list.iterator(); iterator.hasNext(); ) {
            FieldsRevisionControlPoint iter_control_points = iterator.next();
            //save control point in DB
            control_point = new ControlPointServiceRequest();
            control_point.setId(iter_control_points.getId());
            control_point.setControl_point_id(iter_control_points.getControl_point_id());
            control_point.setService_request_id(iter_control_points.getService_request_id());
            control_point.setComment(iter_control_points.getComment().getText().toString());
            control_point.setUtil_life(iter_control_points.getUtil_life().getSelectedItem().toString());
            if (iter_control_points.getPhoto_path() != null) {
                //save image in a directory
                String new_path = Util.saveApplicationDirectory(iter_control_points.getPhoto_path(), service_request_id, "control_point_images", Integer.valueOf(iter_control_points.getControl_point_id()));
                control_point.setPhoto(new_path);
            }
            control_poi_data.save(control_point);
        }

        Intent intent = new Intent(getActivity(), WorkReportHomeActivity.class);
        intent.putExtra("service_request_id", service_request_id);

        if (action.equals("FINISHED")) {
            //valid status
            Util.ValidationServiceRequest(service_request_id, 3, getActivity());
            //send success message
            intent.putExtra("message", getResources().getString(R.string.work_report_message_control_point));
        } else {
            ValidationServiceRequestData validation_data = new ValidationServiceRequestData(context);
            validation_data.deleteByKey(service_request_id + "_" + 3);
            intent.putExtra("message", getResources().getString(R.string.work_report_message_partial_control_point));
        }
        startActivity(intent);
        parentActivity.finish();
    }
}
