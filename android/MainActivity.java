package app.taxi_android.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;

import app.taxi_android.R;
import app.taxi_android.data.RideData;
import app.taxi_android.data.TaxiAssociatedData;
import app.taxi_android.models.Ride;
import app.taxi_android.util.AndroidMultiPartEntity;
import app.taxi_android.util.TaxiDataBase;
import app.taxi_android.util.Util;


public class MainActivity extends ActionBarActivity {
    private String message;
    private String saved_current_ride_id;
    //variables send to server
    private ProgressBar progressBar;
    private TextView txtPercentage;
    long totalSize = 0;
    private ProgressDialog progDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new TaxiDataBase(this);

        progDialog = new ProgressDialog(this, ProgressDialog.THEME_TRADITIONAL);

        //send to server
        txtPercentage = (TextView) findViewById(R.id.txtPercentage);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        if (Util.hasInternetConnectivity(this)) {
            // before to start the application it sends  all saved surveys to the server
            RideData ride_all_data = new RideData(this);
            ArrayList<Ride> rides_list = new ArrayList<Ride>();
            String[] status = {"COMPLETED", "CANCELLED"};
            rides_list = ride_all_data.getRidesByStatus(status);

            if (rides_list != null) {
                synchronizeWithServerSavedSurveys();
            }else{
                initApplication();
            }
        }else{
            initApplication();
        }


    }

    private void initApplication(){
        //get current ride
        RideData ride_data = new RideData(this);
        Ride obj_ride = new Ride();
        obj_ride = ride_data.getCurrentRide("INCOMPLETED");

        message = getIntent().getStringExtra("message");
        if (message != null) {
            Log.e("message", message);
        }

        if (obj_ride != null) {
            Intent intent = new Intent(this, InfoTaxiActivity.class);
            intent.putExtra("current_ride_id", obj_ride.getId());
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
            if (message != null) {
                message = Html.fromHtml(message).toString();
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
            finish();
        }
    }


    //save in server
    private void synchronizeWithServerSavedSurveys() {
        progDialog = ProgressDialog.show(MainActivity.this,
                getResources().getString(R.string.loading_ini_title),
                getResources().getString(R.string.loading_ini),
                true);
        new ReadSavedRidesSurveyJSONFeedTask().execute();
    }

    private class ReadSavedRidesSurveyJSONFeedTask extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            progressBar.setProgress(0);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Making progress bar visible
            progressBar.setVisibility(View.VISIBLE);

            // updating progress bar value
            progressBar.setProgress(progress[0]);

            // updating percentage value
            txtPercentage.setText(String.valueOf(progress[0]) + "%");
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                return uploadRides();
            } catch (Exception ex) {
                Util.logException(ex);
            }
            return null;
        }

        @SuppressWarnings("deprecation")
        private String uploadRides() {
            String responseString;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(getResources().getString(R.string.url_web_services) + "ws/taxi/synchronize");

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                Charset chars = Charset.forName("UTF-8");

                //reported problems
                RideData rides_data = new RideData(getApplication().getApplicationContext());
                ArrayList<Ride> rides = new ArrayList<Ride>();
                String[] status = {"COMPLETED", "CANCELLED"};
                rides = rides_data.getRidesByStatus(status);

                if(rides!=null){
                    for (Iterator<Ride> iterator = rides.iterator(); iterator.hasNext(); ) {
                        Ride inf_ride = iterator.next();

                        entity.addPart("start_date["+inf_ride.getId()+"]", new StringBody(inf_ride.getStart_date(), chars));
                        if (inf_ride.getFinish_date() != null)
                            entity.addPart("finish_date["+inf_ride.getId()+"]", new StringBody(inf_ride.getFinish_date(), chars));
                        entity.addPart("start_location["+inf_ride.getId()+"]", new StringBody(inf_ride.getStart_location(), chars));
                        if (inf_ride.getFinish_location() != null)
                            entity.addPart("finish_location["+inf_ride.getId()+"]", new StringBody(inf_ride.getFinish_location(), chars));
                        if (inf_ride.getQuality() != null)
                            entity.addPart("quality["+inf_ride.getId()+"]", new StringBody(inf_ride.getQuality(), chars));
                        if (inf_ride.getTaximeter() != null)
                            entity.addPart("taximeter["+inf_ride.getId()+"]", new StringBody(inf_ride.getTaximeter(), chars));
                        if (inf_ride.getStatus() != null)
                            entity.addPart("status["+inf_ride.getId()+"]", new StringBody(inf_ride.getStatus(), chars));
                        if (inf_ride.getTaxi_id() != null)
                            entity.addPart("taxi_id["+inf_ride.getId()+"]", new StringBody(inf_ride.getTaxi_id(), chars));
                    }
                }

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        protected void onPostExecute(String result) {

            String message = "";
            String status_transaction = "";
            String errors= "";
            progDialog.dismiss();
            try {
                JSONObject response = new JSONObject(result);
                message = response.getString("message");
                status_transaction = response.getString("status_transaction");

                if (status_transaction.equals("SUCCESS")) {
                    /*
                        DELETE RIDES
                     */

                    RideData ride_all_data = new RideData(MainActivity.this);
                    ArrayList<Ride> rides_list = new ArrayList<Ride>();
                    String[] status = {"COMPLETED", "CANCELLED"};
                    rides_list = ride_all_data.getRidesByStatus(status);
                    if(rides_list!=null){
                        for (Iterator<Ride> iterator = rides_list.iterator(); iterator.hasNext(); ) {
                            Ride inf_ride = iterator.next();
                            TaxiAssociatedData taxi_associated_data = new TaxiAssociatedData(getApplication().getApplicationContext());
                            RideData ride_data = new RideData(getApplication().getApplicationContext());
                            taxi_associated_data.deleteByKey(inf_ride.getTaxi_associated_id());
                            ride_data.deleteByKey(inf_ride.getId());
                        }
                    }
                }else {
                    if(status_transaction.equals("PARTIAL_SUCCESS")){
                        message = response.getString("message");

                        RideData ride_all_data = new RideData(MainActivity.this);
                        ArrayList<Ride> rides_list = new ArrayList<Ride>();
                        String[] status = {"COMPLETED", "CANCELLED"};
                        rides_list = ride_all_data.getRidesByStatus(status);
                        if(rides_list!=null){
                            for (Iterator<Ride> iterator = rides_list.iterator(); iterator.hasNext(); ) {
                                Ride inf_ride = iterator.next();
                                TaxiAssociatedData taxi_associated_data = new TaxiAssociatedData(getApplication().getApplicationContext());
                                RideData ride_data = new RideData(getApplication().getApplicationContext());
                                taxi_associated_data.deleteByKey(inf_ride.getTaxi_associated_id());
                                ride_data.deleteByKey(inf_ride.getId());
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();

            }
            initApplication();
            super.onPostExecute(result);
        }
    }
}
