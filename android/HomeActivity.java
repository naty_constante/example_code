package app.taxi_android.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import app.taxi_android.R;
import app.taxi_android.data.ParameterData;
import app.taxi_android.data.RideData;
import app.taxi_android.data.TaxiAssociatedData;
import app.taxi_android.models.MCrypt;
import app.taxi_android.models.Parameter;
import app.taxi_android.models.Ride;
import app.taxi_android.models.TaxiAssociated;
import app.taxi_android.util.GPSTracker;
import app.taxi_android.util.Util;

public class HomeActivity extends ActionBarActivity {

    private Button btnStartRide;
    private CheckBox ckbTerms;
    private TextView txtTerms;
    private GPSTracker gps;
    private ProgressDialog progDialog;
    private TextView location;
    private String current_ride_id;
    private Boolean terms_conditions = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btnStartRide = (Button) findViewById(R.id.btn_start_ride);
        ckbTerms = (CheckBox) findViewById(R.id.ckb_terms);
        txtTerms = (TextView) findViewById(R.id.txt_link_terms);
        location = (TextView) findViewById(R.id.hdn_start_ride);

        String htmlString = "<u><b>t&eacute;rminos y condiciones</b> </u>";
        txtTerms.setText(Html.fromHtml(htmlString));
        txtTerms.setTextColor(Color.BLACK);

        location.setVisibility(View.GONE);

        terms_conditions = checkTermsAndConditions();
        if (terms_conditions) {
            ckbTerms.setVisibility(View.GONE);
            txtTerms.setVisibility(View.GONE);
        }


        btnStartRide.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (ckbTerms.isChecked() || terms_conditions == true) {

                    if (terms_conditions == false) {
                        ParameterData parameter_data = new ParameterData(HomeActivity.this);
                        Parameter parameter = new Parameter();
                        parameter.setId("1");
                        parameter.setTerms_and_conditions("TRUE");
                        parameter_data.save(parameter);
                    }
                    //get location GPS
                    getlocation(HomeActivity.this);
                    if (!location.getText().toString().isEmpty()) {
                        //QR code reader initialization
                        IntentIntegrator integrator = new IntentIntegrator(HomeActivity.this);
                        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                        integrator.setPrompt(getResources().getString(R.string.message_scan));
                        integrator.initiateScan();
                    } else {
                        Util.showAlert(HomeActivity.this, "Ok", getResources().getString(R.string.error_get_location));
                    }
                } else {
                    Util.showAlert(HomeActivity.this, "Ok", getResources().getString(R.string.alert_terms_required));
                }
            }
        });

        txtTerms.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                txtTerms.setTextColor(Color.rgb(86,42,133));
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.link_terms_and_conditions)));
                startActivity(browserIntent);
            }
        });

    }
    /*
    get current user location using GPS tracker
     */
    public void getlocation(Context context) {

        gps = new GPSTracker(context);
        progDialog = new ProgressDialog(context, ProgressDialog.THEME_TRADITIONAL);
        Util.showProgressDialog(progDialog, "Detectando ...");
        Util.updateLocation(gps, location, progDialog);
    }

    public boolean checkTermsAndConditions() {
        ParameterData parameter_data = new ParameterData(this);
        Parameter parameter = new Parameter();
        parameter = parameter_data.getParameter("1"); //1 represent terms and conditions row, this exists only if user accepts terms and conditions.
        if (parameter != null) {
            return true;
        } else {
            return false;
        }
    }
/*
    Save taxi data obtained from QR code
 */
    public void saveInitialRideData(Context context, String[] qr_data_taxi) {
        Long id_ride;
        Long id_taxi_assoc;
        RideData ride_data = new RideData(context);
        Ride obj_ride = new Ride();

        TaxiAssociatedData taxi_associated_data = new TaxiAssociatedData(context);
        TaxiAssociated taxi_associated_obj = new TaxiAssociated();

        taxi_associated_obj.setBrand(qr_data_taxi[2]);
        taxi_associated_obj.setModel(qr_data_taxi[3]);
        taxi_associated_obj.setColor(qr_data_taxi[4]);
        taxi_associated_obj.setCompany_name(qr_data_taxi[5]);
        taxi_associated_obj.setPlate(qr_data_taxi[6]);
        taxi_associated_obj.setRegistry_number(qr_data_taxi[7]);
        taxi_associated_obj.setTaxi_id(qr_data_taxi[0]);
        taxi_associated_obj.setAsociated(qr_data_taxi[1]);

        id_taxi_assoc = taxi_associated_data.save(taxi_associated_obj);

        if (id_taxi_assoc != null) {

            Calendar c = Calendar.getInstance();
            c.getTime();
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String start_date = format1.format(c.getTime());

            obj_ride.setStart_date(start_date);
            obj_ride.setStart_location(location.getText().toString());
            obj_ride.setStatus("INCOMPLETED");
            obj_ride.setTaxi_id(taxi_associated_obj.getTaxi_id());
            obj_ride.setTaxi_associated_id(String.valueOf(id_taxi_assoc));

            id_ride = ride_data.save(obj_ride);
            if (id_ride != null) {
                current_ride_id = String.valueOf(id_ride);
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            String response_data_taxi = scanResult.getContents();

            if (response_data_taxi != null) {

                try {

                    MCrypt mcrypt = new MCrypt();

                    /* Decrypt taxi data in QR code*/
                    String decrypted = new String(mcrypt.decrypt(response_data_taxi));

                    String[] split_qr = decrypted.split(",");

                    if (split_qr.length == 11) {
                        //save in database initial data in a ride
                        saveInitialRideData(HomeActivity.this, split_qr);
                        //  redirect to new intent
                        Intent intent_info = new Intent(HomeActivity.this, InfoTaxiActivity.class);
                        intent_info.putExtra("current_ride_id", current_ride_id);
                        startActivity(intent_info);
                        finish();
                    } else {
                        Util.showAlert(this, "Ok", getResources().getString(R.string.message_invalid_code));
                    }

                } catch (Exception ex) {
                    Util.logException(ex);
                    Util.showAlert(this, "Ok", getResources().getString(R.string.message_invalid_code));
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        ActivityCompat.finishAffinity(this);
    }

}
