Ext.define('Base.model.WorkOrder', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      {name: 'id', type: 'string', isUnique: true},
      {name: 'date_created', type: 'int'},
      {name: 'call_slip', type: 'string'},
      {name: 'external_id', type: 'string'},
      {
        name: 'expiration_date', type: 'string',
        convert: function (value) {
          return formatWoDate(value);
        }
      },
      {
        name: 'scheduled_date_timestamp', type: 'number',
        convert: function (v, record) {
          return record.get('scheduled_date');
        }
      },
      {
        name: 'scheduled_date', type: 'string',
        convert: function (value) {
          return formatWoDate(value);
        }
      },
      {name: 'customer_po', type: 'string'},
      {name: 'status', type: 'string'},
      {name: 'store_name', type: 'string'},
      {name: 'priority', type: 'string'},
      {name: 'address', type: 'string'},
      {name: 'general_notes', type: 'string'},
      {name: 'latitude', type: 'string'},
      {name: 'longitude', type: 'string'},
      {name: 'check_out', type: 'object'},
      {name: 'ivr', type: 'object'},
      {name: 'distance', type: 'number'},
      {name: 'distance_unit', type: 'string'},
      {
        name: 'group_index', type: 'int', defaultValue: 0, persist: false,
        convert: function (v, record) {
          var status = record.get('status');
          var expiration_date = Math.round(new Date(record.get('expiration_date')) / 1000);
          if (status == 'open' && expiration_date > Math.round(Date.now() / 1000)) {
            return 2;
          }
          else if (status == 'in_progress' && expiration_date > Math.round(Date.now() / 1000)) {
            return 1;
          }
          else if (status == 'completed') {
            return 3;
          }
          else {
            return 4;
          }
        }
      },
      {
        name: 'contact_person', type: 'array',  //brinco  field
        convert: function (value) {
          if (Array.isArray(value) && App.adAppType == 'Brinco') {
            return value.join(' ');
          } else {
            return value;
          }
        }
      },
      {
        name: 'contact_name', type: 'string', persist: false,  //rael field
        convert: function (v, record) {
          var contact_person = record.get('contact_person');
          return Array.isArray(contact_person) ? contact_person[0] : contact_person;
        }
      },
      {name: 'trade', type: 'string'}, //rael field
      {name: 'store_id', type: 'string'}, //rael field
      {name: 'area_id', type: 'string'}, //rael field
      {
        name: 'contact_number', type: 'string', persist: false,  //rael field
        convert: function (v, record) {
          var contact_person = record.get('contact_person');
          return Array.isArray(contact_person) ? contact_person[1].replace('-', '.') : contact_person;
        }
      },
      {name: 'problem_desc', type: 'string'},  //brinco field
      {name: 'instructions', type: 'string'}  //brinco field
    ],
    proxy: {
      type: 'rest',
      url: '/mobileserv/workorders'
    }
  }
});
// Returns a formatted date for wo
function formatWoDate (field, section_date) {
  var dateObj = new Date(field * 1000);
  date = Ext.Date.format(dateObj, 'm/d/Y');
  time = Ext.Date.format(dateObj, 'g:i a');

  if (section_date == "D") {
    return date
  } else if (section_date == "H") {
    return time;
  } else {
    return date + " " + time;
  }
}

