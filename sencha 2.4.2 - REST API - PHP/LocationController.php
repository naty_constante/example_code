<?php

namespace ad\FTC;

use \MABI\RESTModelController;

include_once __DIR__ . '/../middleware/SessionHeader.php';

/**
 *
 * Manages the endpoints that encompass all elements of the work order details,
 * which include, but are not limited to, the blanket authorization of location,
 * time record, status, instructions and call to action for a work order.
 * @middleware("ad\FTC\SessionHeader")
 * @middleware("\MABI\Identity\Middleware\SessionOnlyAccess")
 * @middleware("\MABI\Middleware\SharedSecret")
 */
class LocationController extends RESTModelController {

  /**
   * Retrieves all the nearby locations from a specific coordinates
   * @Docs\Param("latitude",type="float",location="query",required=true,description="Location latitude")
   * @Docs\Param("longitude",type="float",location="query",required=true,description="Location longitude")
   * @Docs\Param("trade",type="string",location="query",required=false,description="Filter trade for work order count")
   */
  public function get() {
    $latitude  = $this->getApp()->getRequest()->get('latitude');
    $longitude = $this->getApp()->getRequest()->get('longitude');

    if ($this->getApp()->getRequest()->apiApplication->applicationName == BRIGHTVIEW_APP_NAME) {
      $trade = $this->getApp()->getRequest()->get('trade');
      $trade = !$trade || strstr($trade, 'landscape') ? LAND_TRADE : SNOW_TRADE;

      if (!is_numeric($latitude) || !is_numeric($longitude)) {
        echo json_encode([]);
        return;
      }
      echo json_encode($this->getLocationWorkOrderCount($latitude, $longitude, $trade));

    } else {

      echo json_encode($this->getWoCountRael($latitude, $longitude));
    }
  }

  /**
   * Returns all the locations nearby the provider's location
   */
  public function getNearby() {
    $latitude  = $this->getApp()->getRequest()->get('latitude');
    $longitude = $this->getApp()->getRequest()->get('longitude');
    $scAPI     = new ServiceChannel($this->getApp());
    $scAPI->setAPIVersion(3);
    $locations = $this->getNearByLocations($scAPI, $latitude, $longitude);

    echo json_encode($locations);
  }

  /**
   * @param float  $latitude
   * @param float  $longitude
   * @param string $trade
   *
   * @return array
   */
  private function getLocationWorkOrderCount($latitude, $longitude, $trade = LAND_TRADE) {
    $company = Company::init($this->getApp());
    $company->findById($this->getApp()->getRequest()->session->user->company_id);

    $distanceRadius = 25;
    $userFilter     = '';
    // Override distance radius and filter if the provider used is BES Market Manager
    if ($company->external_token['SC']['provider_id'] == 2000085905) {
      $distanceRadius = 5;
      $userFilter     = ' and CreatedBy eq \'adbvtest\'';
    }

    $scAPI = new ServiceChannel($this->getApp());
    $scAPI->setAPIVersion(3);
    $allLocations = $this->getNearByLocations($scAPI, $latitude, $longitude, $distanceRadius);

    $nextWeek     = date('Y-m-d\TH:i:s.uP', strtotime('+1 week'));
    $today        = date('Y-m-d\TH:i:s.uP');
    $skip         = 0;
    $foundWos     = [];
    $filter       = Util::getWoTradeFilter($this->getApp(), $trade);
    $woController = new WorkOrderController($this->getExtension());
    do {
      $scResponse = $scAPI->callGET('workorders', [
        '$expand' => 'Location($select=Id,Name,Latitude,Longitude),Subscriber,Provider',
        '$filter' => "{$filter} and CallDate lt $nextWeek and ExpirationDate gt $today and (Status/Primary eq 'OPEN' or Status/Primary eq 'IN PROGRESS')" . $userFilter,
        '$skip'   => $skip,
        '$count'  => 'true'
      ]);
      if (is_array($scResponse->value) && count($scResponse->value) > 0) {
        foreach ($scResponse->value as $wo) {
          $foundWos[$wo->Location->Id]['wos'][] = $woController->_formatScWorkOrder($wo);
          if (isset($foundWos[$wo->Location->Id][$wo->Status->Primary])) {
            $foundWos[$wo->Location->Id][$wo->Status->Primary]++;
          }
          else {
            $foundWos[$wo->Location->Id][$wo->Status->Primary] = 1;
          }
        }
        $skip += 50;
      }
    } while ($scResponse->{'@odata.count'} > $skip);

    $locations = [];
    foreach ($allLocations as $location) {
      if (isset($foundWos[$location['id']]) && $this->_checkWoCounter($foundWos[$location['id']])) {
        $location['open_wos_count']       = isset($foundWos[$location['id']]['OPEN']) ? $foundWos[$location['id']]['OPEN'] : 0;
        $location['inprogress_wos_count'] = isset($foundWos[$location['id']]['IN PROGRESS']) ? $foundWos[$location['id']]['IN PROGRESS'] : 0;
        $location['wos']                  = $foundWos[$location['id']]['wos'];
        $locations[]                      = $location;
      }
    }

    return $locations;
  }
  /**
   * Gets all the work orders and order by sites
   * Counts the number of open and in_progress work orders
   *
   * @param number $latitude and $longitude
   *
   * @return array
   */
  private function getWoCountRael($currentLatitude, $currentLongitude ) {
    $company = Company::init($this->getApp());
    $company->findById($this->getApp()->getRequest()->session->user->company_id);

    $foundWosCount = [];
    $sort = array('scheduled_date' => 1);
    $query['sort'] = $sort;
    $query['query']['user_ids'] = ['$all' => [$this->getApp()->getRequest()->session->userId]];
    $query['query']['company_id'] = $company->id;
    $outputArr    = [];
    $workOrder_bn = [];
    $inst = WorkOrder::init($this->getApp());

    foreach ($inst->query($query) as $foundModel) {
      $aux = $foundModel->getPropertyArray(true);

      //save the distance from current user location to the stores
      $util                  = new Util();
      $site['id']            = $aux['id'];
      $distance_unit         = 'M';
      $site['status']        = $aux['status'];
      $site['address']       = isset($aux['address']) ? $aux['address'] : '';
      $site['name']          = isset($aux['store_name']) ? $aux['store_name'] : '';
      $site['store_id']      = isset($aux['store_id']) ? $aux['store_id'] : '';
      $site['distance']      = $util->calculateDistanceBetweenCoordinates($currentLatitude, $currentLongitude,
        $aux['latitude'], $aux['longitude'], $distance_unit, 2);
      $site['distance_unit'] = $site['distance'] ? strtolower($distance_unit) : '';
      $site['priority']      = isset($aux['priority']) ? $aux['priority'] : '';

      $workOrder_bn[] = $foundModel->scheduled_date;
      $outputArr[]    = $site;
    }

      foreach ($outputArr as $wo) {
        if (isset($foundWosCount[$wo['store_id']][$wo['status']])) {
          $foundWosCount[$wo['store_id']][$wo['status']]++;
        }
        else {
          $foundWosCount[$wo['store_id']][$wo['status']] = 1;
        }
      }

    $locations = [];
    $output = array_intersect_key($outputArr, array_unique(array_column($outputArr, 'store_id')));

    foreach ($output as $location) {
      if (isset($foundWosCount[$location['store_id']]) && $this->_checkWoCounter($foundWosCount[$location['store_id']])) {
        $location['open_wos_count']       = isset($foundWosCount[$location['store_id']]['open']) ? $foundWosCount[$location['store_id']]['open'] : 0;
        $location['inprogress_wos_count'] = isset($foundWosCount[$location['store_id']]['in_progress']) ? $foundWosCount[$location['store_id']]['in_progress'] : 0;
        if ($location['open_wos_count'] > 0 || $location['inprogress_wos_count'] > 0) {
          $locations[] = $location;
        }
      }
    }

    return $locations;
  }

  /**
   * Check if the counters has at least one work order
   *
   * @param array $counters
   *
   * @return bool
   */
  private function _checkWoCounter($counters) {
    $display = FALSE;
    foreach ($counters as $count) {
      if ($count > 0) {
        $display = TRUE;
      }
    }
    return $display;
  }

  /**
   * @param ServiceChannel $scAPI
   * @param float          $latitude
   * @param float          $longitude
   * @param int            $distanceRadius
   *
   * @return array
   */
  private function getNearByLocations(ServiceChannel $scAPI, $latitude, $longitude, $distanceRadius = 5) {
    $skip           = 0;
    $foundLocations = [];
    do {
      $scResponse = $scAPI->callGET('locations/Service.NearBy(latitude=' . $latitude . ',longitude=' . $longitude . ')',
        [
          '$filter'  => 'Distance le ' . $distanceRadius,
          '$orderby' => 'Distance',
          '$skip'    => $skip,
          '$count'   => 'true'
        ]);
      if (is_array($scResponse->value) && count($scResponse->value) > 0) {
        foreach ($scResponse->value as $location) {
          $foundLocations[] = [
            'id'                   => $location->Id,
            'name'                 => $location->Name,
            'address'              => trim($location->Address1 . ' ' . $location->Address2),
            'city'                 => $location->City,
            'state'                => $location->State,
            'zip'                  => $location->Zip,
            'open_wos_count'       => 0,
            'inprogress_wos_count' => 0,
            'wos'                  => [],
            'distance'             => round($location->Distance, 1),
            'distance_unit'        => 'm'
          ];
        }
        $skip += 50;
      }
    } while ($scResponse->{'@odata.count'} > $skip);

    return $foundLocations;
  }

  /**
   * Deny access to default CRUD endpoints
   */
  public function post() {
    $this->getApp()->returnError(Errors::$INVALID_ENDPOINT);
  }

  public function _restGetResource($id) {
    $this->getApp()->returnError(Errors::$INVALID_ENDPOINT);
  }

  public function _restPutResource($id) {
    $this->getApp()->returnError(Errors::$INVALID_ENDPOINT);
  }

  public function _restDeleteResource($id) {
    $this->getApp()->returnError(Errors::$INVALID_ENDPOINT);
  }
}
