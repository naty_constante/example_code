Ext.define('Base.controller.WorkOrders', {
  extend: 'Ext.app.Controller',
  config: {
    lastStatus: null,
    sortWoBy: null,
    locationService: false,
    previous_check_in: null,
    refs: {
      //views
      mainView: 'main',
      workOrdersContainerNav: 'workorders',
      WorkOrders: 'workordersmain',
      woDetail: 'workorderdetail',
      woManager: 'workordermanager',
      techniciansCounterQuestion: 'technicianscounter',
      workOrdersPopUpNav: 'workorderpopupnav',

      //buttons
      menuWorkOrderButton: 'workorders #menu-work-order-button',
      refreshWoButton: 'workordersmain #refreshWosBtn',
      woDetailNextButton: 'workorders #NextButton',
      itemCheckInButton: 'container[name="itemCheckIn"]',
      itemJobRequirementButton: 'container[name="itemJobRequirement"]',
      itemCloseoutJobButton: 'container[name="itemCloseoutJob"]',
      itemCheckOutButton: 'container[name="itemCheckOut"]',
      leftMenuButton: 'workorders leftmenubutton',
      searchWoButton: 'workordersmaintapmenu #search-wo-button',

      //fields
      woSearchfield: 'workordersmain searchfield[name="woSearchfield"]',
      woList: 'workordersmainlist list[name="woList"]',
      requestList: 'requestmain #wo-request-list',

      //fieldset
      fieldsetWoLog: 'workordermanager #fieldsetWoLog',
      priorityField: 'workorderdetail [name=priority]',
      goWoButton: 'workorderfiltermessagebox #go-wo',
      workOrderFilterMessageBox: 'workorderfiltermessagebox',
      workOrderFilterCheckbox: 'workorderfiltermessagebox checkboxfield',
      workOrderFilterCloseButton: 'workorderfiltermessagebox #close-wo-filter-icon'
    },
    control: {
      //views
      workOrdersContainerNav: {
        initialize: 'onWorkOrdersContainerNavInitialize'
      },
      WorkOrders: {
        activate: 'onWorkOrdersActivate',
        deactivate: 'onWorkOrderDeactivate'
      },
      woDetail: {
        activate: 'onWoDetailActivate',
        deactivate: 'onWoDetailDeactivate'
      },
      woManager: {
        activate: 'onWoManagerActivate'
      },

      //buttons
      menuWorkOrderButton: {
        tap: 'onMenuWorkOrderButtonTap'
      },
      refreshWoButton: {
        tap: 'onRefreshWoButtonTap'
      },
      woDetailNextButton: {
        tap: 'onWoDetailNextButtonTap'
      },
      itemCheckInButton: {
        initialize: "onItemCheckInButtonInitialize"
      },
      itemCloseoutJobButton: {
        initialize: "onItemCloseOutJobButtonInitialize"
      },
      itemCheckOutButton: {
        initialize: "onItemCheckOutButtonInitialize"
      },
      //fields
      woSearchfield: {
        clearicontap: 'clearAndDismissWoSearchfield',
        keyup: 'onWoSearchfieldChanged'
      },
      woList: {
        itemtap: 'onWoItemTap'
      },
      goWoButton: {
        tap: 'goWoButtonTap'
      },
      workOrderFilterMessageBox: {
        show: 'onWorkOrderFilterMessageBoxActivate'
      },
      workOrderFilterCheckbox: {
        check: 'checkWorkOrderFilterCheckbox',
        uncheck: 'uncheckWorkOrderFilterCheckbox'
      },
      workOrderFilterCloseButton: {
        tap: 'onTapWorkOrderFilterCloseButton'
      },
      searchWoButton: {
        tap: 'onSearchButtonTap'
      }

    }
  },
  //mask for requests
  maskForRequests: function () {
    var contr = this;
    Ext.Msg.defaultAllowedConfig.showAnimation = false;
    Ext.Ajax.on('beforerequest', function (conn, options, eOpts) {
      if ('showLoading' in options && !options.showLoading) {
        return true;
      }
      if ('params' in options && 'showLoading' in options.params && !options.params.showLoading) {
        return true;
      }

      // Prevent multiple loading masks overlap
      if (Ext.ComponentQuery.query('loadmask').length > 1) return true;

      Ext.Viewport.setMasked({xtype: 'loadmask', zIndex: 20});

      //remove mask in wo main list
      if (contr.getWoList()) {
        contr.getWoList().setMasked(false);
      }
      //remove mask in wo request main list
      if (contr.getRequestList()) {
        contr.getRequestList().setMasked(false);
      }

      var dongle = new IOSDongle();
      dongle.calliOSFunction("checkConnection");
    });
    Ext.Ajax.on('requestcomplete', function (conn, response, options) {
      var loadmask = Ext.Viewport.getMasked();
      if ('showLoading' in options && !options.showLoading) {
        return true;
      }
      if ('params' in options && 'showLoading' in options.params && !options.params.showLoading) {
        return true;
      }
      if (loadmask) Ext.Viewport.remove(loadmask);
    });
    Ext.Ajax.on('requestexception', function (conn, response, options) {
      var loadmask = Ext.Viewport.getMasked();
      if ('showLoading' in options && !options.showLoading) {
        return true;
      }
      if ('params' in options && 'showLoading' in options.params && !options.params.showLoading) {
        return true;
      }
      if (loadmask) Ext.Viewport.remove(loadmask);

      if (response.status == 401) {
        location.reload();
      }
    });
  },
  //views
  onWorkOrdersContainerNavInitialize: function () {
    this.maskForRequests();

    //user with active session
    if (App.authUser) {
      this.setCurrentCoordinatesForWorkOrdersStore('App.getController(\'WorkOrders\').loadWoStore');
    } else {
      this.getWorkOrdersContainerNav().push(Ext.widget('workordersnouser'));
    }
  },
  loadWoStore: function (latitude, longitude, error) {

    var woStore = Ext.getStore('WorkOrders'), contr = this;
    var proxy = woStore.getProxy();
    proxy.setExtraParams({});
    if (!error) {
      proxy.setExtraParams({'currLatitude': latitude, 'currLongitude': longitude});
      if (!this.getLocationService()) {
        this.setSortWoBy('distance');
      }
      this.setLocationService(true);

    } else {
      this.setLocationService(false);
    }

    woStore.load(function (records, operation, success) {
      if (success) {
        contr.setInitLastStatus(contr);
        Ext.getStore('WorkOrders').filter([contr.filterToApply(contr.getLastStatus())]);
      }
      else {
        Ext.Msg.alert(lang.base.error_title, lang.base.error_loading_wo);
      }
    });
    this.getWorkOrdersContainerNav().push(Ext.widget('workordersmain'));
  },
  setCurrentCoordinatesForWorkOrdersStore: function (callbackFunctionName) {
    var ios = new IOSDongle();
    ios.calliOSFunction('fetchLocation', [callbackFunctionName]);
  },
  onSearchButtonTap: function () {
    if (this.getWoSearchfield().isHidden()) {
      this.getWoSearchfield().show();
    } else {
      this.getWoSearchfield().hide();
    }
  },
  onWorkOrdersActivate: function () {
    //add leftmenu button
    this.getLeftMenuButton().show();

    //clear previous selection wo
    this.getWoList().deselectAll();

    this.setInitLastStatus(this);

    if (!this.getSortWoBy()) {
      this.setSortWoBy('distance');
    }

    ga('send', 'screenview', {
      'appName': App.adAppType,
      'appVersion': lang.base.appVersion,
      'screenName': 'work_orders'
    });
    this.getWoSearchfield().fireEvent('clearicontap');
  },
  setInitLastStatus: function (contr) {
    this.setLastStatus(['open-in-progress-wo']);
  },
  onWorkOrderDeactivate: function () {
    //hide leftmenu button
    this.getLeftMenuButton().hide();
  },
  onWoDetailActivate: function () {

    NextButton = {
      xtype: 'button',
      itemId: 'NextButton',
      text: lang.base.lbl_next,
      cls: 'x-button-navigationbar',
      align: 'right'
    };
    this.NextButton = this.getWorkOrdersContainerNav().getNavigationBar().add(NextButton);

    var woReport = this.getWoDetail().getRecord();

    //add high priority color
    var optionPriority = lang.base.high_priorities_list;
    if (Ext.Array.contains(optionPriority, woReport.get('priority'))) {
      this.getPriorityField().addCls('high-priority');
    } else {
      this.getPriorityField().removeCls('high-priority');
    }
  },
  onWoDetailDeactivate: function () {
    this.getWorkOrdersContainerNav().getNavigationBar().remove(this.NextButton, true);
  },
  onWoManagerActivate: function () {
    var contr = this;
    var workOrderLogs = Ext.getStore('WorkOrderLogs');

    var woReport = this.getWoDetail().getRecord();
    var srQuery = '?woId=' + woReport.get('id');

    workOrderLogs.setProxy(
      {
        type: 'rest',
        url: '/mobileserv/workorderlogs' + srQuery
      }
    );

    workOrderLogs.load(function (records, operation, success) {
      if (success) {
        if (records.length > 0) {
          contr.getFieldsetWoLog().show();
        } else {
          contr.getFieldsetWoLog().hide();
        }
        //check if user has previous check in
        contr.validateFistCheckIn(contr);
      }
    });
  },
  //buttons
  onMenuWorkOrderButtonTap: function () {
    var woFilterMessageBox = Ext.widget('workorderfiltermessagebox');
    Ext.Viewport.add(woFilterMessageBox).show();
  },
  goWoButtonTap: function () {
    var woFilterMessageBox = this.getWorkOrderFilterMessageBox(),
      storeWO = Ext.getStore('WorkOrders'),
      selectedFiltersArray = new Array(),
      filterOptions = Ext.ComponentQuery.query('workorderfiltermessagebox checkboxfield'),
      contr = this;
    //original value
    this.setSortWoBy('distance');
    //get selected filters
    filterOptions.forEach(function (checkbox) {
      if (checkbox.isChecked()) {
        //sort filter
        if (checkbox.getName() == 'distance' || checkbox.getName() == 'scheduled_date_timestamp') {
          contr.setSortWoBy(checkbox.getName());
        } else {
          selectedFiltersArray.push(checkbox.getName());
        }
      }
    });
    storeWO.clearFilter();

    this.sortWoByFilter(storeWO, this.getSortWoBy());
    storeWO.filter([this.filterToApply(selectedFiltersArray)]);
    this.setLastStatus(selectedFiltersArray);
    this.getWoSearchfield().fireEvent('clearicontap');
    this.getWoSearchfield().setValue('');
    woFilterMessageBox.destroy();
  },
  onWorkOrderFilterMessageBoxActivate: function (self) {
    var contr = this,
      filterOptions = Ext.ComponentQuery.query('workorderfiltermessagebox checkboxfield'),
      distanceCheckbox = self.down('#sort-wo-fieldset [name=distance]'),
      isActivateButton = false;

    if (this.getLocationService()) {
      distanceCheckbox.enable();
    } else {
      distanceCheckbox.disable();
      contr.setSortWoBy('scheduled_date_timestamp');
    }

    //show selected filters
    filterOptions.forEach(function (checkbox) {
      isActivateButton = Ext.Array.contains(contr.getLastStatus(), checkbox.getName());
      if (isActivateButton) {
        checkbox.check();
      } else if (checkbox.getName() == contr.getSortWoBy()) { //sort wo filter
        checkbox.check();
      }
    });
  },
  checkWorkOrderFilterCheckbox: function (self) {
    //sort wo filter validation
    //If one of the options inside the sort wo filter fieldset is checked the other ones are unchecked
    if (self.getName() == 'distance' || self.getName() == 'scheduled_date_timestamp') {
      var sortWoCheckbox = Ext.ComponentQuery.query('workorderfiltermessagebox #sort-wo-fieldset checkboxfield');
      sortWoCheckbox.forEach(function (checkbox) {
        // Skip the current checked task
        if (checkbox.getName() == self.getName()) {
          return;
        }
        else {
          checkbox.uncheck();
        }
      });
    }
  },
  uncheckWorkOrderFilterCheckbox: function (self) {
    var filterFieldsetName = self.up('fieldset').getItemId();
    var filterOptions = Ext.ComponentQuery.query('workorderfiltermessagebox #' + filterFieldsetName + ' checkboxfield');

    //Evaluate if there is any checked checkboxfield
    var selectedFilter = filterOptions.some(function (checkbox) {
      return checkbox.isChecked();
    });
    //it has to be checked at least one option by each fieldset
    if (!selectedFilter) self.check();
  },
  onTapWorkOrderFilterCloseButton: function (self, e) {
    var woFilterMessageBox = this.getWorkOrderFilterMessageBox();

    if (Ext.isFunction(e.preventDefault)) e.preventDefault();
    woFilterMessageBox.destroy();
  },
  onRefreshWoButtonTap: function () {
    this.setCurrentCoordinatesForWorkOrdersStore('App.getController(\'WorkOrders\').loadWoStoreSimple');
  },
  loadWoStoreSimple: function (latitude, longitude, error) {
    var woStore = Ext.getStore('WorkOrders');
    var proxy = woStore.getProxy();
    proxy.setExtraParams({});
    if (!error) {
      proxy.setExtraParams({'currLatitude': latitude, 'currLongitude': longitude});
      if (!this.getLocationService()) {
        this.setSortWoBy('distance')
      }
      this.setLocationService(true);
    } else {
      this.setLocationService(false);
    }

    woStore.load();
  },
  onWoDetailNextButtonTap: function () {
    var workOrdderManager = Ext.widget('workordermanager');

    var woReport = this.getWoDetail().getRecord();

    //change wo detail title and save wo id in woDetailView
    workOrdderManager.setTitle("<span class='wo-title'>" + woReport.get('store_name') + "</span>");

    this.getWorkOrdersContainerNav().push(workOrdderManager);

  },
  onItemCheckInButtonInitialize: function (comp) {
    var contr = this;
    var woRecord = this.getWoDetail().getRecord();
    comp.element.on('tap', function () {

      //block action if wo is completed
      if (woRecord.get('status') == 'completed') {
        return false;
      }

      //validate if button has to be active
      var currentCls = contr.getItemCheckInButton().getCls();
      var activateButton = Ext.Array.contains(currentCls, 'wo-item-inactive');

      if (activateButton) {
        return false;
      }

      var workOrderPopUpNav = Ext.widget('workorderpopupnav');
      var technicianCounter = Ext.widget('technicianscounter');
      technicianCounter.setTitle("<span class='wo-title'>" + lang.base.title_questions + "</span>");
      var nextCounterButton = {
        xtype: 'button',
        text: lang.base.lbl_next,
        align: 'right',
        itemId: 'next-tech-counter-btn',
        cls: 'x-button-navigationbar'
      };
      workOrderPopUpNav.getNavigationBar().add(nextCounterButton);
      workOrderPopUpNav.add(technicianCounter);
      Ext.Viewport.add(workOrderPopUpNav);
      workOrderPopUpNav.show();

    });
  },
  onItemCloseOutJobButtonInitialize: function (comp) {
    var woRecord = this.getWoDetail().getRecord(), contr = this;

    comp.element.on('tap', function () {
      if (woRecord.get('status') == 'completed') {
        return false;
      }
      //verify if job requirements are complete
      if (contr.checkJobRequirementsProgress() != 'completed' || contr.getPrevious_check_in() == 'no') {
        return false;
      }

      var woSignatureField = Ext.widget('workordersignature');
      Ext.Viewport.add(woSignatureField);
      woSignatureField.show();

    });
  },
  //buttons
  onItemCheckOutButtonInitialize: function (comp) {
    var woReport = this.getWoDetail().getRecord();
    var contr = this;
    comp.element.on('tap', function (self) {

        //block action if wo is completed
        if (woReport.get('status') == 'completed') {
          return false;
        }

        //validate if button has to be active
        var currentCls = contr.getItemCheckOutButton().getCls();
        var activateButton = Ext.Array.contains(currentCls, 'wo-item-inactive');

        if (!activateButton) {
          contr.showWoCheckOutScreen(contr);
        }
      }
    );
  },
  showWoCheckOutScreen: function (contr) {
    var woReport = contr.getWoDetail().getRecord();
    //string selected company
    var companyData = [woReport.get('store_name'), woReport.get('latitude'), woReport.get('longitude'),
                       'checkOut'
    ];
    this.ios = new IOSDongle();
    this.ios.calliOSFunction('showWoCheckInScreen', companyData);
  },

  //fields
  clearAndDismissWoSearchfield: function () {
    var contr = this;
    //add current status filter to search field
    this.getWoSearchfield().setNewFilters(Ext.create('Ext.util.Filter', this.filterToApply(contr.getLastStatus())));

    var selected_wo_option = this.messagesByWoStatus(this.getLastStatus());
    this.getWoList().setEmptyText(lang.base.html_empty_wo.replace('{v1}', selected_wo_option));
  },
  onWoSearchfieldChanged: function () {
    var contr = this;
    //add current status filter to search field
    this.getWoSearchfield().setNewFilters(Ext.create('Ext.util.Filter', this.filterToApply(contr.getLastStatus())));
    this.getWoList().setEmptyText(lang.base.html_empty_wo_search);
  },
  messagesByWoStatus: function (statusList) {
    if (statusList.length == 1) {
      switch (statusList[0]) {
        case 'open-in-progress-wo':
          return lang.base.btn_active_wo;
          break;
        case 'completed-wo':
          return lang.base.btn_completed_wo;
          break;
        case 'expired-wo':
          return lang.base.btn_expired_wo;
          break;
        default :
          return '';
          break;
      }
    }
    return '';
  },
  //this function has the filter functionality to(pending-open&in_progress-Closed complete- Closed incomplete - declined)
  //if you need the basic filter functionality to (open&in_process-expired-completed ) use basicFilterFunctionality
  filterToApply: function (statusList) {
    var filter = null;
    //distance and scheduled_date filters are used to sort the work orders,
    // for that reason they are not gonna be into this function
    if (statusList.length == 1) {
      switch (statusList[0]) {
        case 'declined-wo':
          filter = {
            filterFn: function (item) {
              if (item.get('request_approval') == 'declined') {
                return true;
              }
              return false;
            }
          };
          return filter;
          break;
        case 'open-in-progress-wo':
          filter = {
            filterFn: function (item) {
              if (((item.get('status') == 'open' && item.get('request_approval') != 'declined') || item.get('status') == 'in_progress' ) && Math.round(new Date(item.get("expiration_date")) / 1000) > Math.round(Date.now() / 1000)) {
                return true;
              }
              return false;

            }
          };
          return filter;
          break;
        case 'completed-wo':
          filter = {
            filterFn: function (item) {
              if (item.get('status') == 'completed' && item.get('incomplete_equipment') == false) {
                return true;
              }
              return false;
            }
          };
          return filter;
          break;
        case 'incomplete-wo':
          filter = {
            filterFn: function (item) {
              if (item.get('status') == 'completed' && item.get('incomplete_equipment') == true) {
                return true;
              }
              return false;
            }
          };
          return filter;
          break;
      }
    } else if (statusList.length == 0) { // apply when there are not any filter
      filter = {
        filterFn: function (item) {
          return true;
        }
      };
      return filter;
    } else {
      //wo status filter
      filter = {
        filterFn: function (item) {
          if (item.get('request_approval') == 'declined') {
            return Ext.Array.contains(statusList, 'declined-wo');
          }
          else if ((((item.get('status') == 'open' && item.get('request_approval') != 'declined') || item.get("status") == 'in_progress' ) && Math.round(new Date(item.get("expiration_date")) / 1000) > Math.round(Date.now() / 1000))) {
            return Ext.Array.contains(statusList, 'open-in-progress-wo');
          }
          else if (item.get('status') == 'completed' && item.get('incomplete_equipment') == false) {
            return Ext.Array.contains(statusList, 'completed-wo');
          }
          else if (item.get('status') == 'completed' && item.get('incomplete_equipment') == true) {
            return Ext.Array.contains(statusList, 'incomplete-wo');
          }
          return Ext.Array.contains(statusList, 'expired-wo');

        }
      };

      return filter;
    }
  },
  sortWoByFilter: function (store, param) {
    store.setSorters({property: param, direction: 'ASC'});
  },
  //item list
  onWoItemTap: function (list, idx, el, record) {
    //add mask to allow the event loads wo detail view
    Ext.Viewport.setMasked({xtype: 'loadmask'});
    this.getWOData(record, this, this.loadWoDetailScreen);
  },
  loadWoDetailScreen: function (woRecord, contr) {
    var woDetailView = Ext.widget('workorderdetail');
    //change wo detail title and save wo id in woDetailView
    woDetailView.setTitle("<span class='wo-title'>" + woRecord.get('store_name') + "</span>");
    woDetailView.setWo_id("");
    woDetailView.setRecord("");
    woDetailView.setWo_id(woRecord.get('id'));
    woDetailView.setRecord(woRecord);

    var reportStore = Ext.getStore('Reports');
    var srQuery = 'filter[include][equipment]=equipmentType&filter[where][work_order_id]=' + woRecord.get('id');

    var proxy = reportStore.getProxy();
    proxy.setExtraParams({});
    proxy.setExtraParams({'filter': srQuery});

    reportStore.load(function () {
      reportStore.each(function (record, index) {
        record.set('cont', index + 1);
      });
    });
    contr.getWorkOrdersContainerNav().push(woDetailView);
  },
  getWOData: function (woRecord, contr, cb, showLoading) {
    Ext.Ajax.request({
      url: '/mobileserv/workorders/' + woRecord.get('id') + '/data',
      showLoading: Boolean(showLoading),
      success: function (res) {
        if (res.responseText != "") {
          woRecord.data.check_out = JSON.parse(res.responseText);
          if (cb) {
            cb(woRecord, contr);
          }
        }
      },
      failure: function (response) {
        var loadmask = Ext.Viewport.getMasked();
        if (loadmask) Ext.Viewport.remove(loadmask);

        var errorMessage = lang.base.lbl_error_loading;
        if (response != undefined) {
          var res = JSON.parse(response.responseText);
          errorMessage = res.error.message;
        }
        Ext.Msg.alert('', errorMessage);
      }
    });
  },
  onCheckConnection: function (cbSuccess, cbError, scope) {
    if (!scope) scope = this;
    Ext.Ajax.request({
      url: '/mobileserv/users/checkconnection',
      method: 'POST',
      scope: this,
      showLoading: false,
      success: function (res) {
        if (cbSuccess) {
          cbSuccess.call(scope);
          return;
        }
        var dongle = new IOSDongle();
        dongle.calliOSFunction("checkConnectionResult");
      },
      failure: function () {
        if (cbError) cbError.call(scope);
      }
    });
  },
  //response from native apps
  locationCheckIn: function (result) {
    var woLogStore = Ext.getStore('WorkOrderLogs');
    var woRecord = this.getWoDetail().getRecord();
    var woLogId = null, user_time_zone = null;
    //user time zone
    user_time_zone = Ext.Date.format(new Date(), 'T');

    if (result[2] == 'checkOut') {
      if (woRecord.get('check_out').closeoutjob) {
        woLogId = woRecord.get('check_out').closeoutjob.work_order_log_id;
      } else {
        woLogId = woRecord.get('check_out').survey.work_order_log_id;
      }
    }

    if (result) {

      var technicians_number = null;
      if (result[2] == "checkIn") {
        technicians_number = App.getController('TechniciansCounter').getTecnicianNumberValue();
      }

      // Filter store to get only the user's inactive check in logs
      var match = woLogStore.findBy(function (record) {
        if (record.get('user_id') == App.authUser.get('userId') && record.get('type') == 'checkIn' && record.get('status') == 'inactive') {
          return true;
        }
      });

      var data = {
        work_order_id: woRecord.get('id'),
        latitude: result[0],
        longitude: result[1],
        type: result[2],
        wo_log_id: woLogId,
        technicians_number: technicians_number,
        re_check_in: match != -1,
        user_time_zone: user_time_zone
      };

      Ext.Ajax.request({
        url: '/mobileserv/workorderlogs',
        method: 'POST',
        jsonData: data,
        scope: this,
        success: function (res) {
          this.checkInCheckOutResponse(this, result[2], woLogStore, woRecord);
        },
        failure: function (response) {
          var res = JSON.parse(response.responseText);
          Ext.Msg.alert('', res.error.message);
        }
      });
    }
  },
  checkInCheckOutResponse: function (contr, action, woLogStore, woRecord) {
    if (action == 'checkIn') {
      contr.getFieldsetWoLog().show();
      woLogStore.load(function (records, operation, success) {
        if (success) {
          if (records.length > 0) {
            contr.getFieldsetWoLog().show();
          } else {
            contr.getFieldsetWoLog().hide();
          }
          //check if user has previous check in
          contr.validateFistCheckIn(contr);
          woRecord.set('status', 'in_progress');

          // show IVR popup screen
          contr.showIVRPopup(woRecord.get('ivr'), true);
          return;
        }
      });
    } else {
      contr.getWorkOrdersContainerNav().pop(3);
      woRecord.set('status', 'completed');
      contr.setCurrentCoordinatesForWorkOrdersStore('App.getController(\'WorkOrders\').loadWoStoreSimple');
      Ext.Msg.alert('', lang.base.alert_success_save_check_out);

      // show IVR popup screen
      contr.showIVRPopup(woRecord.get('ivr'), false);
    }
  },
  showIVRPopup: function (ivr_data, show_msg) {
    if (ivr_data && ivr_data.required) {
      var IVRPopup = Ext.create('widget.workorderIVR', {
        notes: ivr_data.instructions,
        phone: ivr_data.phone_type,
        phoneNumber: ivr_data.phone_no,
        pin: ivr_data.pin,
        woNumber: ivr_data.wo_number
      });
      var IVRCloseBtn = IVRPopup.down('button[itemId=closeIVR]');
      IVRCloseBtn.on('tap', function () {
        IVRPopup.destroy();
        if (show_msg) Ext.Msg.alert('', lang.base.alert_success_save_check_in);

      }, this);
      Ext.Viewport.add(IVRPopup);
    } else {
      if (show_msg) Ext.Msg.alert('', lang.base.alert_success_save_check_in);
    }
  },

  validateFistCheckIn: function (contr) {

    if (contr == undefined) {
      contr = this;
    }
    var woReport = this.getWoDetail().getRecord();

    //block check in and check out in completed wo
    if (woReport.get('status') == 'completed') {
      contr.getItemCheckInButton().addCls('wo-item-inactive');
      contr.getItemCheckInButton().removeCls('wo-item-active');
      contr.getItemJobRequirementButton().addCls('wo-item-inactive');
      contr.getItemJobRequirementButton().removeCls('wo-item-active');
      contr.getItemCloseoutJobButton().addCls('wo-item-inactive');
      contr.getItemCloseoutJobButton().removeCls('wo-item-active');
      contr.getItemCheckOutButton().addCls('wo-item-inactive');
      contr.getItemCheckOutButton().removeCls('wo-item-active');
    } else {
      //check  if user has previous check in
      var match = Ext.getStore('WorkOrderLogs').findBy(function (record) {
        if (record.get('user_id') == App.authUser.get('userId') && record.get('type') == 'checkIn' && record.get('status') == 'complete') {
          return true;
        }
      });
      if (match == -1) {
        contr.setPrevious_check_in("no");
      } else {
        contr.setPrevious_check_in("yes");
      }
      //check in button
      if (contr.getPrevious_check_in() == "yes") {
        contr.getItemCheckInButton().addCls('wo-item-inactive');
        contr.getItemCheckInButton().removeCls('wo-item-active');
      } else {
        contr.getItemCheckInButton().addCls('wo-item-active');
        contr.getItemCheckInButton().removeCls('wo-item-inactive');
      }
      //job requirements
      contr.job_requirement_progress = contr.checkJobRequirementsProgress();

      if (contr.job_requirement_progress == "completed" || (contr.job_requirement_progress == "none" && contr.getPrevious_check_in() == "no")) {
        contr.getItemJobRequirementButton().addCls('wo-item-inactive');
        contr.getItemJobRequirementButton().removeCls('wo-item-active');
      } else {
        contr.getItemJobRequirementButton().addCls('wo-item-active');
        contr.getItemJobRequirementButton().removeCls('wo-item-inactive');
      }
      if (contr.job_requirement_progress == "none" || contr.getPrevious_check_in() == "no" || (woReport.get('check_out').closeoutjob && woReport.get('check_out').closeoutjob.data.signature && woReport.get('check_out').closeoutjob.data.signed_wo_picture && contr.job_requirement_progress == "completed")) {
        contr.getItemCloseoutJobButton().addCls('wo-item-inactive');
        contr.getItemCloseoutJobButton().removeCls('wo-item-active');
      } else {
        contr.getItemCloseoutJobButton().addCls('wo-item-active');
        contr.getItemCloseoutJobButton().removeCls('wo-item-inactive');
      }

      if (woReport.get('check_out').closeoutjob && woReport.get('check_out').closeoutjob.data.signature && woReport.get('check_out').closeoutjob.data.signed_wo_picture && contr.job_requirement_progress == "completed" && contr.getPrevious_check_in() == "yes") {
        contr.getItemCheckOutButton().addCls('wo-item-active');
        contr.getItemCheckOutButton().removeCls('wo-item-inactive');
      } else {
        contr.getItemCheckOutButton().addCls('wo-item-inactive');
        contr.getItemCheckOutButton().removeCls('wo-item-active');
      }

    }
  },
  checkJobRequirementsProgress: function () {
    var reportStore = Ext.getStore('Reports'),
      result = 'completed';
    reportStore.each(function (record, index) {
      if (record.get('equipment_photos') == undefined) {
        result = 'none';
        return false; //break loop
      } else {
        if (!record.get('equipment_photos').before || !record.get('equipment_photos').after) {
          result = 'none';
          return false; //break loop
        }
      }
      if (record.get('inspection_questions') == undefined) {
        result = 'none';
        return false; //break loop
      }
    });
    return result;
  },
  /*
   This function has the basic filter functionality (open-in_progress-complete)
   How to use:
   Override the function filterToApply in the WorkOrders Controller specific for the app you are working on
   filterToApply: function (statusList) {
   return this.basicFilterFunctionality(statusList);
   },
   **/
  basicFilterFunctionality: function (statusList) {
    var filter = null;
    //distance and scheduled_date filters are used to sort the work orders,
    // for that reason they are not gonna be into this function
    if (statusList.length == 1) {
      switch (statusList[0]) {
        case 'open-in-progress-wo':
          filter = {
            filterFn: function (item) {
              if ((item.get("status") == 'open' || item.get("status") == 'in_progress' ) && Math.round(new Date(item.get("expiration_date")) / 1000) > Math.round(Date.now() / 1000)) {
                return true;
              }
              return false;

            }
          };
          return filter;
          break;
        case 'completed-wo':
          filter = {
            filterFn: function (item) {
              if (item.get("status") == 'completed') {
                return true;
              }
              return false;

            }
          };
          return filter;
          break;
        case 'expired-wo':
          filter = {
            filterFn: function (item) {
              if (item.get("status") == 'open' && Math.round(new Date(item.get("expiration_date")) / 1000) > Math.round(Date.now() / 1000)) {
                return false;
              }
              if (item.get("status") == 'in_progress' && Math.round(new Date(item.get("expiration_date")) / 1000) > Math.round(Date.now() / 1000)) {
                return false;
              }
              if (item.get("status") == 'completed') {
                return false;
              }
              return true;
            }
          };
          return filter;
          break;
      }
    } else if (statusList.length == 0) { // apply when there are not any filter
      filter = {
        filterFn: function (item) {
          return true;
        }
      };
      return filter;
    } else {
      //wo status filter
      filter = {
        filterFn: function (item) {
          if (((item.get("status") == 'open' || item.get("status") == 'in_progress' ) && Math.round(new Date(item.get("expiration_date")) / 1000) > Math.round(Date.now() / 1000))) {
            return Ext.Array.contains(statusList, 'open-in-progress-wo');
          }
          if (item.get("status") == 'completed') {
            return Ext.Array.contains(statusList, 'completed-wo');
          } else {
            return Ext.Array.contains(statusList, 'expired-wo');
          }
          return false;
        }
      };

      return filter;
    }
  },
  activateDeactivateButton: function (button, result) {
    if (!button) return;
    if (result) {
      button.addCls('wo-item-inactive');
      button.removeCls('wo-item-active');
    } else {
      button.addCls('wo-item-active');
      button.removeCls('wo-item-inactive');
    }
  }
});

