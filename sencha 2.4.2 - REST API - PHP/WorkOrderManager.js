Ext.define('Base.view.workorder.WorkOrderManager', {
  extend: 'Ext.Container',
  xtype: 'workordermanager',
  config: {
    layout: 'vbox',
    scrollable: null,
    title: lang.base.title_wo_manager,
    cls: 'wo-manager-container',
    items: [
      {
        xtype: 'workordersmanageroptions'
      },
      {
        xtype: 'fieldset',
        itemId: 'fieldsetWoLog',
        scrollable: null,
        margin: '0 0 0 0',
        cls: 'fieldset-wo-manager',
        title: lang.base.title_wo_manager_logs,
        layout: 'fit',
        flex: 1,
        items: [
          {
            xtype: 'slidedraglist',
            store: 'WorkOrderLogs',
            name: 'woList',
            height: '100%',
            grouped: false,
            pinHeaders: false,
            indexBar: false,
            disableSelection: true,
            scrollable: true,
            flex: true,
            deferEmptyText: false,
            emptyText: "",
            plugins: [
              {
                xclass: 'Ext.ux.touch.PullRefreshFn',
                cls: 'pull-down-refresh-container',
                pullText: lang.base.lbl_pull_down_refresh,
                refreshFn: function () {
                  Ext.getStore('WorkOrderLogs').load();
                }
              }
            ],
            itemTpl: [
              '<div class="table" style="width: 100%">' +
              '<div class="row row-space">' +
              '<div class="cell cell-icon" style="width: 3.5em"></div>' +
              '<div class="cell" style="width: 100%">' +
              '<div class="row row-space" style="width: 100% ;display: table;table-layout: fixed;">' +
              '<div class="cell text-left cell-font-big" style="width: 60%">{[this.formatType(values)]}</div>' +
              '<div class="cell text-right no-truncate" style="min-height: 6em !important;">{[this.formatWoDate(values,"D")]}</div>' +
              '</div>' +
              '<div class="row row-space" style="width: 100% ;display: table;table-layout: fixed;">' +
              '<div class="cell text-left" style="width: 65% !important">{technician_name}</div>' +
              '<div class="cell text-right no-truncate" >{[this.formatWoDate(values,"H")]}</div>' +
              '</div>' +
              '</div>' +
              '</div>' +
              '</div>',
              {
                // Template functions
                // Returns a formatted date for wo
                formatWoDate: function (record, section_date) {
                  var dateObj = new Date(parseInt(record.date_created) * 1000);
                  date = Ext.Date.format(dateObj, 'm/d/Y');
                  time = Ext.Date.format(dateObj, 'g:i A');

                  if (section_date == "D") {
                    return date
                  } else if (section_date == "H") {
                    return time;
                  } else {
                    return date + " " + time;

                  }
                },
                formatType: function (record) {
                  switch (record.type) {
                    case 'checkIn':
                      return lang.base.title_check_in;
                      break;
                    case 'checkOut':
                      return lang.base.title_check_out;
                      break;
                    default:
                      return record.type;
                      break;
                  }
                }
              }
            ]
          }
        ]
      }
    ]
  }
})
;
