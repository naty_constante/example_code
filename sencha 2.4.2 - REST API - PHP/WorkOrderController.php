<?php

namespace ad\FTC;

use Httpful\Mime;
use \MABI\RESTModelController;
use MongoDuplicateKeyException;
use MABI\EmailSupport\Mandrill;
use MABI\EmailSupport\TokenTemplate;
use template\Htmlparser;

include_once __DIR__ . '/../middleware/SessionHeader.php';
include_once __DIR__ . '/../mabi/extensions/EmailSupport/Mandrill.php';
include_once __DIR__ . '/../mabi/extensions/EmailSupport/TokenTemplate.php';
include_once __DIR__ . '/../../template/Htmlparser.php';

/**
 * @Docs\ShowModel
 *
 * Manages the endpoints that encompass all elements of the work order details,
 * which include, but are not limited to, the blanket authorization of location,
 * time record, status, instructions and call to action for a work order.
 * @middleware("ad\FTC\SessionHeader")
 * @middleware("\MABI\Identity\Middleware\SessionOnlyAccess")
 * @middleware("\MABI\Middleware\SharedSecret")
 */
class WorkOrderController extends RESTModelController {

  /**
   * Retrieves all work orders for the authenticated user
   * @Docs\Param("filter",type="string",location="query",required=false,description=" The filter expressions by default is field1=value1&field2=value2&field3=value3... ")
   * @Docs\Param("fields",type="string",location="query",required=false,description=" The fields expressions by default is field1,field2,field3... ")
   */
  public function get() {
    if ($this->getApp()->getRequest()->apiApplication->applicationName == BRIGHTVIEW_APP_NAME) {
      return $this->getBrightView();
    }


    if ($this->getApp()->getRequest()->apiApplication->applicationName == BRINCO_APP_NAME) {
      $this->syncWorkOrdersWithBrincoApi();
    }

    $query  = [];
    $filter = $this->getApp()->getRequest()->get('filter');
    if (!empty($filter)) {
      parse_str($filter, $filterArray);
      //build query filter
      foreach ($filterArray as $key => $filterParam) {
        if ($key == 'id') {
          $query['query']['_id'] = new \MongoId($filterParam);
        }
        elseif (is_numeric($filterParam) && in_array($key, [
            'latitude',
            'longitude',
            'date_created',
            'scheduled_date',
            'expiration_date',
            'call_date'
          ])
        ) {
          $query['query'][$key] = in_array($key, [
            'latitude',
            'longitude'
          ]) ? (float) $filterParam : (int) $filterParam;
        }
        elseif (strtolower($filterParam) === "null" || empty($filterParam)) {
          $query['query'][$key] = NULL;
        }
        else {
          $query['query'][$key] = $filterParam;
        }
      }
    }

    //$GET VALUES
    $currentLatitude  = $this->getApp()->getRequest()->get('currLatitude');
    $currentLongitude = $this->getApp()->getRequest()->get('currLongitude');

    $inst = WorkOrder::init($this->getApp());
    $sort = array('scheduled_date' => 1);

    $query['query']['company_id'] = $this->getApp()->getRequest()->session->user->company_id;

    if ($this->getApp()->getRequest()->apiApplication->applicationName == CORNELL_APP_NAME) {
      if ($this->getApp()->getRequest()->get('woRequest')) {
        $query['query'] = [
          'company_id'       => $this->getApp()->getRequest()->session->user->company_id,
          'request_approval' => NULL
        ];
      }
      elseif ($this->getApp()->getRequest()->get('requestApproval')) {
        $request_approval                   = explode(',', $this->getApp()->getRequest()->get('requestApproval'));
        $query['query']['request_approval'] = ['$in' => $request_approval];
      }
    }

    if ($this->getApp()->getRequest()->apiApplication->applicationName == BRIGHTVIEW_APP_NAME) {
      $limitCallDate = date('Y-m-d H:i:s', strtotime('-2 weeks'));

      $query['query']['call_date'] = ['$gt' => strtotime($limitCallDate)];
      // Get only wo that have a call date that falls within the last two week period.
    }

    if ($this->getApp()->getRequest()->apiApplication->applicationName == BRINCO_APP_NAME) {
      $scheduledDate = date('Y-m-d 00:00:00', strtotime('-1 weeks'));

      $query['query'] = [
        '$and' => [
          ['company_id' => $this->getApp()->getRequest()->session->user->company_id],
          [
            '$or' => [
              [
                '$and' => [
                  ['scheduled_date' => ['$gt' => strtotime($scheduledDate)]],
                  ['status' => 'completed']
                ]
              ],
              ['status' => ['$ne' => 'completed']]
            ]
          ]
        ]
      ];
    }

    if ($this->getApp()->getRequest()->apiApplication->applicationName == RAEL_APP_NAME) {
      $siteId                     = $this->getApp()->getRequest()->get('location_id');
      $query['query']['user_ids'] = ['$all' => [$this->getApp()->getRequest()->session->userId]];
      $query['query']['store_id'] = ['$all' => [$siteId]];
    }

    $query['sort'] = $sort;

    $outputArr    = [];
    $workOrder_bn = [];
    foreach ($inst->query($query) as $foundModel) {
      $aux = $foundModel->getPropertyArray(TRUE);

      //save the distance from current user location to the stores
      $util            = new Util();
      $distance_unit   = 'M';
      $aux['distance'] = $util->calculateDistanceBetweenCoordinates($currentLatitude, $currentLongitude, $aux['latitude'], $aux['longitude'], $distance_unit, 2);

      //if $aux['distance'] is invalid it will be equal to '', 0 can be considered as a valid value for $aux['distance']
      $aux['distance_unit'] = $aux['distance'] !== '' ? strtolower($distance_unit) : '';

      $workOrder_bn[] = $foundModel->scheduled_date;

      if ($this->getApp()->getRequest()->apiApplication->applicationName == CORNELL_APP_NAME) {

        try {
          //get incomplete wo
          $equiptid           = new Equiptid($this->getApp());
          $incompleteWos      = $equiptid->callGET('Reports/incompleteWos');
          $incompleteWosArray = [];

          foreach ($incompleteWos->result as $incompleteWos) {
            $incompleteWosArray[$incompleteWos->_id] = $incompleteWos->count;
          }
        } catch (\Exception $exception) {
          $this->getApp()->returnError($exception->getMessage());
        }
        $aux['incomplete_equipment'] = isset($incompleteWosArray[$foundModel->getId()]) ? TRUE : FALSE;
      }

      //remove the fields that wasn't requested
      $fields = $this->getApp()->getRequest()->get('fields');
      if (!empty($fields)) {
        $fieldsArray = explode(',', $fields);
        foreach ($aux as $k => $value) {
          if (!in_array($k, $fieldsArray)) {
            unset($aux[$k]);
          }
        }
      }

      $outputArr[] = $aux;
    }

    $outputArrUnOrdered = $outputArr;
    $outputArr          = [];

    // Order work order in a natural case insensitive order
    natcasesort($workOrder_bn);
    foreach ($workOrder_bn as $k => $o_wo) {
      $outputArr[] = $outputArrUnOrdered[$k];
    }

    echo json_encode($outputArr);
  }

  /**
   * Returns all the work orders for brightview. Requires a locationId and a trade.
   * It calls Service Channel API, formats the returned work orders and
   * returns it as an array of JSON objects.
   */
  public function getBrightView() {
    $scAPI = new ServiceChannel($this->getApp());
    $scAPI->setAPIVersion(3);
    $skip        = 0;
    $locationId  = $this->getApp()->getRequest()->get('locationId');
    $woTrade     = $this->getApp()->getRequest()->get('trade');
    $tradeFilter = Util::getWoTradeFilter($this->getApp(), !$woTrade || strstr($woTrade, 'landscape') ? LAND_TRADE : SNOW_TRADE);
    $today       = date('Y-m-d\TH:i:s.uP');
    $nextWeek    = date('Y-m-d\TH:i:s.uP', strtotime('+1 week'));
    $filter      = "{$tradeFilter} and Location/Id eq $locationId and CallDate lt $nextWeek and ExpirationDate gt $today and (Status/Primary eq 'OPEN' or Status/Primary eq 'IN PROGRESS')";
    $rawWos      = [];
    do {
      $scResponse = $scAPI->callGET('workorders', [
        '$expand'  => 'Location,Subscriber,Provider',
        '$filter'  => $filter,
        '$skip'    => $skip,
        '$orderby' => 'CallDate desc',
        '$count'   => 'true'
      ]);
      if (is_array($scResponse->value) && count($scResponse->value) > 0) {
        $rawWos = $rawWos + $scResponse->value;
        $skip += 50;
      }
    } while ($scResponse->{'@odata.count'} > $skip);

    echo json_encode($this->_formatScWorkOrders($rawWos));
  }

  /**
   * Synchronizes a work order that was stored on the device's local storage
   */
  public function postSyncSave() {
    $input = json_decode($this->getApp()->getRequest()->getBody());
    $this->_saveServiceChannelWO([$input->external_data]);
    $workOrder = WorkOrder::init($this->getApp());
    $workOrder->findByField('external_id', (int) $input->external_id);
    echo json_encode($workOrder->getPropertyArray(TRUE));
  }

  /**
   * @Docs\Name Synchronizes an array of service Channel Work Order Objects and returns the synced WOs in an array
   *
   * ~~~
   * [{
   *  "ContractInfo": {
   *  "SubscriberId": "int",
   *  "LocationId": "int",
   *  "ProviderId": "int",
   *  "TradeName": "string"
   *  },
   * "Category": "string",
   * "Priority": "string",
   * "Nte": "int",
   * "CallerName": "string",
   * "CallDate": "string",
   * "ScheduledDate": "string",
   * "ProblemSource": "string",
   * "Description": "string",
   * "ProblemCode": "string",
   * "Status": {
   *  "Primary": "string",
   *  "Extended": "string"
   *  }
   * }]
   * ~~~
   *
   * @Docs\Param("body",type="string",required=true,description="An array of Service channel Work Order objects")
   *
   * @return array
   */
  public function postSync() {
    $input        = json_decode($this->getApp()->getRequest()->getBody());
    $externalData = [];
    $externalIds  = [];
    foreach ($input as $wo) {
      $externalData[] = $wo->external_data;
      $externalIds[]  = $wo->external_id;
    }

    $this->_saveServiceChannelWO($externalData);

    $outputWoArr = [];
    foreach (WorkOrder::init($this->getApp())
               ->query(['query' => ['external_id' => ['$in' => $externalIds]]]) as $foundWo) {
      $outputWoArr[] = $foundWo->getPropertyArray(TRUE);
    }

    $this->setWorkOrderDependencies($outputWoArr);

    echo json_encode($outputWoArr);
  }

  /**
   * Adds logs and log data to every work order on the given array
   *
   * @param array $workOrdersArr
   */
  private function setWorkOrderDependencies(Array &$workOrdersArr) {
    $woLogController = new WorkOrderLogController($this->getExtension());
    foreach ($workOrdersArr as &$wo) {
      $woModel = WorkOrder::init($this->getApp());
      $woModel->findById($wo['id']);
      $this->model = $woModel;
      $woLogs      = $woLogController->searchWoLogs($wo['id'], ['complete', 'incomplete', 'inactive']);
      foreach ($woLogs as &$woLog) {
        $woLog['data'] = $this->returnWoData(FALSE);
      }
      $wo['logs'] = $woLogs;
    }
  }

  /**
   * Synchronizes all the work orders retrieved from Brinco API
   */
  private function syncWorkOrdersWithBrincoApi() {

    try {

      $brincoAPI      = new BrincoApi($this->getApp());
      $brincoResponse = $brincoAPI->callGET('WorkOrders');

      if (is_array($brincoResponse) && count($brincoResponse) > 0) {
        $this->_saveBrincoWO($brincoResponse);
      }
    } catch (Exception $ex) {
    }//we are going to ignore the error from Brinco api and display the local WOs
  }


  /**
   *
   * Retrieve the data and logs associated to a specific Work Order by Application.
   *
   * @return array
   */
  public function restGetData($id) {
    $appName = $this->getApp()->getRequest()->apiApplication->applicationName;

    switch ($appName) {
      case BRIGHTVIEW_APP_NAME:
        $this->_getWoData();
        break;
      case COMMERCIALFIRE_APP_NAME:
        $this->_getWoData();
        break;
      case CORNELL_APP_NAME:
        $this->_getWoDataEquipments();
        break;
      case BRINCO_APP_NAME:
        $this->_getWoData();
        break;
      case RAEL_APP_NAME:
        $this->_checkAccessToWorkOrder($this->model);
        $this->_getWoData();
        break;
      case AMERICANMAINTENANCE_APP_NAME:
        $this->_getWoData();
        break;
      default:
        $this->getApp()->returnError(Errors::$ACCESS_DENIED);
        break;
    }
  }

  /**
   * Retrieve the data and logs associated to a specific Work Order.
   *
   * @return array
   */
  private function _getWoData() {
    echo $this->returnWoData(TRUE);
  }

  /**
   * Returs the data and logs associated to the current loaded Work Order.
   *
   * @param bool $encodeResponse
   *
   * @return null|string
   */
  private function returnWoData($encodeResponse = TRUE) {
    $woCheckOutData = NULL;
    $appName        = $this->getApp()->getRequest()->apiApplication->applicationName;
    //Get work order check out log
    $woLog = WorkOrderLog::init($this->getApp());

    $query_wolog['query'] = [
      'work_order_id' => $this->model->getId(),
      'type'          => 'checkOut',
      'status'        => ['$ne' => 'inactive']
    ];
    if ($appName == BRIGHTVIEW_APP_NAME) {
      $query_wolog          = NULL;
      $query_wolog['query'] = [
        'work_order_id' => $this->model->getId(),
        'user_id'       => $this->getApp()->getRequest()->session->userId,
        'type'          => 'checkOut',
        'status'        => ['$ne' => 'inactive']
      ];
    }
    $outputWoLogArr = [];
    foreach ($woLog->query($query_wolog) as $foundModelLog) {
      $outputWoLogArr[] = $foundModelLog->getPropertyArray(TRUE);
    }

    if (count($outputWoLogArr) > 0) {

      //Get survey
      $woDataSurvey           = WorkOrderData::init($this->getApp());
      $query_wo_data['query'] = [
        'work_order_log_id' => $outputWoLogArr[0]['id'],
        'type'              => "survey"
      ];
      $outputWoDataSurveyArr  = [];
      foreach ($woDataSurvey->query($query_wo_data) as $foundModelData) {
        $outputWoDataSurveyArr[] = $foundModelData->getPropertyArray(TRUE);
      }

      //Get photos
      $woDataPhoto            = WorkOrderData::init($this->getApp());
      $query_wo_data['query'] = [
        'work_order_log_id' => $outputWoLogArr[0]['id'],
        'type'              => "photos"
      ];
      $outputWoDataPhotoArr   = [];
      foreach ($woDataPhoto->query($query_wo_data) as $foundModelDataPhoto) {
        $outputWoDataPhotoArr[] = $foundModelDataPhoto->getPropertyArray(TRUE);
      }

      $woCheckOutData['survey'] = isset($outputWoDataSurveyArr[0]) ? $outputWoDataSurveyArr[0] : NULL;

      if (!empty($outputWoDataPhotoArr[0])) {
        $woCheckOutData['photos'] = isset($outputWoDataPhotoArr[0]) ? $outputWoDataPhotoArr[0] : NULL;
      }
      else {

        $woCheckOutData['photos'] = NULL;

        //Get photo before
        $woDataPhotoBefore               = WorkOrderData::init($this->getApp());
        $queryWoDataPhotoBefore['query'] = [
          'work_order_log_id' => $outputWoLogArr[0]['id'],
          'type'              => 'photos_before'
        ];
        $outputWoDataPhotoBeforeArr      = [];
        foreach ($woDataPhotoBefore->query($queryWoDataPhotoBefore) as $foundModelDataPhotoBefore) {
          $outputWoDataPhotoBeforeArr[] = $foundModelDataPhotoBefore->getPropertyArray(TRUE);
        }

        if (!empty($outputWoDataPhotoBeforeArr)) {
          $woCheckOutData['photos']['photos_before'] = $outputWoDataPhotoBeforeArr[0];
        }

        //Get photo after
        $woDataPhotoAfter               = WorkOrderData::init($this->getApp());
        $queryWoDataPhotoAfter['query'] = [
          'work_order_log_id' => $outputWoLogArr[0]['id'],
          'type'              => 'photos_after'
        ];
        $outputWoDataPhotoAfterArr      = [];
        foreach ($woDataPhotoAfter->query($queryWoDataPhotoAfter) as $foundModelDataPhotoAfter) {
          $outputWoDataPhotoAfterArr[] = $foundModelDataPhotoAfter->getPropertyArray(TRUE);
        }

        if (!empty($outputWoDataPhotoAfterArr)) {
          $woCheckOutData['photos']['photos_after'] = $outputWoDataPhotoAfterArr[0];
        }
      }

      //Get resolution notes (commercial fire )
      if ($this->getApp()->getRequest()->apiApplication->applicationName == COMMERCIALFIRE_APP_NAME) {
        $woDataResolutionNotes               = WorkOrderData::init($this->getApp());
        $queryWoDataResolutionNotes['query'] = [
          'work_order_log_id' => $outputWoLogArr[0]['id'],
          'type'              => 'resolution_notes'
        ];
        $outputWoDataResolutionNotesArr      = [];
        foreach ($woDataResolutionNotes->query($queryWoDataResolutionNotes) as $foundModelDataResolution) {
          $outputWoDataResolutionNotesArr[] = $foundModelDataResolution->getPropertyArray(TRUE);
        }

        $woCheckOutData['resolution_notes'] = !empty($outputWoDataResolutionNotesArr) ? $outputWoDataResolutionNotesArr[0] : NULL;
      }
    }
    else {
      $woCheckOutData['survey'] = NULL;
      $woCheckOutData['photos'] = NULL;
    }

    if ($this->getApp()->getRequest()->apiApplication->applicationName == COMMERCIALFIRE_APP_NAME || $this->getApp()
        ->getRequest()->apiApplication->applicationName == RAEL_APP_NAME
    ) {
      //get closeoutjob
      $woCheckOutData = $woCheckOutData + $this->_getSignature();
    }

    return $encodeResponse ? json_encode($woCheckOutData) : $woCheckOutData;
  }

  /**
   * Cornell function
   *
   * Retrieve the data work order log (closeoutjob) associated to a specific Work Order.
   * @Docs\Param("body",type="string",location="query",required=true,description="WorkOrder id")
   * @return array
   */
  private function _getWoDataEquipments() {

    echo json_encode($this->_getSignature());
  }

  /*
   * Retrieve the data (closeoutjob) associated to a specific Work Order.
   * @return array
   */
  private function _getSignature() {
    $woCheckOutData = NULL;
    //Get work order check out log
    $woLog                = WorkOrderLog::init($this->getApp());
    $query_wolog['query'] = array(
      'work_order_id' => $this->model->getId(),
      'type'          => 'checkOut',
      'status'        => ['$ne' => 'inactive']
    );
    $outputWoLogArr       = [];
    foreach ($woLog->query($query_wolog) as $foundModelLog) {
      $outputWoLogArr[] = $foundModelLog->getPropertyArray(TRUE);
    }

    if (count($outputWoLogArr) > 0) {
      //Get closeoutjob
      $woDataCloseOutJob          = WorkOrderData::init($this->getApp());
      $query_wo_data['query']     = [
        'work_order_log_id' => $outputWoLogArr[0]['id'],
        'type'              => "closeoutjob"
      ];
      $outputWoDataCloseOutJobArr = [];
      foreach ($woDataCloseOutJob->query($query_wo_data) as $foundModelData) {
        $outputWoDataCloseOutJobArr[] = $foundModelData->getPropertyArray(TRUE);
      }

      $woCheckOutData['closeoutjob'] = isset($outputWoDataCloseOutJobArr[0]) ? $outputWoDataCloseOutJobArr[0] : NULL;
    }
    else {
      $woCheckOutData['closeoutjob'] = NULL;
    }
    return $woCheckOutData;
  }

  /**
   * @Docs\Name Creates a wo by calling POST /workorders
   *
   * Brightview - Creates a WO on Service Channel system and syncs it with Automated Decision
   *
   * ~~~
   * {
   *  "locationId":0,
   *  "equipmentType":"string",
   *  "problemCode":"string",
   *  "description":"string"
   * }
   * ~~~
   *
   * Cornell - Inserts a work order.
   * ~~~
   * {
   *  "scheduled_date" : 0,
   *  "expiration_date" : 0,
   *  "notes" : "string",
   *  "status" : "string",
   *  "address" : "string",
   *  "call_slip" : string,
   *  "customer_po" : string,
   *  "store_name" : "string",
   *  "store_id" : "string"
   *  "priority" : "string",
   *  "latitude" : Double,
   *  "longitude" : Double,
   *  "company_id" : "string",
   *  "general_notes" : "string",
   *  "ivr" : {
   *    "required" : boolean,
   *    "instructions" : "string",
   *    "phone_type" : "string",
   *    "phone_no" : "string",
   *    "pin" : "string",
   *    "wo_number” : "string"
   *  },
   *  "equipment": [
   *   {
   *    "customer_unique_id": "string",
   *    "equipment_problem": "string"
   *    }
   *   ]
   * }
   * ~~~
   * Rael - Inserts a work order.
   * ~~~
   * {
   *  "scheduled_date" : 0,
   *  "expiration_date" : 0,
   *  "notes" : "string",
   *  "status" : "string",
   *  "address" : "string",
   *  "call_slip" : "string",
   *  "customer_po" : "string,
   *  "store_name" : "string",
   *  "store_id" : "string"
   *  "priority" : "string",
   *  "latitude" : Double,
   *  "longitude" : Double,
   *  "company_id" : "string",
   *  "general_notes" : "string",
   *  "contact_person" : ["string","string"]
   *  "user_ids": Array,
   *  "area_id": "string"
   * }
   * ~~~
   *
   * @Docs\Param("body",type="string",location="body",required=true,description="WorkOrder JSON parameters")
   */
  public function post() {
    $appName = $this->getApp()->getRequest()->apiApplication->applicationName;

    // equipment data fields - see structure in Equipment post
    switch ($appName) {
      case BRIGHTVIEW_APP_NAME:
        $this->_saveWorkOrderBrigthview();
        break;
      case CORNELL_APP_NAME:
        $this->_saveWorkOrderCornell();
        break;
      case COMMERCIALFIRE_APP_NAME:
        $this->_saveWorkOrderGeneral();
        break;
      case RAEL_APP_NAME:
        $this->_saveWorkOrderWithAssignedUsers();
        break;
      case AMERICANMAINTENANCE_APP_NAME:
        $this->_saveWorkOrderGeneral();
        break;
      default:
        $this->getApp()->returnError(Errors::$ACCESS_DENIED);
        break;
    }
  }

  /**
   * Creates a WO Log
   *
   * @param array $input
   *
   * @return string|null
   */
  public function getWorkOrderLog($input) {
    //get check out log (if it exist)
    $inst = WorkOrderLog::init($this->getApp());

    if ($this->getApp()->getRequest()->apiApplication->applicationName == BRIGHTVIEW_APP_NAME) {
      $query = [
        'query' => [
          'work_order_id' => $input['work_order_id'],
          'status'        => ['$in' => ['incomplete', 'complete']],
          'type'          => 'checkOut',
          'user_id'       => $this->getApp()->getRequest()->session->userId
        ],
        'sort'  => ['date_created' => -1]
      ];
    }
    else {
      $query = [
        'query' => [
          'work_order_id' => $input['work_order_id'],
          'status'        => ['$in' => ['incomplete', 'complete']],
          'type'          => 'checkOut',
        ],
        'sort'  => ['date_created' => -1]
      ];
    }

    $outputArr = array();
    foreach ($inst->query($query) as $foundModel) {
      if ($foundModel->status == 'complete') {
        $foundModel->status = 'inactive';
        $foundModel->save();
        continue;
      }
      $outputArr[] = $foundModel->getPropertyArray(TRUE);
    }

    $workOrderLogId = NULL;
    if (count($outputArr) > 0) {
      $workOrderLogId = $outputArr[0]['id'];
    }
    else {
      //save work order log new
      $workOrderLog                = WorkOrderLog::init($this->getApp());
      $workOrderLog->work_order_id = $input['work_order_id'];
      $workOrderLog->type          = 'checkOut';
      $workOrderLog->status        = 'incomplete';
      $workOrderLog->date_created  = time();
      $workOrderLog->user_id       = $this->getApp()->getRequest()->session->userId;
      $workOrderLog->insert();

      $workOrderLogId = $workOrderLog->id;
    }

    return $workOrderLogId;
  }

  /**
   * Save an inspection question survey.
   * Check for a work order log with status incomplete and type checkOut linked to that work order and user,
   * if it doesn't exist we create a new work order log.
   * ~~~
   * {
   *  "work_order_id": string
   * }
   * ~~~
   * @Docs\Param("body",type="string",location="body",required=true,description="Work order ID")
   * @return Returns the work order data
   */
  public function postSurvey() {

    $input = json_decode($this->getApp()->getRequest()->getBody(), TRUE);

    $workOrderLogId = $this->getWorkOrderLog($input);

    //verify if a survey has already been created for the WorkOrderLog
    //get inspection question data
    $instWorkOrderData = WorkOrderData::init($this->getApp());

    $query['query'] = array(
      'work_order_log_id' => $workOrderLogId,
      'type'              => 'survey'
    );

    $outputWorkOrderLogArr = array();
    foreach ($instWorkOrderData->query($query) as $foundModel) {
      $outputWorkOrderLogArr[] = $foundModel->getPropertyArray(TRUE);
    }
    if (count($outputWorkOrderLogArr) > 0) {
      //update work order data (type survey)
      $workOrderData = WorkOrderData::init($this->getApp());
      $workOrderData->findById($outputWorkOrderLogArr[0]['id']);
      $workOrderData->loadFromExternalSource($input);
      $workOrderData->work_order_log_id = $workOrderLogId;
      $workOrderData->date_created      = time();
      $workOrderData->user_id           = $this->getApp()->getRequest()->session->userId;
      $workOrderData->save();
    }
    else {
      //save work order data (type survey)
      $workOrderData = WorkOrderData::init($this->getApp());
      $workOrderData->loadFromExternalSource($input);
      $workOrderData->work_order_log_id = $workOrderLogId;
      $workOrderData->date_created      = time();
      $workOrderData->user_id           = $this->getApp()->getRequest()->session->userId;
      $workOrderData->insert();
    }

    echo json_encode($workOrderData);
  }


  /**
   * Updates report information - save inspection questions data in EQUIPT ID DATABASE.
   *
   * @return Returns the report data
   */
  public function postSurveyEquipment() {

    $input = json_decode($this->getApp()->getRequest()->getBody(), TRUE);

    $filter = '?filter[where][and][0][work_order_id]=' . $input['work_order_id'] . '&' . 'filter[where][and][1][equipment_id]=' . $input['equipment_id'];

    $equiptid = new Equiptid($this->getApp());
    $woDetail = $equiptid->callGET('Reports' . $filter);

    $params = [
      'inspection_questions' => $input['survey_answers'],
      'survey_id'            => $input['survey_id']
    ];

    if (!empty($woDetail)) {
      $reports = $equiptid->callPUT('Reports/' . $woDetail[0]->id, $params);
    }
    else {
      $this->getApp()->returnError(Errors::$UNDEFINED_RESULT);
    }

    echo json_encode($reports);
  }

  /**
   * Get a work order by the external id
   * ~~~
   * {
   *  "external_id": string with the 3rd party ID
   * }
   * ~~~
   * @Docs\Param("external_id",type="string",location="url",description="String with the 3rd party ID",required=true)
   * Return a WO object.
   */
  public function getByExternalID() {
    $external_id = $this->getApp()->getRequest()->get('external_id');
    if (is_null($external_id) || empty($external_id)) {
      $this->getApp()->returnError(Errors::$MISSING_WO_ID);
    }
    $workOrder = WorkOrder::init($this->getApp());
    if ($workOrder->findByField('external_id', (int) $external_id)) {
      echo $workOrder->outputJSON();
    }
    else {
      $this->getApp()->returnError(Errors::$WORKORDER_NOT_FOUND);
    }
  }

  /**
   * Gets the photos of a work order
   * ~~~
   * {
   *  "latest": bool
   * }
   * ~~~
   * @Docs\Param("body",type="string",location="query",description="Flag to get the last photo taken or all of them")
   * - - -
   * Return Photos' complete URL
   */
  private function callGetPhotos($id, $type) {
    $photo           = WorkOrderPhoto::init($this->getApp());
    $latest          = $this->getApp()->getRequest()->params('latest');
    $applicationName = $this->getApp()->getRequest()->apiApplication->applicationName;
    $photos          = [];

    //get check out log (if it exist)
    $inst = WorkOrderLog::init($this->getApp());

    if ($applicationName == 'Brightview') {
      $queryWo['query'] = [
        'work_order_id' => $id,
        'status'        => 'incomplete',
        'type'          => 'checkOut',
        'user_id'       => $this->getApp()->getRequest()->session->userId
      ];
    }
    else {
      $queryWo['query'] = [
        'work_order_id' => $id,
        'status'        => 'incomplete',
        'type'          => 'checkOut'
      ];
    }

    $outputArr = array();
    foreach ($inst->query($queryWo) as $foundModel) {
      $outputArr[] = $foundModel->getPropertyArray(TRUE);
    }

    if (count($outputArr) > 0) {
      $workOrderLogId = $outputArr[0]['id'];
      $query          = [
        'query' => ['work_order_log_id' => $workOrderLogId, 'type' => $type],
        'sort'  => ['creation_date' => -1]
      ];
      if ($latest) {
        $query['limit'] = 1;
      }
      foreach ($photo->query($query) as $photo) {
        $photos[] = $photo->getCollapsedArray();
      }
    }
    echo json_encode($photos);
  }

  /**
   * Gets the closeout job data of a work order
   */
  public function restGetPhotos($id) {
    //get photos before or after - this varible is used in commercial fire
    $photos_type = $this->getApp()->getRequest()->params('type');

    return $this->callGetPhotos($id, !empty($photos_type) ? $photos_type : 'photos');
  }

  /**
   * Gets the photos of a work order
   */
  public function restGetCloseOutJob($id) {
    return $this->callGetPhotos($id, 'closeoutjob');
  }

  /**
   * Saves photos  of a Work Order.
   * ~~~
   * {
   * "work_order_log_id": string,
   * "photo_1": string,
   * "photo_2": string
   * "photo_3": string
   * "photo_4": string
   * }
   * ~~~
   * Find the wo log associated to the user and work order, and create a WorkOrderData record with the photos.
   * @Docs\Param("body",required=true,description="Work order log ID, and photos' data in base64")
   * - - -
   * Return Photo data
   */
  public function restPostPhotos($id) {
    $input           = json_decode($this->getApp()->getRequest()->getBody(), TRUE);
    $applicationName = $this->getApp()->getRequest()->apiApplication->applicationName;

    //type is sent in commercial fire app
    $type = !empty($input['type']) ? $input['type'] : 'photos';

    $input['work_order_id'] = $id;
    $workOrderLogId         = $this->getWorkOrderLog($input);

    $userId            = $this->getApp()->getRequest()->session->userId;
    $photos['photo_1'] = $input['photo_1'];
    $photos['photo_2'] = $input['photo_2'];
    $photos['photo_3'] = $input['photo_3'];
    $photos['photo_4'] = $input['photo_4'];

    //verify if work order photo already exists
    $inst           = WorkOrderPhoto::init($this->getApp());
    $query['query'] = [
      'work_order_log_id' => $workOrderLogId,
      'type'              => $type
    ];
    if ($applicationName == BRIGHTVIEW_APP_NAME) {
      $query['query'] = [
        'work_order_log_id' => $workOrderLogId,
        'type'              => $type,
        'user_id'           => $this->getApp()->getRequest()->session->userId
      ];
    }

    $outputArr = [];
    foreach ($inst->query($query) as $foundModel) {
      $outputArr[] = $foundModel->getPropertyArray(TRUE);
    }

    $photo = WorkOrderPhoto::init($this->getApp());
    if (isset($outputArr[0]['id'])) {
      $photo->findById($outputArr[0]['id']);
    }

    $photo->data              = $photos;
    $photo->type              = $type;
    $photo->date_created      = time();
    $photo->user_id           = $userId;
    $photo->work_order_log_id = $workOrderLogId;

    $photo->savePhotos();
    if (isset($outputArr[0]['id'])) {
      $photo->save();
    }
    else {
      $photo->insert();
    }

    echo json_encode($photo);
  }

  /**
   * Saves resolution notes of a Work Order.
   * ~~~
   * {
   * "work_order_id": string,
   * "notes": string,
   * }
   * ~~~
   * Find the wo log associated to the user and work order, and create a WorkOrderData record with the notes.
   * @Docs\Param("body",required=true,description="Work order ID, and notes data in string")
   * - - -
   * Return notes created work order data with resolution notes
   */
  public function postResolutionnotes() {
    $input = json_decode($this->getApp()->getRequest()->getBody(), TRUE);

    $userId = $this->getApp()->getRequest()->session->userId;
    $data   = ['notes' => $input['notes']];

    //verify if work order resolution notes already exists
    $workOrderLogId    = $this->getWorkOrderLog($input);
    $instWorkOrderData = WorkOrderData::init($this->getApp());

    $query['query'] = [
      'work_order_log_id' => $workOrderLogId,
      'type'              => 'resolution_notes'
    ];

    $outputWorkOrderLogArr = [];
    foreach ($instWorkOrderData->query($query) as $foundModel) {
      $outputWorkOrderLogArr[] = $foundModel->getPropertyArray(TRUE);
    }
    if (count($outputWorkOrderLogArr) > 0) {
      //update work order data
      $workOrderData = WorkOrderData::init($this->getApp());
      $workOrderData->findById($outputWorkOrderLogArr[0]['id']);
      $workOrderData->data              = $data;
      $workOrderData->work_order_log_id = $workOrderLogId;
      $workOrderData->date_created      = time();
      $workOrderData->user_id           = $this->getApp()->getRequest()->session->userId;
      $workOrderData->save();
    }
    else {
      //save work order data (type survey)
      $workOrderData                    = WorkOrderData::init($this->getApp());
      $workOrderData->type              = 'resolution_notes';
      $workOrderData->data              = $data;
      $workOrderData->work_order_log_id = $workOrderLogId;
      $workOrderData->work_order_id     = $input['work_order_id'];
      $workOrderData->date_created      = time();
      $workOrderData->user_id           = $this->getApp()->getRequest()->session->userId;
      $workOrderData->insert();
    }

    echo json_encode($workOrderData);
  }

  /**
   * Saves closeout job details of a Work Order.
   * ~~~
   * {
   * "work_order_id": string,
   * "signature": string,
   * "signed_wo_picture": string
   * }
   * ~~~
   * Find the wo log associated to the user and work order, and create a WorkOrderData record with the photos.
   * @Docs\Param("body",required=true,description="Work order ID, and photos' data in base64")
   * - - -
   * Return Photo data
   */
  public function postCloseoutjob() {
    $input = json_decode($this->getApp()->getRequest()->getBody(), TRUE);

    $userId              = $this->getApp()->getRequest()->session->userId;
    $photos              = [];
    $photos['signature'] = $input['signature'];

    //this field is not send in commercial fire
    if (isset($input['signed_wo_picture'])) {
      $photos['signed_wo_picture'] = $input['signed_wo_picture'];
    }

    if ($this->getApp()->getRequest()->apiApplication->applicationName == RAEL_APP_NAME) {
      $workOrder = WorkOrder::init($this->getApp());
      $workOrder->findById($input['work_order_id']);
      $this->_checkAccessToWorkOrder($workOrder);
    }

    //verify if work order closeout job already exists
    $workOrderLogId = $this->getWorkOrderLog($input);
    $inst           = WorkOrderPhoto::init($this->getApp());
    $query['query'] = [
      'work_order_log_id' => $workOrderLogId,
      'type'              => 'closeoutjob'
    ];

    $outputArr = [];
    foreach ($inst->query($query) as $foundModel) {
      $outputArr[] = $foundModel->getPropertyArray(TRUE);
    }

    $photo = WorkOrderPhoto::init($this->getApp());
    if (isset($outputArr[0]['id'])) {
      $photo->findById($outputArr[0]['id']);
    }

    $photo->data              = $photos;
    $photo->type              = $input['type'];
    $photo->date_created      = time();
    $photo->user_id           = $userId;
    $photo->work_order_log_id = $workOrderLogId;

    $photo->savePhotos();
    if (isset($outputArr[0]['id'])) {
      $photo->save();
    }
    else {
      $photo->insert();
    }
    echo json_encode($photo);
  }

  /**
   * Retrieves reports by work order id and equipment id
   * return  Photo data
   */
  public function restGetPhotosEquipment($id) {
    $type    = $this->getApp()->getRequest()->params('type');
    $filter  = '?filter[where][and][0][work_order_id][regexp]=' . $id;
    $appName = $this->getApp()->getRequest()->apiApplication->applicationName;

    if ($equipment_id = $this->getApp()->getRequest()->params('equipment_id')) {
      $filter .= '&filter[where][and][1][equipment_id]=' . $equipment_id;
    }

    $equiptid = new Equiptid($this->getApp());
    $woDetail = $equiptid->callGET('Reports' . $filter);

    if ($appName == RAEL_APP_NAME) {
      $photos = [];
      foreach ($woDetail as $wo) {
        if (isset($wo->equipment_photos->$type)) {
          $photoRecord = [
            'equipment_id' => $wo->equipment_id,
          ];
          foreach ($wo->equipment_photos->$type as $k => $v) {
            $photoRecord[$k] = $this->getApp()->getConfig('IMAGE_BASE_URL') . $v;
          }

          array_push($photos, $photoRecord);
        }
      }
    }
    else {
      $photos = isset($woDetail[0]->equipment_photos->$type) ? $woDetail[0]->equipment_photos->$type : [];

      if ($photos) {
        foreach ($photos as $k => $v) {
          if ($v) {
            $photos->$k = $this->getApp()->getConfig('IMAGE_BASE_URL') . $v;
          }
        }
      }
    }
    echo json_encode($photos);
  }

  /**
   * Updates report information - save equipment photos data in EQUIPT ID DATABASE.
   * @return Returns the photos data
   */
  public function restPostPhotosEquipment($id) {
    $input   = json_decode($this->getApp()->getRequest()->getBody(), TRUE);
    $appName = $this->getApp()->getRequest()->apiApplication->applicationName;

    if ($appName == RAEL_APP_NAME) {
      $filter = '?filter[where][and][0][work_order_id][regexp]=' . $id . '&' . 'filter[where][and][1][equipment_id]=' . $input['equipment_id'] .
        '&' . 'filter[where][and][2][type]=equipment_photos';
    }
    else {
      $filter = '?filter[where][and][0][work_order_id][regexp]=' . $id . '&' . 'filter[where][and][1][equipment_id]=' . $input['equipment_id'];
    }

    $equiptid = new Equiptid($this->getApp());
    $woDetail = $equiptid->callGET('Reports' . $filter);

    $photos['photo_1'] = $input['photo_1'];
    $photos['photo_2'] = $input['photo_2'];
    $photos['photo_3'] = $input['photo_3'];
    $photos['photo_4'] = $input['photo_4'];

    $photo = WorkOrderPhoto::init($this->getApp());
    if ($appName == RAEL_APP_NAME) {
      $photo->rael_equipment = TRUE;
    }
    $photo->work_order_id = $id;
    $photo->equipment_id  = $input['equipment_id'];
    $photo->data          = $photos;
    $photo->savePhotos();

    if (!empty($woDetail)) {

      if (isset($woDetail[0]->equipment_photos)) {
        //adds or updates  the current equipment photos before or equipment photos before
        $woDetail[0]->equipment_photos->$input['type'] = $photo->data;
      }
      else {
        $woDetail[0]->equipment_photos = (object) [$input['type'] => $photo->data];
      }

      $params = [
        'equipment_photos' => $woDetail[0]->equipment_photos
      ];

      $reports = $equiptid->callPUT('Reports/' . $woDetail[0]->id, $params);
    }
    else {
      $this->getApp()->returnError(Errors::$UNDEFINED_RESULT);
    }

    echo json_encode($reports->equipment_photos->$input['type']);
  }

  /**
   * Formats an array of Service Channel's Work Order objects into an array
   * of Work Orders
   *
   * @param array $workOrders
   * @param bool  $asArray
   *
   * @return array
   */
  private function _formatScWorkOrders($workOrders, $asArray = TRUE) {
    $woObjects = [];
    foreach ($workOrders as $wo) {
      if (!property_exists($wo, 'Location') || $wo->Subscriber->Id != '2014917124') {
        continue;
      }
      $woObjects[] = $this->_formatScWorkOrder($wo, $asArray);
    }

    return $woObjects;
  }

  /**
   * @Docs\Name Formats a WO based on a service Channel WO object (internal use only)
   */
  public function _formatScWorkOrder($wo, $asArray = TRUE) {
    $data      = [
      'external_id'     => $wo->Id,
      'external_data'   => json_encode($wo),
      'scheduled_date'  => isset($wo->ScheduledDate) ? strtotime($wo->ScheduledDate) : NULL,
      'expiration_date' => isset($wo->ExpirationDate) ? strtotime($wo->ExpirationDate) : NULL,
      'call_date'       => isset($wo->CallDate) ? strtotime($wo->CallDate) : NULL,
      'trade'           => isset($wo->Trade) ? $wo->Trade : NULL,
      'company_id'      => $this->getApp()->getRequest()->session->user->company_id,
      'problem_group'   => $wo->Category,
      'problem_desc'    => $wo->Description,
      'priority'        => $wo->Priority,
      'status'          => strtolower(str_replace(' ', '_', $wo->Status->Primary)),
      'address'         => $this->_generateAddress($wo->Location),
      'store_name'      => $wo->Location->Name,
      'latitude'        => $wo->Location->Latitude,
      'longitude'       => $wo->Location->Longitude,
      'location_id'     => $wo->Location->Id
    ];
    $workOrder = WorkOrder::init($this->getApp());
    $workOrder->loadFromExternalSource($data);
    return $asArray ? $workOrder->getPropertyArray(TRUE) : $workOrder;
  }

  /**
   * Returns true if the new status requires to be saved.
   *
   * @param string $currentStatus
   * @param string $newStatus
   *
   * @return bool
   */
  private function _checkWoStatusPriority($currentStatus, $newStatus) {
    if ($currentStatus == $newStatus) {
      return FALSE;
    }
    switch ($currentStatus) {
      case 'open':
        return TRUE;
        break;
      case 'in_progress':
        return $newStatus == 'completed';
        break;
    }
    return FALSE;
  }

  /**
   * @param $woData array with Work Order info
   *
   * @link https://sb2api.servicechannel.com/swagger/ui/index#!/WorkOrders/WorkOrders_GetWorkOrders
   */
  private function _saveServiceChannelWO($woData) {
    // First, look for the WOs that we already have in the DB
    $woIds    = [];
    $foundWos = [];
    foreach ($woData as $scWo) {
      $woIds[] = $scWo->Id;
    }
    foreach (WorkOrder::init($this->getApp())
               ->query(['query' => ['external_id' => ['$in' => $woIds]], 'limit' => count($woIds)]) as $foundModel) {
      $foundWos[] = $foundModel;
    }

    // Then update the status of those found WOs
    foreach ($foundWos as $foundWo) {
      foreach ($woData as $k => $wo) {
        $status = strtolower(str_replace(' ', '_', $wo->Status->Primary));
        if ($wo->Id == $foundWo->external_id) {//existing WO in our database
          $save = FALSE;
          if ($foundWo->location_id != $wo->Location->Id) {
            $foundWo->location_id = $wo->Location->Id;
            $save                 = TRUE;
          }
          if ($this->_checkWoStatusPriority($foundWo->status, $status)) {
            $foundWo->status = $status;
            $save            = TRUE;
          }
          if ($save) {
            $foundWo->save();
          }
          unset($woData[$k]);
          break;
        }
      }
    }

    // Now insert all the new WOs returned by SC
    try {
      foreach ($woData as $wo) {
        if (!property_exists($wo, 'Location') || $wo->Subscriber->Id != '2014917124') {
          continue;
        }
        $data                    = array(
          'external_id'     => $wo->Id,
          'external_data'   => json_encode($wo),
          'scheduled_date'  => isset($wo->ScheduledDate) ? strtotime($wo->ScheduledDate) : NULL,
          'expiration_date' => isset($wo->ExpirationDate) ? strtotime($wo->ExpirationDate) : NULL,
          'call_date'       => isset($wo->CallDate) ? strtotime($wo->CallDate) : NULL,
          'trade'           => isset($wo->Trade) ? $wo->Trade : NULL,
          'company_id'      => $this->getApp()->getRequest()->session->user->company_id,
          'problem_group'   => $wo->Category,
          'problem_desc'    => $wo->Description,
          'priority'        => $wo->Priority,
          'status'          => strtolower(str_replace(' ', '_', $wo->Status->Primary)),
          'address'         => $this->_generateAddress($wo->Location),
          'store_name'      => $wo->Location->Name,
          'latitude'        => $wo->Location->Latitude,
          'longitude'       => $wo->Location->Longitude,
          'location_id'     => $wo->Location->Id,
        );
        $workOrder               = WorkOrder::init($this->getApp());
        $workOrder->date_created = time();
        $workOrder->loadFromExternalSource($data);
        $workOrder->insert();
      }
    } catch (MongoDuplicateKeyException $e) {
      $this->getApp()->returnError(Errors::$DUPLICATED_WO_EXTERNAL_ID);
    }
  }


  /**
   * @param $woData array with Work Order info
   *
   * @link https://portal.brinco.com/mobile/common/api/WorkOrders
   */
  private function _saveBrincoWO($woData) {
    $updated  = 0;
    $inserted = 0;

    // First, look for the WOs that we already have in the DB
    $woIds    = [];
    $foundWos = [];
    foreach ($woData as $scWo) {
      $woIds[] = $scWo->OrderNumber;
    }
    foreach (WorkOrder::init($this->getApp())
               ->query(['query' => ['customer_po' => ['$in' => $woIds]], 'limit' => count($woIds)]) as $foundModel) {
      $foundWos[] = $foundModel;
    }

    // Then update the status of those found WOs
    foreach ($foundWos as $foundWo) {
      foreach ($woData as $k => $wo) {
        $status = $wo->Status;
        if ($wo->OrderNumber == $foundWo->customer_po) {//existing WO in our database
          $updated++;
          unset($woData[$k]);
          break;
        }
      }
    }

    // Now insert all the new WOs returned by Brinco API
    foreach ($woData as $wo) {
      $scheduledDateArray = explode('T', $wo->VisitDate);
      $expirationDate     = $scheduledDateArray[0] . 'T' . '23:59:59';

      $data                    = [
        'store_name'      => $wo->Company,
        'call_slip'       => $wo->PONumber,
        'dispatch_id'     => $wo->DispatchId,
        'customer_po'     => $wo->OrderNumber,
        'external_id'     => $wo->ClientPO,
        'scheduled_date'  => isset($wo->VisitDate) ? strtotime(str_replace('T', ' ', $wo->VisitDate)) : NULL,
        'expiration_date' => isset($wo->VisitDate) ? strtotime($expirationDate) : NULL,
        'priority'        => $wo->Priority,
        'problem_desc'    => $wo->Category . " - " . $wo->OrderType,
        'address'         => $this->_generateAddress($wo),
        'status'          => $wo->Status,
        'latitude'        => $wo->Latitude,
        'longitude'       => $wo->Longitude,
        'instructions'    => $wo->Scope,
        'contact_person'  => "pending",
        'general_notes'   => "",
        'company_id'      => $this->getApp()->getRequest()->session->user->company_id,
        'user_id'         => $this->getApp()->getRequest()->session->userId
      ];
      $workOrder               = WorkOrder::init($this->getApp());
      $workOrder->date_created = time();
      $workOrder->loadFromExternalSource($data);

      $workOrder->insert();

      $inserted++;
    }
  }

  private function _generateAddress($l) {
    $address = '';
    if (isset($l->Address1) && $l->Address1 != '') {
      $address .= $l->Address1;
    }
    if (isset($l->Address2) && $l->Address2 != '') {
      if ($address) {
        $address .= " ";
      }
      $address .= "$l->Address2";
    }
    if (isset($l->City) && $l->City != '') {
      $address .= ", $l->City";
    }
    if (isset($l->State) && $l->State != '') {
      $address .= ", $l->State";
    }
    if (isset($l->Zip) && $l->Zip != '') {
      $address .= " $l->Zip";
    }
    return $address;
  }

  /**
   * Creates a snow trade work order in Service Channel.
   */
  private function _saveWorkOrderBrigthview() {
    $input   = json_decode($this->getApp()->getRequest()->getBody(), TRUE);
    $company = Company::init($this->getApp());
    $company->findById($this->getApp()->getRequest()->session->user->company_id);
    $wo = [
      'ContractInfo'  => [
        'SubscriberId' => $this->getApp()->getConfig('SC_SUBSCRIBER_CREDENTIALS')['subscriber_id'],
        'LocationId'   => $input['locationId'],
        'ProviderId'   => $company->external_token['SC']['provider_id'],
        'TradeName'    => SNOW_TRADE
      ],
      'Category'      => 'MAINTENANCE',
      'Priority'      => 'Snow Emergency - 2 Hours',
      'Nte'           => 0,
      'CallerName'    => 'Brightview FTC service partner',
      'CallDate'      => gmdate('j M Y H:i:s') . ' GMT',
      'ScheduledDate' => gmdate('j M Y H:i:s', time() + 8 * 3600) . ' GMT',
      'ProblemSource' => BRIGHTVIEW_APP_NAME . ' App',
      'Description'   => 'Snow Cleaning',
      'ProblemCode'   => 'Plow #90001#',
      'Status'        => [
        'Primary'  => 'Open',
        'Extended' => ''
      ]
    ];

    $scSubscriberAPI = new ServiceChannel($this->getApp(), FALSE, ServiceChannel::getSubscriberAccessToken($this->getApp()));
    $createdWo       = $scSubscriberAPI->callPOST('workorders', $wo);
    $createdWo       = $scSubscriberAPI->callGET('workorders', ['id' => $createdWo->id]);
    $createdWo       = $this->_formatScWorkOrders($createdWo, TRUE);

    echo json_encode($createdWo[0]);
  }

  private function _saveWorkOrderCornell() {
    $input = json_decode($this->getApp()->getRequest()->getBody(), TRUE);

    try {
      $company = Company::init($this->getApp());
      $company->findById($input['company_id']);
      if (!$company->name) {
        $this->getApp()->returnError(Errors::$INVALID_COMPANY);
      }
    } catch (\MongoException $e) {
      $this->getApp()->returnError(Errors::$INVALID_COMPANY);
    }

    if (!isset($input['equipment'])) {
      $this->getApp()->returnError(Errors::$EMPTY_EQUIPMENT);
    }

    if ($this->checkArrayKey($input['equipment'], 'customer_unique_id')) {
      $this->saveEquipmentToEquiptId($input);
    }
    else {
      $this->getApp()->returnError(Errors::$EMPTY_EQUIPMENT_CUSTOMER_UNIQUE_ID);
    }
  }

  private function _saveWorkOrderGeneral() {
    $input = json_decode($this->getApp()->getRequest()->getBody(), TRUE);

    try {
      $company = Company::init($this->getApp());
      $company->findById($input['company_id']);
      if (!$company->name) {
        $this->getApp()->returnError(Errors::$INVALID_COMPANY);
      }
    } catch (\MongoException $e) {
      $this->getApp()->returnError(Errors::$INVALID_COMPANY);
    }

    $workOrder = WorkOrder::init($this->getApp());
    $workOrder->loadFromExternalSource($input);
    $workOrder->date_created = time();
    $workOrder->insert();

    echo $workOrder->outputJSON();
  }

  private function _saveWorkOrderWithAssignedUsers() {
    $input = json_decode($this->getApp()->getRequest()->getBody(), TRUE);

    try {
      $company = Company::init($this->getApp());
      $company->findById($input['company_id']);
      if (!$company->name) {
        $this->getApp()->returnError(Errors::$INVALID_COMPANY);
      }
    } catch (\MongoException $e) {
      $this->getApp()->returnError(Errors::$INVALID_COMPANY);
    }

    //validate users into user_ids exists and belong to a same company_id
    if (!empty($input['user_ids'])) {

      $users             = User::init($this->getApp());
      $input['user_ids'] = array_values(array_unique($input['user_ids']));

      try {
        //check user_ids are valid objectId
        $assignedUsers = array_map(function ($u) {
          return new \MongoId($u);
        }, $input['user_ids']);

        //check all users in user_ids exist
        foreach ($input['user_ids'] as $assignedUser) {
          $user = User::init($this->getApp());
          $user->findById($assignedUser);
        }
      } catch (\MongoException $e) {
        $this->getApp()->returnError(Errors::$INVALID_USER_ID);
      }

      //check all user belong to a same company
      $query = [
        'query' => [
          '_id'        => ['$in' => $assignedUsers],
          'company_id' => $input['company_id']
        ]
      ];

      $input['user_ids'] = array_values($input['user_ids']);
      if (count($users->query($query)) != count($assignedUsers)) {
        $this->getApp()->returnError(Errors::$INVALID_COMPANY_ID_FOR_USER_ID);
      }
    }
    else {
      $this->getApp()->returnError(Errors::$USER_IDS_REQUIRED);
    }

    if (!isset($input['store_id'])) {
      $this->getApp()->returnError(Errors::$EMPTY_STORE_ID);
    }

    if (empty($input['store_id'])) {
      $this->getApp()->returnError(Errors::$NOT_EMPTY_STORE_ID);
    }

    if (!is_string($input['store_id'])) {
      $this->getApp()->returnError(Errors::$STRING_STORE_ID);
    }

    if (!isset($input['area_id'])) {
      $this->getApp()->returnError(Errors::$EMPTY_AREA_ID);
    }

    if (empty($input['area_id'])) {
      $this->getApp()->returnError(Errors::$NOT_EMPTY_AREA_ID);
    }

    if (!is_string($input['area_id'])) {
      $this->getApp()->returnError(Errors::$STRING_AREA_ID);
    }

    $this->saveEquipmentToEquiptIdRael($input);
  }

  public function saveEquipmentToEquiptId($input) {

    $workOrder = WorkOrder::init($this->getApp());
    $workOrder->loadFromExternalSource($input);
    $workOrder->date_created = time();
    $workOrder->insert();
    $workOrderData = $workOrder->outputJSON();

    $equipmentList = $input['equipment'];
    $reports       = [];
    foreach ($equipmentList as $woEquipment) {
      $data                  = [];
      $data['work_order_id'] = $workOrder->getId();
      $data['status']        = 'active';
      if ($woEquipment['customer_unique_id'] != NULL) {
        $data['customer_unique_id'] = $woEquipment['customer_unique_id'];
      }
      else {
        $this->getApp()->returnError(Errors::$EMPTY_EQUIPMENT_CUSTOMER_UNIQUE_ID);
      }

      $data['equipment_problem'] = $woEquipment['equipment_problem'];
      array_push($reports, $data);
    }

    try {
      //save the work order reports in Equipt id Database
      $equiptid = new Equiptid($this->getApp());
      $equiptid->callPOST('Reports/woReport', $reports, Mime::JSON);
    } catch (\Exception $exception) {
      //remove the work order if there was some problem calling to the endpoint 'Reports/woReport
      $workOrder->delete();
    }
    echo $workOrderData;
  }

  public function saveEquipmentToEquiptIdRael($input) {

    $workOrder = WorkOrder::init($this->getApp());
    $workOrder->loadFromExternalSource($input);
    $workOrder->date_created = time();
    $workOrder->insert();
    $workOrderData = $workOrder->outputJSON();

    $areaId  = $input['area_id'];
    $reports = [];
    $filter  = '?filter[where][and][0][area_id]=' . $areaId . '&filter[where][and][1][status]=active';

    $equiptid      = new Equiptid($this->getApp());
    $equipmentList = $equiptid->callGET('Equipment' . $filter);
    if (count($equipmentList) > 0) {
      foreach ($equipmentList as $woEquipment) {
        $data                      = [];
        $data['work_order_id']     = $workOrder->getId();
        $data['status']            = 'active';
        $data['equipment_id']      = $woEquipment->id;
        $data['equipment_problem'] = 'Equipment Photos';
        $data['type']              = 'equipment_photos';
        array_push($reports, $data);
      }

      try {
        //save the work order reports in Equipt id Database
        $equiptid->callPOST('Reports/woReport', $reports, Mime::JSON);
      } catch (\Exception $exception) {
        //remove the work order if there was some problem calling to the endpoint 'Reports/woReport
        $workOrder->delete();
      }
    }
    echo $workOrderData;
  }

  public function restPostRequest($id) {

    $input = json_decode($this->getApp()->getRequest()->getBody(), TRUE);

    $workOrder = WorkOrder::init($this->getApp());
    $workOrder->findById($id);

    if (empty($workOrder->request_approval)) {
      $this->sendWorkOrderREquestApprovalEmail($workOrder->getId());
      $workOrder->request_approval = $input['requestApproval'];
      $workOrder->save();

      echo $workOrder->outputJSON();
    }
    else {
      //the wo request has already been approved or declined ;
      $data['request_approval_status_error'] = Errors::$REQUEST_APPROVAL_NOT_ALLOWED;
      echo json_encode($data);
    }
  }


  /**
   * Send a Email on check in and check out process to Cornell.
   */
  private function sendWorkOrderREquestApprovalEmail($woId) {
    $input    = json_decode($this->getApp()->getRequest()->getBody(), TRUE);
    $currUser = $this->getApp()->getRequest()->session->user;

    $workOrder = WorkOrder::init($this->getApp());
    $workOrder->findById($woId);

    //get Company
    $company = Company::init($this->getApp());
    $company->findById($workOrder->company_id);

    //wo Approval
    $woProcess = $input['requestApproval'];

    $emailProvider = new Mandrill($this->getApp()->getConfig('MANDRILL_API_KEY'),
      $this->getApp()->getConfig('OUTBOUND_FROM_EMAIL'),
      $this->getApp()->getConfig('OUTBOUND_FROM_NAME_CO'));

    $template = new Htmlparser(__DIR__ . '/../../template/coworkorderrequestapprovalemail.tpl.php');

    $subject                  = 'Work Order Request Approval for ' . $workOrder->call_slip;
    $dataEmail['firstName']   = $currUser->firstName;
    $dataEmail['lastName']    = $currUser->lastName;
    $dataEmail['companyName'] = $company->name;
    $dataEmail['storeName']   = $workOrder->store_name;
    $dataEmail['callSlip']    = $workOrder->call_slip;
    $dataEmail['woProcess']   = $woProcess;

    $template->data = $dataEmail;
    $emailContent   = $template->parse();

    $outEmail = $this->getApp()->getConfig('OUTBOUND_TO_EMAIL_CO');
    foreach ($outEmail as $to) {
      $emailProvider->sendEmail($to, new TokenTemplate(
        $emailContent,
        $subject
      ));
    }
  }

  /*
   * Check if the current user has permission to process the current wo.
   * This function is applicable only when the work orders are assigned to specific users
   */
  private function _checkAccessToWorkOrder($workOrder) {
    if (!in_array($this->getApp()->getRequest()->session->userId, $workOrder->user_ids)) {
      $this->getApp()->returnError(Errors::$UNAUTHORIZED_USER_WORK_ORDER);
    }
  }

  /*
   * Check if the array contains arrays with the same key
   * This function is applicable only when work order has equipment
   */
  private function checkArrayKey($array, $keySearch) {
    $contains = [];
    foreach ($array as $key => $item) {
      if (array_key_exists($keySearch, $item)) {
        array_push($contains, TRUE);
      }
      else {
        array_push($contains, FALSE);
      }
    }

    return !in_array(FALSE, $contains);
  }
}
