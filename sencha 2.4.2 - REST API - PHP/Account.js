Ext.define('Base.controller.Account', {
  extend: 'Ext.app.Controller',
  config: {
    refs: {
      //views
      mainView: 'main',
      initMain: 'initmain',
      accountContainer: 'account',
      loginForm: 'loginform',
      logoutContainer: 'initmain[name="init-main-flat"]',
      forgetPasswordForm: 'forgetpasswordform',
      viewAccount: 'viewaccount',
      createAccountStep1Form: 'createaccountform',
      createAccountStep2Form: 'createaccountformstep2',
      //buttons
      signInButton: 'initmain #signIn',
      registerButton: 'initmain #register',
      loginButton: 'loginform button[name="login"]',
      logoutButton: 'viewaccount button[name="logout"]',
      forgotButton: 'account loginform button[name="forgot"]',
      forgetPasswordSubmitButton: 'account button[name="forgetPasswordSubmit"]',
      updateButton: 'button[name="updateaccount"]',
      createAccountStep1NextButton: 'account button[name="createAccountStep1NextButton"]',
      createAccountStep2NextButton: 'account button[name="createAccountStep2NextButton"]',
      leftMenuButton: 'account leftmenubutton',
      //fields
      loginPasswordField: 'loginform passwordfield',
      passwordField: 'viewaccount passwordfield[name="pass"]',
      createAccountStep1FormFields: 'createaccountform field',
      createAccountStep2FormFields: 'createaccountformstep2 field',
      confirmEmail: 'createaccountform [name=confirmEmail]',
      companyId: 'createaccountform [name=company_id]'
    },
    control: {
      //views
      initMain: {
        activate: 'onInitMainActive',
        deactivate: 'onInitMainDeactivate'
      },
      logoutContainer: {
        activate: 'onLogoutContainerActivate'
      },
      accountContainer: {
        initialize: 'onAccountContainerInitialize',
        activate: 'onAccountContainerActivate'
      },
      viewAccount: {
        activate: 'onViewAccountFormActivate',
        deactivate: 'onViewAccountFormDeactivate'
      },
      createAccountStep1Form: {
        deactivate: 'onCreateAccountStep1FormDeactivate',
        activate: 'onCreateAccountStep1FormActivate'
      },
      createAccountStep2Form: {
        deactivate: 'onCreateAccountStep2FormDeactivate',
        activate: 'onCreateAccountStep2FormActivate'
      },
      forgetPasswordForm: {
        activate: 'onForgetPasswordFormActivate',
        deactivate: 'onForgetPasswordFormDeactivate'
      },
      //buttons
      signInButton: {
        tap: 'onSignInTap'
      },
      loginButton: {
        tap: 'onLoginTap'
      },
      logoutButton: {
        tap: 'onLogoutTap'
      },
      forgotButton: {
        tap: 'onForgotTap'
      },
      forgetPasswordSubmitButton: {
        tap: 'onForgetPasswordSubmitTap'
      },
      updateButton: {
        tap: 'onUpdateTap'
      },
      registerButton: {
        tap: 'onRegisterTap'
      },
      createAccountStep1NextButton: {
        tap: 'onCreateAccountStep1NextButtonTap'
      },
      createAccountStep2NextButton: {
        tap: 'onCreateAccountStep2NextButtonTap'
      },
      //fields
      loginPasswordField: {
        keyup: 'onLoginPasswordFieldKeyup'
      },
      passwordField: {
        focus: 'onPasswordFieldFocus',
        blur: 'onPasswordFieldBlur'
      },
      createAccountStep1FormFields: {
        change: 'onCreateAccountStep1FormFieldsChange',
        focus: 'onFormInputFocus',
        blur: 'onFormInputBlur'
      },
      createAccountStep2FormFields: {
        change: 'onCreateAccountStep2FormFieldsChange',
        focus: 'onFormInputFocus',
        blur: 'onFormInputBlur'
      }
    }
  },
  //views
  onInitMainActive: function () {
    if (App.authUser) {
      this.getLeftMenuButton().show();
    }
  },
  onInitMainDeactivate: function () {
    if (App.authUser) {
      this.getLeftMenuButton().hide();
    }
  },
  onLogoutContainerActivate: function () {
    var ios = new IOSDongle();
    ios.calliOSFunction('showLandingScreen');
    ios.calliOSFunction('landingViewLoaded');
  },
  minNativeVersion: function (minVersion) {
    var version = new Ext.Version(new RegExp('FTC/(\\S*)', 'i').exec(navigator.userAgent)[1]);
    return version.gtEq(minVersion);
  },
  onAccountContainerInitialize: function () {
    var contr = this;

    //user with active session
    if (App.authUser) {
      function loadFinished () {
        contr.getAccountContainer().push(Ext.create('Base.view.account.ViewAccount', {
          firstName: App.authUser.get('firstName'),
          lastName: App.authUser.get('lastName'),
          username: App.authUser.get('username'),
          email: App.authUser.get('email'),
          phone: App.authUser.get('phone'),
          company_name: App.authUser.get('company_name')
        }));
      }

      loadFinished();
    } else {
      contr.getAccountContainer().push(Ext.widget('initmain'));
    }
  },
  onAccountContainerActivate: function () {
    ga('send', 'screenview', {
      'appName': App.adAppType,
      'appVersion': lang.base.appVersion,
      'screenName': 'account'
    });
  },
  onViewAccountFormActivate: function () {
    this.viewAccountFormSaveButton = this.getAccountContainer().getNavigationBar().add({
      xtype: 'button',
      name: 'updateaccount',
      text: lang.base.btn_save,
      cls: 'x-button-navigationbar',
      align: 'right'
    });
  },
  onViewAccountFormDeactivate: function () {
    this.getAccountContainer().getNavigationBar().remove(this.viewAccountFormSaveButton, true);
  },

  onCreateAccountStep1FormActivate: function () {

    this.createAccountStep1NextButton = this.getAccountContainer().getNavigationBar().add({
      xtype: 'button',
      name: 'createAccountStep1NextButton',
      text: lang.base.lbl_next,
      cls: 'x-button-navigationbar',
      disabled: true,
      align: 'right'
    });
    this.validateFields(this.getCreateAccountStep1Form());
    ga('send', 'screenview', {
      'appName': App.adAppType,
      'appVersion': lang.base.appVersion,
      'screenName': 'register'
    });

  },
  onCreateAccountStep1FormDeactivate: function () {
    this.getAccountContainer().getNavigationBar().remove(this.createAccountStep1NextButton, true);
  },
  onCreateAccountStep2FormActivate: function () {
    this.createAccountStep2NextButton = this.getAccountContainer().getNavigationBar().add({
      xtype: 'button',
      name: 'createAccountStep2NextButton',
      text: lang.base.lbl_next,
      cls: 'x-button-navigationbar',
      disabled: true,
      align: 'right'
    });
  },
  onCreateAccountStep2FormDeactivate: function () {
    this.getAccountContainer().getNavigationBar().remove(this.createAccountStep2NextButton, true);
  },
  onForgetPasswordFormActivate: function () {
    this.forgetPasswordSubmitButton = this.getAccountContainer().getNavigationBar().add({
      xtype: 'button',
      name: 'forgetPasswordSubmit',
      text: lang.base.btn_forget_password_submit,
      cls: 'x-button-navigationbar',
      align: 'right'
    });
  },
  onForgetPasswordFormDeactivate: function () {
    this.getAccountContainer().getNavigationBar().remove(this.forgetPasswordSubmitButton, true);
  },
  //buttons
  onSignInTap: function () {
    this.getAccountContainer().push(Ext.create('Base.view.account.LoginForm'));
  },
  onRegisterTap: function () {
    this.displayRegistrationStep1();
  },
  displayOutdatedAlert: function () {
    var message = lang.base.native_outdated.replace(/{appLink}/g, Ext.os.is.iOS ? lang.base.app_store_url : lang.base.play_store_url).replace(
      /{appHref}/g, Ext.os.is.iOS ? lang.base.app_store_href : lang.base.play_store_href);
    Ext.create('Ext.MessageBox').show({
      padding: 20,
      cls: 'x-dock-body',
      message: message,
      icon: Ext.MessageBox.INFO,
      buttons: []
    });
  },
  displayRegistrationStep1: function (serviceResponse) {
    this.registerFormStep1 = Ext.widget('createaccountform');
    if (serviceResponse) {
      this.registerFormStep1.down('textfield[name="company_id"]').destroy();
      this.registerFormStep1.setServiceResponse(serviceResponse);
    }
    this.getAccountContainer().push(this.registerFormStep1);
  },
  onLoginTap: function () {
    var form = this.getLoginForm();

    var loginVals = form.getValues();
    var data = {
      email: loginVals.email.trim(),
      password: loginVals.pass.trim()
    };

    //LOGIN
    Ext.Ajax.request({
      url: '/mobileserv/sessions',
      method: 'POST',
      jsonData: data,
      params: {lang : navigator.language.substr(0,2)},
      success: function (response, opts) {
        var data = JSON.parse(response.responseText);
        if (data.sessionId) {
          document.cookie = "userId=" + data.userId;
          document.cookie = "sessionId=" + data.sessionId;
          window.location.reload();
        }
      },
      failure: function (response, opts) {
        var message = lang.base.error_logging_in;
        if (response.responseText) {
          var res = JSON.parse(response.responseText);
          message = res.error.message;
          if (message == 'INVALID_CREDENTIALS_COMPANY') {
            message = lang.base.error_credentials_company;
          }
        }
        Ext.Msg.alert("", message);
      }
    });
  },
  onLogoutTap: function () {
    Ext.Ajax.request({
      url: '/mobileserv/sessions/' + getCookie('sessionId'),
      method: 'DELETE',
      success: function (response, opts) {
        var data = JSON.parse(response.responseText);
        if (data.sessionId) {
          document.cookie = "userId=; expires=Thu, 01 Jan 1970 00:00:00 UTC;"
          document.cookie = "sessionId=; expires=Thu, 01 Jan 1970 00:00:00 UTC;"
          var ios = new IOSDongle();
          ios.calliOSFunction('storeSessionCookie');
          window.location.reload();
        }
      },
      failure: function (response, opts) {
        var res = JSON.parse(response.responseText);
        Ext.Msg.alert("", res.error.message, Ext.emptyFn);
      }
    });
  },
  onForgotTap: function () {
    this.ForgetPasswordForm = Ext.widget('forgetpasswordform');
    this.getAccountContainer().push(this.ForgetPasswordForm);
  },
  onForgetPasswordSubmitTap: function () {
    var form = this.getForgetPasswordForm();
    var createVals = form.getValues();
    Ext.Ajax.request({
      url: '/mobileserv/users/forgotpassword?_dc=' + (new Date().getTime()),
      jsonData: {
        email: createVals.email
      },
      success: function (response, opts) {
        Ext.Msg.alert("", lang.base.alert_forgot_password_success, Ext.emptyFn);
        form.parent.pop();
      },
      failure: function (response, opts) {
        var res = JSON.parse(response.responseText);
        if (res && res.error.message) {
          Ext.Msg.alert("", res.error.message, Ext.emptyFn);
        } else {
          Ext.Msg.alert("", lang.base.server_error, Ext.emptyFn);
        }
      }
    });
  },
  onForgetPasswordCancelTap: function () {
    window.location.reload();
  },
  onUpdateTap: function () {
    var form = this.getViewAccount();
    var createVals = form.getValues();

    Ext.Ajax.request({
      url: '/mobileserv/users/' + App.authUser.get('userId'),
      method: 'PUT',
      jsonData: {
        firstName: createVals.firstName,
        lastName: createVals.lastName,
        username: createVals.username,
        email: createVals.email,
        phone: createVals.phone,
        password: createVals.pass
      },
      success: function (response, opts) {
        window.location.reload();
      },
      failure: function (response, opts) {
        var res = JSON.parse(response.responseText);
        Ext.Msg.alert("", res.error.message, Ext.emptyFn);
      }
    });
  },
  onCreateAccountStep1NextButtonTap: function () {
    var registrationForm = this.getCreateAccountStep1Form();
    var data = registrationForm.getValues();
    //hide keypad
    this.hideKeypad(registrationForm);
    //create user
    Ext.Viewport.setMasked({xtype: 'loadmask', message: 'Loading'});
    Ext.Ajax.request({
      url: '/mobileserv/users',
      method: 'POST',
      jsonData: data,
      scope: this,
      showLoading: false,
      success: function (res) {
        this.registerFormStep2 = Ext.widget('createaccountformstep2');
        this.getAccountContainer().push(this.registerFormStep2);
        var maskElm = Ext.Viewport.getMasked();
        Ext.Viewport.remove(maskElm);
        this.hideKeypad(this.registerFormStep2);

      },
      failure: function (res, opt) {
        var maskElm = Ext.Viewport.getMasked();
        Ext.Viewport.remove(maskElm);
        var response = JSON.parse(res.responseText);
        var errorMessage = response.error.message;
        if (errorMessage == 'INVALID_COMPANY') {
          errorMessage = lang.base.error_message_invalid_company;
        }
        Ext.Msg.alert('', errorMessage, Ext.emptyFn);
      }
    });
    return false;
  },
  onCreateAccountStep2NextButtonTap: function () {
    var registrationForm = this.getCreateAccountStep2Form();
    var data = registrationForm.getValues();

    //check password
    if (data.password != data.confirmpassword) {
      Ext.Msg.alert('', lang.base.error_confirm_password);
      return false;
    }
    delete(data.confirmpassword);
    //add values from step 1
    var registrationForm1 = this.getCreateAccountStep1Form();
    Ext.Object.merge(data, registrationForm1.getValues());

    //add third party auth token into form data
    if (registrationForm1.getServiceResponse()) {
      data.externalToken = registrationForm1.getServiceResponse();
    }

    //hide keypad
    this.hideKeypad(registrationForm);
    //update user
    Ext.Viewport.setMasked({xtype: 'loadmask', message: 'Loading'});
    Ext.Ajax.request({
      url: '/mobileserv/users/?step=final',
      method: 'POST',
      jsonData: data,
      scope: this,
      showLoading: false,
      success: function (res) {
        var user = JSON.parse(res.responseText);
        document.cookie = "sessionId=" + user.newSessionId;
        Ext.Ajax.setDefaultHeaders({
          SESSION: user.newSessionId,
          "SHARED-SECRET": lang.base.shared_secret,
          "APP-VERSION": lang.base.appVersion
        });
        document.cookie = "userId=" + user.userId;
        window.location.reload();
      },
      failure: function (res, opt) {
        var maskElm = Ext.Viewport.getMasked();
        Ext.Viewport.remove(maskElm);
        var response = JSON.parse(res.responseText);
        Ext.Msg.alert('', response.error.message, Ext.emptyFn);
      }
    });
  },
  onAccountRegistrationInfoTap: function () {
    Ext.Msg.show({
      message: lang.base.info_message_company_id,
      buttons: Ext.MessageBox.OK,
      cls: 'info-company-id-msg-box'
    });
  },
  //fields
  onLoginPasswordFieldKeyup: function (contr, evt) {
    var keyCode = evt.event.keyCode;
    if (keyCode == 13) {//enter and go key
      var loginform = this.getLoginForm();
      loginform.query('button[name="login"]')[0].fireAction('tap');
    }
  },
  onPasswordFieldFocus: function () {
    var bc = Ext.getCmp('pass');
    if (bc.getValue() == '**********') {
      bc.setValue('');
    }
  },
  onPasswordFieldBlur: function () {
    var bc = Ext.getCmp('pass');
    if (bc.getValue() == '') {
      bc.setValue('**********');
    }
  },
  onCreateAccountStep1FormFieldsChange: function () {
    this.validateFields(this.getCreateAccountStep1Form());
  },
  onCreateAccountStep2FormFieldsChange: function () {
    this.validateFields(this.getCreateAccountStep2Form());
  },
  //additionals
  onFormInputFocus: function (element) {
    element.up('formpanel').down('#activeScreenDots').hide();
  },
  onFormInputBlur: function (element) {
    element.up('formpanel').down('#activeScreenDots').show();
  },
  validateFields: function (form, button) {
    var fields = form.query('field');
    if (!button && this.getAccountContainer().query('button[text="' + lang.base.lbl_next + '"]')) {
      button = this.getAccountContainer().query('button[text="' + lang.base.lbl_next + '"]')[0];
    }

    if (button) button.disable();

    var complete = true;
    for (var x in fields) {
      var field = fields[x];
      if (field.get('value') == '') {
        complete = false;
        break;
      }
    }
    if (complete && button) {
      button.enable();
    }
  },
  hideKeypad: function (el) {
    var textfields = el.query('textfield');
    for (i in textfields) {
      var textfield = textfields[i];
      textfield.blur();
    }
  }
});

//additional functions
function getCookie (cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i].trim();
    if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
  }
  return "";
}