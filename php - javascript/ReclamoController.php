<?php
/**
 * Created by PhpStorm.
 * User: Nataly Constante
 * Date: 22/07/15
 * Time: 12:48 PM
 */

namespace UPR;

use \Globalfunctions;
use \Provincia;
use \Canton;
use \Parroquia;
use \Reclamo;
use \ProcesoReclamo;
use \Auth;
use \Input;
use \View;
use \Empresa;
use \Response;
use \Redirect;
use \DateTime;
use \Lang;
use \PersonaEmpresa;
use \DB;
use \Request;
use \Datatables;
use \Form;

class ReclamoController extends BaseController
{

    public function getIndex()
    {
        //redirecciona a la pantalla de reclamos
        return Redirect::action('UPR\ReclamoController@getReclamos');
    }

    public function getReclamos()
    {
        //muestra la pantalla principal de reclamos
        $estados_atencion = array('ATENDIDO'=>'Atendidos','NUNCA_ATENDIDO'=>'Nunca Atendidos','NO_ATENDIDO_PREGUNTA_ADICIONAL'=>'No se respondio pregunta adicional');
        $calidad_servicio = array('SATISFECHO'=>'Satisfecho','INSATISFECHO'=>'Insatisfecho','SIN_CALIFICACION'=>'Sin Calificac&oacute;in');
        $empresa_principal = Auth::user()->es_contacto_empresa_principal();
        if($empresa_principal){
            $empresa_id = $empresa_principal->empresa_id;
            $sucursales = Empresa::where('emp_padre_id', $empresa_id)->orWhere('emp_id', $empresa_id)->get();

        }else{
            $empresa_id =  Auth::user()->empresas()->lists('empresa_id');
            $sucursales = Empresa::whereIn('emp_id', $empresa_id)->get();
        }

        foreach($sucursales as $suc) {
            $sucursales_array[$suc->emp_id] = $suc->emp_numero_defensorial.' '.$suc->sucursal();
        }
        if($empresa_principal){
            $sucursales_array[$empresa_principal->empresa->emp_id] = $empresa_principal->emp_numero_defensorial.' '.$empresa_principal->empresa->emp_razon_social.' - MATRIZ';
        }
        $escalados = (new Globalfunctions())->unshift_select_item(\UCO\Usuario::$discapacidad_enum, '- Todos -');

        //numero total de reclamos
        $numero_reclamos=$this->numeroTotalReclamosPorEstado();


        $this->layout->contenido = View::make('UPR.reclamo.index', ['estado'=>'pendiente',
            'empresa_principal'=>$empresa_principal,
            'calidad_servicio'=>(new Globalfunctions())->unshift_select_item($calidad_servicio,'- Todos -'),
            'sucursales'=>(new Globalfunctions())->unshift_select_item($sucursales_array,'- Todas -'),
            'escalados'=>$escalados,
            'numero_reclamos'=>$numero_reclamos,
            'estados_atencion'=>(new Globalfunctions())->unshift_select_item($estados_atencion,'- Todos -')
        ]);

    }

    //Carga de tabla
    public function getListaReclamos()
    {
        if (Request::ajax()) {
            $estado = Input::get('estado');
            $empresa_principal = Auth::user()->es_contacto_empresa_principal();
            return View::make('UPR.reclamo.carga.lista', array('empresa_principal'=>$empresa_principal,'estado'=>$estado));
        }
    }

    //Metodo para buscar consumidores
    public function getConsumidores(){
        $cons = Input::get('consumidor');
        $consumidores = \UCO\Usuario::where('uco_estado','ACTIVO')
            ->where(function($query) use($cons)
            {
                $query->orWhere('uco_nombre_fantasia','like','%'.$cons.'%')
                    ->orWhere('uco_numero_documento','like','%'.$cons.'%')
                    ->orWhere('uco_nombres','like','%'.$cons.'%');
            })
            ->get();
        if(count($consumidores)>0){
            foreach($consumidores as $con){

                $consumidores_list[]=array('id'=>$con->uco_id,'text'=>$con->uco_nombres);

            }
        }else{
        $consumidores_list[] = array("id"=>"","text"=>"No se encontraron resultados");
        }

        return Response::json($consumidores_list);
    }

    //metodo para buscar sucursales
    public function getSucursales(){
        $empresa_principal =  Auth::user()->es_contacto_empresa_principal();
        if(!$empresa_principal){
            $empresa_id = Auth::user()->empresas()->lists('empresa_id');
            $empresas = Empresa::whereIn('emp_id',$empresa_id);
        }else{
            $empresa_id = $empresa_principal->empresa_id;
            $empresas = Empresa::where('emp_padre_id',$empresa_id);
        }

            $empresas = $empresas->whereRaw('emp_estado IN ("ACTIVO","INACTIVO")')->where('emp_origen_registro','FORMULARIO_REGISTRO')

            ->where(function($query)
            {
                $query->orWhere('emp_interseccion','like','%'.Input::get('sucursal').'%')
                ->orWhere('emp_calle','like','%'.Input::get('sucursal').'%')
                ->orWhere('emp_numero','like','%'.Input::get('sucursal').'%')
                ->orWhere('emp_numero_defensorial','like','%'.Input::get('sucursal').'%')
                    ->orWhere('emp_razon_social','like','%'.Input::get('sucursal').'%');
            })
            ->get();

        if(count($empresas)>0){
            foreach($empresas as $emp){
                $empresas_list[]=array('id'=>$emp->emp_id,'text'=>$emp->sucursal());
            }
        }else{
            $empresas_list[] = array("id"=>"","text"=>"No se encontraron resultados");
        }
        return Response::json($empresas_list);
    }

    //devuelve el resultado de la consulta con filtros

    public function consultarReclamos(){
        $data = Input::all();

        $empresa_principal =  Auth::user()->es_contacto_empresa_principal();
        //si no es usuario de empresa principal, se toman los ids de empresas a su cargo
        if($empresa_principal){
            $empresa_id = $empresa_principal->empresa_id;
            $empresas_ids = Empresa::where('emp_padre_id',$empresa_id)->lists('emp_id');
            $empresas_asignadas = Auth::user()->empresas()->lists('empresa_id');
            array_push($empresas_ids, $empresa_id);
            $empresas_ids = array_merge($empresas_ids,$empresas_asignadas);
        }else{
            $empresas_ids = Auth::user()->empresas()->lists('empresa_id');
        }

        $reclamos = new \ReclamosConsolidados();

        //lista de ids de empresas a cargo del usuario, si es sucursal o principal
        if($empresa_principal){
            $reclamos = $reclamos->whereIn('id_empresa_creacion',$empresas_ids);
        }else{
            $reclamos = $reclamos->whereIn('id_empresa_asignacion',$empresas_ids);
        }

        if($data['consumidor']){
            $reclamos = $reclamos->where('rec_usuario_consumidor_id',$data['consumidor']);
        }
        if($data['numero_reclamo']){
            $reclamos = $reclamos->where('rec_numero_reclamo',$data['numero_reclamo']);
        }
        if($data['desde']){
            $reclamos = $reclamos->where('fecha_creacion','>=',$data['desde'].' 00:00:00');
        }
        if($data['hasta']){
            $reclamos = $reclamos->where('fecha_creacion','<=',$data['hasta'].' 23:59:59');
        }
        if($data['calidad_servicio']){
            if($data['calidad_servicio']=="SIN_CALIFICACION"){
                $reclamos = $reclamos->where(function($query) use ($data) {
                    $query->whereNull('rec_calidad_servicio')
                        ->orWhere('rec_calidad_servicio', $data['calidad_servicio']);
                });
            }
            else
                $reclamos = $reclamos->where('rec_calidad_servicio',$data['calidad_servicio']);
        }
        if($data['atendido']){
            if($data['atendido'] == 'ATENDIDO'){
                $reclamos = $reclamos->whereRaw('rec_reclamo_atendido IN (\'ATENDIDO\',\'ATENDIDO_PREGUNTA_ADICIONAL\')');
            }else{
                $reclamos = $reclamos->where('rec_reclamo_atendido',$data['atendido']);
            }

        }
        if($data['escalado']){
            $reclamos = $reclamos->where('rec_redireccionado_dpe',$data['escalado']);
        }

        //estado de los reclamos
        if($data['estado'] == 'PENDIENTE'){
                $reclamos = $reclamos->where(function($query) {
                    $query->where('rec_estado', "PENDIENTE")
                        ->orWhere('rec_estado', "PENDIENTE_PREGUNTA");
                    });
            if($empresa_principal){

                $reclamos = $reclamos->where(function($query) use($empresa_principal,$empresas_asignadas){
                   $query ->whereNull('id_empresa_asignacion')
                        ->orWhere('id_empresa_asignacion', $empresa_principal->empresa_id)
                        ->orWhereIn('id_empresa_asignacion', $empresas_asignadas)
                       ->orWhere('rec_estado', "PENDIENTE_PREGUNTA");
                });
            }

            if($data['desde']){
                $reclamos = $reclamos->where('fecha_creacion','>=',$data['desde'].' 00:00:00');
            }
            if($data['hasta']){
                $reclamos = $reclamos->where('fecha_creacion','<=',$data['hasta'].' 23:59:59');
            }

                if($data['sucursal']){
                    if($empresa_principal){
                    $reclamos = $reclamos->where('id_empresa_creacion',$data['sucursal']);
                }else{
                        $reclamos = $reclamos->where('id_empresa_asignacion',$data['sucursal']);
                    }
            }
        }
        //reclamos cerrados con calificacion
        if($data['estado'] == 'CERRADO'){
            $reclamos = $reclamos->where('rec_estado',$data['estado']);

            $reclamos = $reclamos->whereNotIN('rec_reclamo_atendido',array('NUNCA_ATENDIDO','NO_ATENDIDO_PREGUNTA_ADICIONAL'));

            if($data['desde']){
                $reclamos = $reclamos->where('fecha_creacion','>=',$data['desde'].' 00:00:00');
            }
            if($data['hasta']){
                $reclamos = $reclamos->where('fecha_creacion','<=',$data['hasta'].' 23:59:59');
            }
            if($data['sucursal']){
                $reclamos = $reclamos->where('id_empresa_asignacion',$data['sucursal']);
            }
        }
        //reclamos cerrados nunca atendidos por el proveedor prestador
        if($data['estado'] == 'CERRADO_C'){
            $reclamos = $reclamos->where(function($query){
                $query->where('rec_estado','CERRADO')
                    ->whereIN('rec_reclamo_atendido',array('NUNCA_ATENDIDO','NO_ATENDIDO_PREGUNTA_ADICIONAL'));
            });
            if($data['desde']){
                $reclamos = $reclamos->where('fecha_creacion','>=',$data['desde'].' 00:00:00');
            }
            if($data['hasta']){
                $reclamos = $reclamos->where('fecha_creacion','<=',$data['hasta'].' 23:59:59');
            }
            if($data['sucursal']){
                $reclamos = $reclamos->where('id_empresa_asignacion',$data['sucursal']);
            }
        }

        if($data['estado'] == 'ASIGNADOS'){
            //eliminar el id de la empresa principal
           array_pop($empresas_ids);
            $reclamos = $reclamos->where('rec_estado','PENDIENTE')
                ->whereIn('id_empresa_asignacion',$empresas_ids)
                ->whereNotIn('id_empresa_asignacion',$empresas_asignadas)
            ->whereNull('fecha_atencion');

            if($data['desde']){
                $reclamos = $reclamos->where('fecha_asignacion','>=',$data['desde'].' 00:00:00');
            }
            if($data['hasta']){
                $reclamos = $reclamos->where('fecha_asignacion','<=',$data['hasta'].' 23:59:59');
            }
            if($data['sucursal']){
                $reclamos = $reclamos->where('id_empresa_asignacion',$data['sucursal']);
            }
        }
        if($data['estado'] == 'TRAMITADOS'){
            $reclamos = $reclamos->where('rec_estado','RESPONDIDO');

            if($data['desde']){
                $reclamos = $reclamos->where('fecha_atencion','>=',$data['desde'].' 00:00:00');
            }
            if($data['hasta']){
                $reclamos = $reclamos->where('fecha_atencion','<=',$data['hasta'].' 23:59:59');
            }
            if($data['sucursal']){
                $reclamos = $reclamos->where('id_empresa_asignacion',$data['sucursal']);
            }
        }

        $reclamos = $reclamos->get();
        return $reclamos;
    }


    public function getDatatableConsultarReclamos(){
        $data = Input::all();

        $empresa_principal =  Auth::user()->es_contacto_empresa_principal();
        //si no es usuario de empresa principal, se toman los ids de empresas a su cargo
        if($empresa_principal){
            $empresa_id = $empresa_principal->empresa_id;
            $empresas_ids = Empresa::where('emp_padre_id',$empresa_id)->lists('emp_id');
            $empresas_asignadas = Auth::user()->empresas()->lists('empresa_id');
            array_push($empresas_ids, $empresa_id);
            $empresas_ids = array_merge($empresas_ids,$empresas_asignadas);
        }else{
            $empresas_ids = Auth::user()->empresas()->lists('empresa_id');
        }

        $empresa_y_sucursales=null;
        if($empresa_principal){
            //Obtiene la empresa y todas las sucursales de la empresa.
            $empresa_y_sucursales= Empresa::
                where("emp_estado","=","ACTIVO")
                ->where(function ($query) use ($empresa_principal) {
                    $query-> where("emp_padre_id","=",$empresa_principal->empresa_id)
                        ->orWhere("emp_id","=",$empresa_principal->empresa_id);
                })
                ->lists("emp_id");
            //Obtener  la empresa y las sucursales de las cuales el usuario loggeado es responsable
            $responsable_de_empresas=Auth::user()->empresas->lists('empresa_id');

        }else{
            $responsable_de_empresas=Auth::user()->empresas->lists('empresa_id');
        }

        $reclamos = DB::table('reclamos_consolidados');
        $reclamos= $reclamos->select('rec_numero_reclamo',
                                     'fecha_creacion',
                                      DB::raw("DATE_FORMAT(fecha_creacion, '%H:%i:%s') AS fecha_creacion_hora"),
                                     'uco_nombres',
                                     'uco_telefono',
                                      DB::raw("'proveedor' AS proveedor"),
                                      DB::raw("'sucursal' AS sucursal"),
                                     'rec_calidad_servicio AS rec_calidad_servicio',
                                      DB::raw("'acciones' AS acciones"),
                                     'rec_id','id_empresa_creacion','id_empresa_asignacion','rec_estado','id_empresa_padre_asignacion'
                                );
        $reclamos= $reclamos->join('usuarios_consumidores','reclamos_consolidados.rec_usuario_consumidor_id','=','usuarios_consumidores.uco_id');

        //lista de ids de empresas a cargo del usuario, si es sucursal o principal
        if($empresa_principal){
            $reclamos = $reclamos->whereIn('id_empresa_creacion',$empresas_ids);
        }else{
            $reclamos = $reclamos->whereIn('id_empresa_asignacion',$empresas_ids);
        }

        if($data['consumidor']){
            $reclamos = $reclamos->where('rec_usuario_consumidor_id',$data['consumidor']);
        }
        if($data['numero_reclamo']){
            $reclamos = $reclamos->where('rec_numero_reclamo',$data['numero_reclamo']);
        }
        if($data['desde']){
            $reclamos = $reclamos->where('fecha_creacion','>=',$data['desde'].' 00:00:00');
        }
        if($data['hasta']){
            $reclamos = $reclamos->where('fecha_creacion','<=',$data['hasta'].' 23:59:59');
        }
        if($data['calidad_servicio']){
            $reclamos = $reclamos->where('rec_calidad_servicio',$data['calidad_servicio']);
        }
        if($data['atendido']){
            if($data['atendido'] == 'ATENDIDO'){
                $reclamos = $reclamos->whereRaw('rec_reclamo_atendido IN (\'ATENDIDO\',\'ATENDIDO_PREGUNTA_ADICIONAL\')');
            }else{
                $reclamos = $reclamos->where('rec_reclamo_atendido',$data['atendido']);
            }

        }
        if($data['escalado']){
            $reclamos = $reclamos->where('rec_redireccionado_dpe',$data['escalado']);
        }

        //estado de los reclamos
        if($data['estado'] == 'PENDIENTE'){

            $reclamos=$reclamos
                ->where(function ($query_reclamo) use ($responsable_de_empresas,$empresa_y_sucursales) {
                    $query_reclamo=$query_reclamo
                        ->where(function ($query) use ($responsable_de_empresas) {
                            //reclamos asignados a el usuario actual en estados pendientes
                            $query->whereIN('id_empresa_asignacion',$responsable_de_empresas)
                                ->whereIN("rec_estado",array("PENDIENTE","PENDIENTE_PREGUNTA"));
                        });
                    if($empresa_y_sucursales){
                        $query_reclamo=$query_reclamo
                            ->orWhere(function ($query) use ($empresa_y_sucursales) {
                                //reclamos completamente nuevos
                                $query->whereIN('id_empresa_creacion',$empresa_y_sucursales)
                                    ->where("rec_estado","=","PENDIENTE")
                                    ->whereNull("fecha_asignacion");
                            });
                    }
                });


            if($data['desde']){
                $reclamos = $reclamos->where('fecha_creacion','>=',$data['desde'].' 00:00:00');
            }
            if($data['hasta']){
                $reclamos = $reclamos->where('fecha_creacion','<=',$data['hasta'].' 23:59:59');
            }

            if($data['sucursal']){
                if($empresa_principal){
                    $reclamos = $reclamos->where('id_empresa_creacion',$data['sucursal']);
                }else{
                    $reclamos = $reclamos->where('id_empresa_asignacion',$data['sucursal']);
                }
            }
        }
        //reclamos cerrados con calificacion
        if($data['estado'] == 'CERRADO'){
            $reclamos = $reclamos->where('rec_estado',$data['estado']);

            $reclamos = $reclamos->whereNotIN('rec_reclamo_atendido',array('NUNCA_ATENDIDO','NO_ATENDIDO_PREGUNTA_ADICIONAL'));

                //->whereRaw('rec_calidad_servicio IN (\'SATISFECHO\',\'INSATISFECHO\')');
            if($data['desde']){
                $reclamos = $reclamos->where('fecha_creacion','>=',$data['desde'].' 00:00:00');
            }
            if($data['hasta']){
                $reclamos = $reclamos->where('fecha_creacion','<=',$data['hasta'].' 23:59:59');
            }
            if($data['sucursal']){
                $reclamos = $reclamos->where('id_empresa_asignacion',$data['sucursal']);
            }
        }
        //reclamos cerrados NUNCA ATENDIDOS
        if($data['estado'] == 'CERRADO_C'){
            $reclamos = $reclamos->where(function($query){
                $query->where('rec_estado','CERRADO')
                    ->whereIN('rec_reclamo_atendido',array('NUNCA_ATENDIDO','NO_ATENDIDO_PREGUNTA_ADICIONAL'));
            });

            if($data['desde']){
                $reclamos = $reclamos->where('fecha_creacion','>=',$data['desde'].' 00:00:00');
            }
            if($data['hasta']){
                $reclamos = $reclamos->where('fecha_creacion','<=',$data['hasta'].' 23:59:59');
            }
            if($data['sucursal']){
                $reclamos = $reclamos->where('id_empresa_asignacion',$data['sucursal']);
            }
        }

        if($data['estado'] == 'ASIGNADOS'){

            $reclamos =  $reclamos
                ->where(function ($query) use ($empresa_y_sucursales, $empresa_principal) {
                    //reclamos asignados a el usuario actual en estados pendientes
                    $query->whereIN('id_empresa_asignacion',$empresa_y_sucursales)
                        ->where('id_empresa_asignacion',"<>",$empresa_principal->empresa_id) //empresa padre no tiene asigancion
                        ->whereIN("rec_estado",array("PENDIENTE"));
                });

            if($data['desde']){
                $reclamos = $reclamos->where('fecha_asignacion','>=',$data['desde'].' 00:00:00');
            }
            if($data['hasta']){
                $reclamos = $reclamos->where('fecha_asignacion','<=',$data['hasta'].' 23:59:59');
            }
            if($data['sucursal']){
                $reclamos = $reclamos->where('id_empresa_asignacion',$data['sucursal']);
            }
        }
        if($data['estado'] == 'TRAMITADOS'){
            $reclamos = $reclamos->where('rec_estado','RESPONDIDO');

            if($data['desde']){
                $reclamos = $reclamos->where('fecha_atencion','>=',$data['desde'].' 00:00:00');
            }
            if($data['hasta']){
                $reclamos = $reclamos->where('fecha_atencion','<=',$data['hasta'].' 23:59:59');
            }
            if($data['sucursal']){
                $reclamos = $reclamos->where('id_empresa_asignacion',$data['sucursal']);
            }
        }


        //datatable
        $dt_reclamos = Datatables::of($reclamos);

        $empresa_principal = Auth::user()->es_contacto_empresa_principal();

        $dt_reclamos->editColumn('fecha_creacion', function ($model) {
            return date('Y-m-d', strtotime($model->fecha_creacion));
        });


        $dt_reclamos->editColumn('proveedor', function ($model) use ($data) {
            if ($data['estado'] != 'PENDIENTE') {
                if($model->id_empresa_asignacion)
                    $empresa = Empresa::find($model->id_empresa_asignacion);
                else
                    $empresa=  Empresa::find($model->id_empresa_creacion);
            } else {
                $reclamo = Reclamo::find($model->rec_id);
                if ($data['estado'] == 'PENDIENTE' && $reclamo->es_asignado())
                    $empresa = Empresa::find($model->id_empresa_asignacion);
                else
                    $empresa = Empresa::find($model->id_empresa_creacion);
            }
            return $empresa->es_principal() ? $empresa->emp_razon_social : $empresa->padre()->emp_razon_social;
        });

        $dt_reclamos->editColumn('sucursal', function ($model) use ($data) {
            if ($data['estado'] != 'PENDIENTE') {
                if($model->id_empresa_asignacion)
                    $empresa = Empresa::find($model->id_empresa_asignacion);
                else
                    $empresa=  Empresa::find($model->id_empresa_creacion);
            } else {
                $reclamo = Reclamo::find($model->rec_id);
                if ($data['estado'] == 'PENDIENTE' && $reclamo->es_asignado())
                    $empresa = Empresa::find($model->id_empresa_asignacion);
                else
                    $empresa = Empresa::find($model->id_empresa_creacion);
            }
            return $empresa->es_principal() ?  'MATRIZ' : $empresa->sucursal();
        });

        $dt_reclamos->editColumn('rec_calidad_servicio', function ($model) use ($data) {
            $calidad_servicio= Reclamo::$calidadservicio;
            if($model->rec_calidad_servicio)
                return $calidad_servicio[$model->rec_calidad_servicio];
            else
                return $model->rec_calidad_servicio;
        });

        $dt_reclamos->editColumn('acciones', function ($model) use ($data,$empresa_principal) {
            $reclamo = Reclamo::find($model->rec_id);
            $acccion_column="";
            if ($data['estado']=='CERRADO' || $data['estado']=='CERRADO_C' || $data['estado']=='TRAMITADOS'){
                $acccion_column.="<a class= 'btn btn-info' href= '".action('UPR\ReclamoController@getMostrar', array($model->rec_id))."'>Mostrar <span class='glyphicon glyphicon-eye-open'></span></a>";
            }else{
                //BOTON TRAMITAR
                if($data['estado']!='ASIGNADOS'){
                    $acccion_column.="<a class= 'btn btn-info' href='".action('UPR\TramitacionController@getTramitar', array($model->rec_id))."' >Tramitar <span class='glyphicon glyphicon-pencil'></span></a>";
                }

                $tiene_sucursales = false;

                if($empresa_principal){
                    $sucursales = Empresa::where('emp_padre_id', $empresa_principal->empresa_id)->where("emp_estado","=","ACTIVO")->get();
                    if($sucursales->count() > 0){
                        $tiene_sucursales = true;
                    }else{
                        $tiene_sucursales = false;
                    }
                }

                $texto_accion="";
                if($data['estado']=='ASIGNADOS'){
                    $texto_accion='Reasignar';
                }else{
                    $texto_accion='Asignar';
                }

                if( ($model->rec_estado =='PENDIENTE_PREGUNTA' || $reclamo->es_atendido()) || (!$empresa_principal || !$tiene_sucursales ) || ($data['estado']=='PENDIENTE' && $reclamo->es_asignado()  && $model->id_empresa_padre_asignacion) ){
                    $acccion_column.="";
                }else{
                    $acccion_column.="&nbsp;&nbsp;<a class= 'btn btn-warning' href= '".action('UPR\AsignacionController@getAsignar', array($model->rec_id))."' >".$texto_accion."<span class='glyphicon glyphicon-circle-arrow-right'></span></a>";
                }
            }
            return  $acccion_column;
        });

        $dt_reclamos->editColumn('reclamo_select', function ($model) use ($data,$empresa_principal) {
            $reclamo = Reclamo::find($model->rec_id);
            if ($data['estado'] != 'PENDIENTE') {
                if($model->id_empresa_asignacion)
                    $empresa = Empresa::find($model->id_empresa_asignacion);
                else
                    $empresa=  Empresa::find($model->id_empresa_creacion);
            } else {
                $reclamo = Reclamo::find($model->rec_id);
                if ($data['estado'] == 'PENDIENTE' && $reclamo->es_asignado())
                    $empresa = Empresa::find($model->id_empresa_asignacion);
                else
                    $empresa = Empresa::find($model->id_empresa_creacion);
            }
            if ($model->rec_estado !='PENDIENTE_PREGUNTA' && $empresa_principal && !$reclamo->es_atendido() && !$empresa->es_principal()){

                if(($data['estado']=='PENDIENTE' && $reclamo->es_asignado()  && $model->id_empresa_padre_asignacion))
                    return "";
                else
                    return " <div class='form-group col-md-12' style='text-align:center'>".Form::checkbox('marcar['.$model->rec_id.']',$model->rec_id,false,array('class'=>'reclamos_check form-control','id'=>$model->rec_id)).'</div>';

            }
        });

        return $dt_reclamos->make(TRUE);
    }


    //asignar reclamos de forma masiva
    public function postReclamos()
    {
        $data = Input::all();
        //arreglo con ids de reclamos a asignar
        $reclamos_array = explode(",", $data['marcar_array']);


        try{

            foreach($reclamos_array as $rec){
                $reclamo = Reclamo::find($rec);

                if(!is_null($reclamo)) {
                    if ($reclamo->es_atendido()) {
                        $message = Lang::get('messages.accion_no_permitida');
                        return Redirect::to('upr/inicio')
                            ->with('type_message', $message['alert'])
                            ->with('message', $message['text']);
                    }

                    //Cancelar los procesos de asignacion anteriores
                    $procesos = $reclamo->procesos()->where('pre_asunto','ASIGNACION')->get();;
                    foreach ($procesos as $proceso) {
                        $proceso->pre_estado = 'CANCELADO';
                        $proceso->save();
                    }

                    //nuevo proceso de asignacion
                    $proceso_reclamo = new ProcesoReclamo();
                    $proceso_reclamo->pre_reclamo_id = $reclamo->rec_id;
                    //asignar a empresa a la q estaba dirigida
                    $proceso_reclamo->pre_empresa_id = $reclamo->proceso_creacion()->pre_empresa_id;
                    $proceso_reclamo->pre_usuario_proveedor_id = Auth::user()->upr_id;
                    $proceso_reclamo->pre_asunto = 'ASIGNACION';
                    $dt = new DateTime();
                    $proceso_reclamo->pre_fecha_atencion = $dt->format('y-m-d H:i:s');
                    $proceso_reclamo->pre_estado = 'LISTO';

                    if ($proceso_reclamo->save() && $reclamo->save()) {
                        $message = Lang::get('messages.asignacion_masiva_success');
                    } else {
                        $message = Lang::get('messages.asignacion_masiva_error');
                    }
                }

            }
            return Redirect::to('upr/reclamos/reclamos')
                ->with('type_message', $message['alert'])
                ->with('message', $message['text']);

        } catch (Exception $e) {
            DB::rollback();
            $message = Lang::get('messages.reasignacion_error');
            return Redirect::to('upr/inicio/')
                ->with('type_message', $message['alert'])
                ->with('message', $message['text']);
            throw $e;
        }

    }

    //Mostrar reclamo
    public function getMostrar($id){

        $method = 'PUT';
        $reclamo = Reclamo::find($id);

        $empresa_principal =  Auth::user()->es_contacto_empresa_principal();
        $rango =  Reclamo::$rangos_tiempo;
        //si no es usuario de empresa principal, se toman los ids de empresas a su cargo
        if(!$empresa_principal){
            $ids = Auth::user()->empresas()->lists('empresa_id');
        }else{
            $empresa_id = $empresa_principal->empresa_id;
            $ids = Empresa::where('emp_padre_id',$empresa_id)->lists('emp_id');
            array_push($ids,$empresa_id);
        }

        if($reclamo->proceso_asignacion()){
            //si el reclamo no pertenece a las empresas asignadas al usuario no se permite la accion
            if(!in_array($reclamo->proceso_asignacion()->pre_empresa_id,$ids)){
                $message = Lang::get('messages.accion_no_permitida_error');
                return Redirect::to('upr/inicio')
                    ->with('type_message', $message['alert'])
                    ->with('message', $message['text']);
            }
        }
        else{
            if(!$empresa_principal){
                $message = Lang::get('messages.accion_no_permitida_error');
                return Redirect::to('upr/inicio')
                    ->with('type_message', $message['alert'])
                    ->with('message', $message['text']);
            }
        }

        //si el reclamo no esta cerrado no se permite la accion
        if($reclamo->rec_estado != 'CERRADO' && $reclamo->rec_estado != 'RESPONDIDO'){
            $message = Lang::get('messages.accion_no_permitida_error');
            return Redirect::to('upr/inicio')
                ->with('type_message', $message['alert'])
                ->with('message', $message['text']);
        }

        $consumidor = \UCO\Usuario::find($reclamo->rec_usuario_consumidor_id);
        $empresa = Empresa::find($reclamo->proceso_creacion()->pre_empresa_id);
        if($consumidor->uco_tipo_documento == 'RUC'){
            $is_persona = false;
        }else{
            $is_persona = true;
        }

        //archivos adjuntos consumidor
        $archivos_usuario_consumidor = $reclamo->archivos_usuario_consumidor();

        //archivos adjuntos consumidor
        $archivos_proveedor_prestador = $reclamo->archivos_proveedor_prestador();

        if($empresa->es_principal()){
            $empresa_padre = $empresa;
        }else{
            $empresa_padre = $empresa->padre();
        }
        $representante = $empresa_padre->personas_empresas()->first()->representante();
        $persona_empresa = PersonaEmpresa::where('pem_persona_id',$representante->per_id)->first();

        //proceso de asignacion
        $proceso = $reclamo->procesos()->where('pre_asunto','ASIGNACION')->first();

        if ($reclamo->proceso_asignacion()) {
            $empresa_atencion = $reclamo->proceso_asignacion()->empresa;
        }else{
            $empresa_atencion = $reclamo->proceso_creacion()->empresa;
        }
        //proceso donde se guarda la respuesta al reclamo
        $proceso_respuesta = $reclamo->procesos()->where('pre_asunto','PREGUNTA')->first();
        if(!$proceso_respuesta){
            $proceso_respuesta = $reclamo->procesos()->where('pre_asunto','ATENCION')->first();
        }

        //categorias del reclamos
        $categorias_reclamo = $reclamo->reclamosCategoriasReclamos()->get();
        $string_categorias = '';
        foreach ($categorias_reclamo as $cat_rec) {
            $string_categorias = $string_categorias.' - '.$cat_rec->categoriaReclamos->cre_nombre;
        }
        //tipos / etapas del reclamos
        $etapas_reclamo = $reclamo->reclamosEtapas()->get();
        $string_etapas = '';
        foreach ($etapas_reclamo as $cat_eta) {
            $string_etapas = $string_etapas.' - '.$cat_eta->etapa->eta_nombre;
        }

        $tipo_de_documentos=\UCO\Usuario::$tipos_de_documentos;

        //DATOS CONSUMIDOR
        if ($consumidor->uco_tipo_documento == 'RUC') {
            $consumidor_persona = false;
        } else {
            $consumidor_persona = true;
        }
        $estados_reclamos=Reclamo::$estados_mis_reclamo_consumidor;

        $rec_tipo_productos= Reclamo::$tipos_producto;
        $rec_tipo_productos = (new GlobalFunctions())->unshift_select_item($rec_tipo_productos);

        $this->layout->contenido = View::make('UPR.reclamo.mostrar',[
            'reclamo' => $reclamo,
            'consumidor' => $consumidor,
            'empresa' => $empresa,
            'rango'=>$rango,
            'is_persona'=> $is_persona,
            'archivos_usuario_consumidor'=> $archivos_usuario_consumidor,
            'archivos_proveedor_prestador'=>$archivos_proveedor_prestador,
            'representante'=> $representante,
            'empresa_atencion'=> $empresa_atencion,
            'proceso_respuesta'=> $proceso_respuesta,
            'persona_empresa'=> $persona_empresa,
            'categorias_reclamo'=> $string_categorias,
            'estados_reclamos'=> $estados_reclamos,
            'tipo_de_documentos'=> $tipo_de_documentos,
            'consumidor_persona'=> $consumidor_persona,
            'etapas_reclamo'=> $string_etapas,
            'rec_tipo_productos'=> $rec_tipo_productos,
            'method' => $method
        ]);
    }


    public function numeroTotalReclamosPorEstado(){

        //verifica si es contacto de una empresa principal o de una sucursal
        $empresa_principal =  Auth::user()->es_contacto_empresa_principal();
        $empresa_y_sucursales=null;
        if($empresa_principal){
            //Obtiene la empresa y todas las sucursales de la empresa.
            $empresa_y_sucursales= Empresa::
                where("emp_estado","=","ACTIVO")
                ->where(function ($query) use ($empresa_principal) {
                    $query-> where("emp_padre_id","=",$empresa_principal->empresa_id)
                        ->orWhere("emp_id","=",$empresa_principal->empresa_id);
                })
                ->lists("emp_id");
            //Obtener  la empresa y las sucursales de las cuales el usuario loggeado es responsable
            $responsable_de_empresas=Auth::user()->empresas->lists('empresa_id');

        }else{
            $responsable_de_empresas=Auth::user()->empresas->lists('empresa_id');
        }

        //RECLAMOS ABIERTOS
        $reclamos_abiertos = new \ReclamosConsolidados();
        $reclamos_abiertos=$reclamos_abiertos
                            ->where(function ($query) use ($responsable_de_empresas) {
                                //reclamos asignados a el usuario actual en estados pendientes
                                $query->whereIN('id_empresa_asignacion',$responsable_de_empresas)
                                    ->whereIN("rec_estado",array("PENDIENTE","PENDIENTE_PREGUNTA"))
                                    ->whereNotNull("fecha_asignacion");
                            });
        if($empresa_y_sucursales){// todos los nuevos reclamos saldran
            $reclamos_abiertos=$reclamos_abiertos
                            ->orWhere(function ($query) use ($empresa_y_sucursales) {
                                //reclamos completamente nuevos
                                $query->whereIN('id_empresa_creacion',$empresa_y_sucursales)
                                    ->where("rec_estado","=","PENDIENTE")
                                    ->whereNull("fecha_asignacion");
                            });
        }
        $reclamos_abiertos=$reclamos_abiertos->get();

        //RECLAMOS ASIGNADOS
        $reclamos_asignados = new \ReclamosConsolidados();
        if($empresa_principal){
            $reclamos_asignados =  $reclamos_asignados
                                    ->where(function ($query) use ($empresa_y_sucursales, $empresa_principal) {
                                        //reclamos asignados a el usuario actual en estados pendientes
                                        $query->whereIN('id_empresa_asignacion',$empresa_y_sucursales)
                                            ->where('id_empresa_asignacion',"<>",$empresa_principal->empresa_id) //empresa padre no tiene asigancion
                                            ->whereIN("rec_estado",array("PENDIENTE"));
                                           // ->whereNull("fecha_asignacion");
                                    })->get();
        }else{
            //las sucursales no pueden ver asignaciones por ello se pone null
            $reclamos_asignados=null;
        }

        //RECLAMOS CERRADOS
        $reclamos_cerrados_calificados = new \ReclamosConsolidados();
        if($empresa_principal){
            $reclamos_cerrados_calificados =  $reclamos_cerrados_calificados
                                            ->where("rec_estado","=","CERRADO")
                                            ->whereNotIN('rec_reclamo_atendido',array('NUNCA_ATENDIDO','NO_ATENDIDO_PREGUNTA_ADICIONAL'))

                                            ->whereIN('id_empresa_creacion',$empresa_y_sucursales)
                                            ->get();
        }else{
            $reclamos_cerrados_calificados =  $reclamos_cerrados_calificados
                                            ->where("rec_estado","=","CERRADO")
                                            ->whereNotIN('rec_reclamo_atendido',array('NUNCA_ATENDIDO','NO_ATENDIDO_PREGUNTA_ADICIONAL'))
                                            ->whereIN('id_empresa_asignacion',$responsable_de_empresas)
                                            ->get();
        }

        //RECLAMOS CERRADOS NUNCA ATENDIDOS
        $reclamos_cerrados_no_calificados = new \ReclamosConsolidados();
        if($empresa_principal){
            $reclamos_cerrados_no_calificados =  $reclamos_cerrados_no_calificados
                                                    ->where("rec_estado","=","CERRADO")
                                                    ->whereIN('rec_reclamo_atendido',array('NUNCA_ATENDIDO','NO_ATENDIDO_PREGUNTA_ADICIONAL'))
                                                    ->whereNotIN("rec_calidad_servicio",array("SATISFECHO","INSATISFECHO"))
                                                    ->whereIN('id_empresa_creacion',$empresa_y_sucursales)
                                                    ->get();
        }else{
            $reclamos_cerrados_no_calificados =  $reclamos_cerrados_no_calificados
                                                    ->where("rec_estado","=","CERRADO")
                                                    ->whereNotIN("rec_calidad_servicio",array("SATISFECHO","INSATISFECHO"))
                                                    ->whereIN('id_empresa_asignacion',$responsable_de_empresas)
                                                    ->get();
        }

        //RECLAMOS QUE ESTAN SIENDO TRAMITADOS
        $reclamos_tramitados = new \ReclamosConsolidados();
        if($empresa_principal){
            $reclamos_tramitados =  $reclamos_tramitados
                ->where("rec_estado","=","RESPONDIDO")
                ->whereIN('id_empresa_creacion',$empresa_y_sucursales)
                ->get();
        }else{
            $reclamos_tramitados =  $reclamos_tramitados
                ->where("rec_estado","=","RESPONDIDO")
                ->whereIN('id_empresa_asignacion',$responsable_de_empresas)
                ->get();
        }

        $reclamo_totales['pendientes']=$reclamos_abiertos->count();
        $reclamo_totales['asignados']=$reclamos_asignados?$reclamos_asignados->count():0;
        $reclamo_totales['cerrados_calificados']=$reclamos_cerrados_calificados->count();
        $reclamo_totales['cerrados_no_calificados']=$reclamos_cerrados_no_calificados->count();
        $reclamo_totales['tramitados']=$reclamos_tramitados->count();

         return $reclamo_totales;

    }

}