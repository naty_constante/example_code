<?php
namespace App\Http\Controllers;


use App\models\AuthorizationRequestLocalPurchase;
use App\models\DetailInvoiceLocalPurchase;
use App\models\DetailRequestEquipmentLocalPurchase;
use App\models\EquipmentInventory;
use App\models\Globalfunctions;
use App\models\Customer;
use App\models\Province;
use App\models\Profile;
use App\models\Contact;
use App\models\City;
use App\models\InvoiceLocalPurchase;
use App\models\ReceptionWarehouse;
use App\models\RequestEquipmentLocalPurchase;
use App\models\TempEquipmentFile;
use View;
use Validator;
use Input;
use Response;
use Lang;
use Auth;
use Redirect;
use DB;
use Datatables;
use URL;

class InvoiceEquipmentLocalPurchaseController extends MyBaseController
{

    /*
    * Start Index Functions
    */
    public function getIndex()
    {
        $this->layout->content = View::make(
            'invoice_equipment_local_purchase.index',
            [
                'method' => 'POST'
            ]
        );
    }

    public function getRequestLocalPurchase()
    {
        $view = View::make(
            'invoice_equipment_local_purchase.loads.equipment_local_purchase_table',
            [
                'method' => 'PUT'
            ]
        )->render();

        return Response::json(
            array(
                'html' => $view
            )
        );
    }

    /**
     *  Get equipment local purchases
     */
    public function getRequestLocalPurchaseServerSide()
    {
        $to_date = (Input::get('to_date'));
        $from_date = (Input::get('from_date'));
        $invoice_registration_values = RequestEquipmentLocalPurchase::$invoice_registration_values;

        $equipment_local_purchase = RequestEquipmentLocalPurchase::
            select(
                "request_equipment_local_purchases.id",
                DB::raw('CONCAT(employees.firstname," ",employees.lastname) as employee_fullname'),
                DB::raw('DATE(request_equipment_local_purchases.date) as date'),
                DB::raw('CONCAT(supplier.name," ",supplier.lastname) as supplier_fullname'),
                DB::raw(
                    '(CASE request_equipment_local_purchases.invoice_registration
                                    WHEN "COMPLETED" THEN "' . $invoice_registration_values['COMPLETED'] . '"
                                    WHEN "INCOMPLETE" THEN "' . $invoice_registration_values['INCOMPLETE'] . '"
                                    WHEN "PENDING" THEN "' . $invoice_registration_values['PENDING'] . '"
                     END ) as invoice_registration_lbl'
                ),
                "request_equipment_local_purchases.status",
                "request_equipment_local_purchases.invoice_registration",
                "invoices_local_purchases.id as invoice_lp_id"
            )
            ->join('users', 'request_equipment_local_purchases.user_id', "=", "users.id")
            ->join('employees', 'users.employee_id', "=", "employees.id")
            ->join('supplier', 'request_equipment_local_purchases.supplier_id', "=", "supplier.id")
            ->leftjoin(
                'invoices_local_purchases',
                "request_equipment_local_purchases.id",
                "=",
                "invoices_local_purchases.request_equipment_local_purchase_id"
            );
        //filters by date
        if ($from_date) {
            $equipment_local_purchase = $equipment_local_purchase->where(
                "request_equipment_local_purchases.date",
                ">=",
                $from_date . " 00:00:00"
            );
        }
        if ($to_date) {
            $equipment_local_purchase = $equipment_local_purchase->where(
                "request_equipment_local_purchases.date",
                "<=",
                $to_date . " 23:59:59"
            );
        }
        //it query must be filter by status APPROVED because that means the local purchase have been approved by manager
        $equipment_local_purchase = $equipment_local_purchase->where(
            "request_equipment_local_purchases.status",
            "=",
            "APPROVED"
        );

        $equipment_local_purchase = $equipment_local_purchase->groupBy("request_equipment_local_purchases.id");

        $this->layout = null;
        $equipment_local_purchase = DB::table(DB::raw("({$equipment_local_purchase->toSql()}) as sub"))->mergeBindings(
            $equipment_local_purchase->getQuery()
        );
        //datatable table creation included filters, searching and ordering functions
        $datatable = Datatables::of($equipment_local_purchase)
            ->editColumn(
                'actions',
                function ($result_obj) {
                    $value = '';
                    if ($result_obj->invoice_lp_id) {
                        $value .= '&nbsp;<a class= "btn btn-success"  href="' . URL::to(
                                'invoice_equipment_local_purchase/invoice-registration/' . $result_obj->id
                            ) . '" ><span class="glyphicon glyphicon-folder-open"></span></a>';
                        $value .= '&nbsp;|&nbsp;<a class= "btn btn-primary" target="_blank" href= "' . URL::to(
                                'invoice_equipment_local_purchase/show-invoices/' . $result_obj->id
                            ) . '" target="_blank" ><span class="glyphicon glyphicon-paperclip"></span></a>';

                    } else {
                        $value .= '&nbsp;<a class= "btn btn-success"  href="' . URL::to(
                                'invoice_equipment_local_purchase/invoice-registration/' . $result_obj->id
                            ) . '" ><span class="glyphicon glyphicon-folder-open"></span></a>';
                    }
                    $value .= ' |&nbsp;<a class= "btn btn-info" target="_blank" href= "' . URL::to(
                            'invoice_equipment_local_purchase/show/' . $result_obj->id
                        ) . '" target="_blank" ><span class="glyphicon glyphicon-eye-open"></span></a>';

                    return $value;
                }
            )
            ->filter(
                function ($query) {
                    if (Input::has('search') && Input::get('search') != '') {
                        $search = Input::get('search');
                        $keyword = $search['value'];

                        $query->where(
                            function ($query2) use ($keyword) {
                                $query2->orWhere('employee_fullname', 'LIKE', "%{$keyword}%")
                                    ->orWhere('supplier_fullname', 'LIKE', "%{$keyword}%")
                                    ->orWhere('date', 'LIKE', "%{$keyword}%")
                                    ->orWhere('invoice_registration_lbl', 'LIKE', "%{$keyword}%");
                            }
                        );
                    }

                    if (Input::has('order') && Input::get('order') != '') {
                        $order = Input::get('order');
                        $column = $order[0]['column'];
                        $dir = $order[0]['dir'];

                        $name_col = Input::get('columns');
                        $name_col = $name_col[$column]['data'];
                        $query->orderBy($name_col, $dir);
                    }
                }
            )->make(true);

        return $datatable;
    }

    /*
    * create a view to show local purchase information
    */
    public function getShow($id)
    {
        $purchase = RequestEquipmentLocalPurchase::find($id);
        if ($purchase->status != 'DRAFT') {
            $equipments = $purchase->details;
            //calculate total cost
            $total = 0;
            foreach ($equipments as $e) {
                $total += ($e->amount * $e->unit_price);
            }
            //customer data view
            $customer_id = $purchase->customer_id;

            $customer = Customer::find($customer_id);
            $document = Customer::$document_type_list;
            $provinces = Province::orderBy('name', 'asc')->lists('name', 'id')->toArray();
            $cities = City::where('province_id', '=', $customer->city->province->id)->orderBy(
                'name',
                'asc'
            )->lists('name', 'id')->toArray();
            $provinces = (new Globalfunctions)->unshift_select_item($provinces);
            $cities = (new Globalfunctions)->unshift_select_item($cities);
            $contacts = Contact::where('customer_id', '=', $customer_id)->get();

            //get a customer partial view and include it in current view.
            $customer_view = View::make(
                'rpc.load_customer_info',
                [
                    'contacts' => $contacts,
                    'provinces' => $provinces,
                    'cities' => $cities,
                    'customer' => $customer,
                    'document_type_list' => $document
                ]
            )->render();
            $request_eq_lp_status = RequestEquipmentLocalPurchase::$status_values;

            $this->layout->content = View::make(
                'invoice_equipment_local_purchase.show',
                [
                    'purchase' => $purchase,
                    'equipments' => $equipments,
                    'total' => $total,
                    'customer_view' => $customer_view,
                    'request_eq_lp_status' => $request_eq_lp_status
                ]
            );
        } else {
            $message = Lang::get('messages.unauthorized_action');
            return Redirect::back()
                ->with('type_message', $message['alert'])
                ->with('message', $message['text']);
        }

    }

    /*
    * show invoices registered in a local purchase
    */
    public function getShowInvoices($id)
    {
        $request_local_purchase = RequestEquipmentLocalPurchase::find($id);
        $status = InvoiceLocalPurchase::$status_values;
        $equipments_list = null;
        //get equipments to include in local purchases
        $equipments = DetailRequestEquipmentLocalPurchase::select(
            'details_request_equipment_local_purchases.id as detail_id',
            'model_equipments.name',
            'model_equipments.year',
            'model_equipments.type',
            'brand.name as brand'
        )
            ->join(
                "model_equipments",
                "model_equipments.id",
                "=",
                "details_request_equipment_local_purchases.model_equipment_id"
            )
            ->join('brand', 'brand.id', '=', 'model_equipments.brand_id')
            ->where('request_equipment_local_purchase_id', '=', $id)
            ->orderBy('model_equipments.name')
            ->get();
        if (count($equipments) > 0) {
            foreach ($equipments as $e) {
                $equipments_list[$e->detail_id] = $e->name . ' - ' . $e->brand . ' - ' . $e->type . ' - ' . $e->year;
            }
        }
        $equipments_list = (new Globalfunctions())->unshift_select_item($equipments_list);
        $cont = 0;
        $saved_invoices_view = null;
        $saved_invoices_links = null;
        $invoice_local_purchase = InvoiceLocalPurchase::where('request_equipment_local_purchase_id', '=', $id)->get();
        foreach ($invoice_local_purchase as $invoice_lp) {
            $saved_invoices_link_block = '<li role="presentation" ' . ($cont == 0 ? 'class="active"' : '') . '><a href="#invoice_' . $cont . '" data-toggle="tab">Factura Registrada</a></li>';

            //get detail equipment ... it is used first() because a equipment invoice has one equipment only.
            $detail_invoice_equipment = DetailInvoiceLocalPurchase::where(
                "invoice_local_purchase_id",
                "=",
                $invoice_lp->id
            )->first();

            $file_equipment = null;
            if ($invoice_lp->status == "REGISTERED") {
                //this view shows data from equipment inventory
                $file_equipment = View::make(
                    'global_views.equipment_inventory.show_file_equipment',
                    [
                        'equipment' => EquipmentInventory::where(
                                "invoice_local_purchase_id",
                                "=",
                                $invoice_lp->id
                            )->first()
                    ]
                )->render();
            } else {
                //this view shows data from temp file equipment
                $file_equipment = View::make(
                    'invoice_equipment_local_purchase.loads.show_temp_file_equipment',
                    [
                        'detail_equipment_lp' => $detail_invoice_equipment->detail_equipment,
                        'temp_equipment' => $detail_invoice_equipment->temp_equipment
                    ]
                )->render();
            }

            $saved_invoices_block = View::make(
                'invoice_equipment_local_purchase.loads.show.main_invoice_block',
                [
                    'request_local_purchase' => $request_local_purchase,
                    'equipments_list' => $equipments_list,
                    'invoice_lp' => $invoice_lp,
                    'detail_invoice_equipment' => $detail_invoice_equipment,
                    'status_values' => $status,
                    'cont' => $cont,
                    'file_equipment' => $file_equipment
                ]
            )->render();

            $saved_invoices_view .= $saved_invoices_block;
            $saved_invoices_links .= $saved_invoices_link_block;
            $cont++;
        }

        $main_invoice_view = View::make(
            'invoice_equipment_local_purchase.loads.show.main_invoice',
            [
                'saved_invoices_view' => $saved_invoices_view,
                'saved_invoices_links' => $saved_invoices_links,
                'invoice_local_purchase' => $invoice_local_purchase,
                'cont' => $cont
            ]
        )->render();

        $this->layout->content = View::make(
            'invoice_equipment_local_purchase.loads.show.show',
            [
                'request_local_purchase' => $request_local_purchase,
                'main_invoice_view' => $main_invoice_view,
            ]
        );

    }

    /*
    * End Index Functions
    */

    /*
    * Start Invoices Registration Process Functions
    */
    public function getInvoiceRegistration($id)
    {
        $request_local_purchase = RequestEquipmentLocalPurchase::find($id);
        $invoices_order = null;
        $status_order = InvoiceLocalPurchase::$status_values;


        //load resume order products
        $resume_order = DetailRequestEquipmentLocalPurchase::select(
            "me.name",
            'details_request_equipment_local_purchases.color',
            'details_request_equipment_local_purchases.optionals',
            'details_request_equipment_local_purchases.unit_price',
            'details_request_equipment_local_purchases.amount as request_amount',
            DB::raw('count(dilp.id) as invoices_amount'),
            DB::raw('details_request_equipment_local_purchases.amount-count(dilp.id) as pending_invoice_amount')
        )
            ->leftjoin(
                'details_invoice_local_purchases as dilp',
                'details_request_equipment_local_purchases.id',
                '=',
                'dilp.detail_request_equipment_local_purchase_id'
            )
            ->join(
                'model_equipments as me',
                'me.id',
                '=',
                'details_request_equipment_local_purchases.model_equipment_id'
            )
            ->where(
                'details_request_equipment_local_purchases.request_equipment_local_purchase_id',
                '=',
                $request_local_purchase->id
            )
            ->groupBy('details_request_equipment_local_purchases.id')
            ->get();

        $invoices_order = InvoiceLocalPurchase::where(
            'request_equipment_local_purchase_id',
            '=',
            $request_local_purchase->id
        )
            ->get();

        $this->layout->content = View::make(
            'invoice_equipment_local_purchase.create',
            [
                'request_local_purchase' => $request_local_purchase,
                'resume_order' => $resume_order,
                'invoices_order' => $invoices_order,
                'status_order' => $status_order,
                'method' => 'POST'
            ]
        );
    }
    /*
     * allow register invoices in local purchase
     */
    public function postInvoiceRegistration($id)
    {
        try {
            DB::beginTransaction();

            $data = Input::all();
            $hdn_invoice = $data['hdn_invoice'];

            $request_lp = RequestEquipmentLocalPurchase::find($id);
            //new invoice
            if (!$hdn_invoice) {

                $invoice_number = $data['invoice_number'];
                $authorization_number = $data["authorization_number"];
                $issue_date = $data["issue_date"];
                $subtotal = $data["subtotal"];
                $discount = $data["discount"];
                $ice = $data["ice"];
                $iva = $data["iva"];
                $total = $data["total"];
                //save details invoice
                $detail_equipment_lp = $data['description_p'];
                $unit_price_p = $data['unit_price_p'];

                //save invoices
                $invoice_equipment_lp = new InvoiceLocalPurchase();
                $invoice_equipment_lp->user_id = Auth::user()->id;
                $invoice_equipment_lp->request_equipment_local_purchase_id = $id;
                $invoice_equipment_lp->invoice_number = $invoice_number;
                $invoice_equipment_lp->authorization_number = $authorization_number;
                $invoice_equipment_lp->issue_date = $issue_date;
                $invoice_equipment_lp->subtotal = $subtotal;
                $invoice_equipment_lp->discount = $discount ? $discount : null;
                $invoice_equipment_lp->ice = $ice ? $ice : null;
                $invoice_equipment_lp->iva = $iva;
                $invoice_equipment_lp->invoice_registration = "COMPLETED";
                $invoice_equipment_lp->total = $total;
                $invoice_equipment_lp->created_at = date('Y-m-d H:m:i');

                //get detail equipment local purchase
                $current_detail_equipment_lp = DetailRequestEquipmentLocalPurchase::find($detail_equipment_lp);

                //valid if unit price in request equipment local purchase is less or equal to invoice unit price
                if ($current_detail_equipment_lp->unit_price >= $unit_price_p) {
                    $invoice_equipment_lp->status = "REGISTERED";
                    $invoice_equipment_lp->excess = 'NO';
                } else {
                    $invoice_equipment_lp->status = "PENDING_AUTHORIZATION";
                    $invoice_equipment_lp->excess = 'YES';
                }

                $invoice_equipment_lp->save();

                $id_reception_warehouse = null;
                if ($invoice_equipment_lp->status == 'REGISTERED') {
                    //create reception warehouse
                    $reception_warehouse = new ReceptionWarehouse();
                    $reception_warehouse->status = 'PENDING';
                    $reception_warehouse->save();
                    $id_reception_warehouse = $reception_warehouse->id;
                }

                //save detail
                $detail_invoice_lp = new DetailInvoiceLocalPurchase();
                $detail_invoice_lp->invoice_local_purchase_id = $invoice_equipment_lp->id;
                $detail_invoice_lp->detail_request_equipment_local_purchase_id = $detail_equipment_lp;
                $detail_invoice_lp->reception_warehouse_id = $id_reception_warehouse;
                $detail_invoice_lp->unit_price = $unit_price_p;
                $detail_invoice_lp->amount = 1;
                $detail_invoice_lp->received = 'PENDING';
                $detail_invoice_lp->save();

                $engine_number = $data['engine_number'];
                $chassis_number = $data['chassis_number'];
                $manufacturing = $data['manufacturing'];
                $color = $data['color'];
                $cyl = $data['cyl'];
                $tonnage = $data['tonnage'];
                $cae_ram = $data['cae_ram'];
                $passengers_number = $data['passengers_number'];


                //no included in inventory
                if ($invoice_equipment_lp->status == 'PENDING_AUTHORIZATION') {
                    $temp_equipment_file = new TempEquipmentFile();
                    $temp_equipment_file->engine_number = $engine_number;
                    $temp_equipment_file->chassis_number = $chassis_number;
                    $temp_equipment_file->manufacturing = $manufacturing;
                    $temp_equipment_file->color = $color;
                    $temp_equipment_file->cyl = $cyl;
                    $temp_equipment_file->tonnage = $tonnage;
                    $temp_equipment_file->cae_ram = $cae_ram;
                    $temp_equipment_file->passengers_number = $passengers_number;
                    $temp_equipment_file->optionals = $current_detail_equipment_lp->optionals;
                    $temp_equipment_file->save();

                    //save temp equipment file associated to detail
                    $detail_invoice_lp->temp_equipment_file_id = $temp_equipment_file->id;
                    $detail_invoice_lp->save();

                    //get profile general manager
                    $profile_general_manager = Profile::where("name_key", '=', 'GENERAL_MANAGER')->first();

                    //create authorization row to invoice
                    $authorization_invoice = new AuthorizationRequestLocalPurchase();
                    $authorization_invoice->status = 'PENDING';
                    $authorization_invoice->profile_id = $profile_general_manager->id; // profile "GERENTE GENERAL"
                    $authorization_invoice->invoice_local_purchase_id = $invoice_equipment_lp->id;
                    $authorization_invoice->save();
                }
                //Included in inventory
                if ($invoice_equipment_lp->status == 'REGISTERED') {
                    $equipment_file = new EquipmentInventory();
                    $equipment_file->model_equipment_id = $current_detail_equipment_lp->model_equipment_id;
                    $equipment_file->invoice_local_purchase_id = $invoice_equipment_lp->id;
                    $equipment_file->warehouse_id = 9; //Quito Matriz
                    $equipment_file->unit_price = $detail_invoice_lp->unit_price;
                    $equipment_file->amount = $detail_invoice_lp->amount;
                    $equipment_file->stock_location = 'TRANSIT';
                    $equipment_file->status = 'IN_STOCK';
                    $equipment_file->engine_number = $engine_number;
                    $equipment_file->chassis_number = $chassis_number;
                    $equipment_file->manufacturing = $manufacturing;
                    $equipment_file->color = $color;
                    $equipment_file->cyl = $cyl;
                    $equipment_file->tonnage = $tonnage;
                    $equipment_file->cae_ram = $cae_ram;
                    $equipment_file->passengers_number = $passengers_number;
                    $equipment_file->optionals = $current_detail_equipment_lp->optionals;
                    $equipment_file->save();
                }
                $message = Lang::get('messages.invoice_equipment_create_success');

                /*
                 * check if registration invoices is complete. it's necessary count the products requested in a order,
                 *  count the invoices registered  and then compare them.
                 */
                $total_products_requested = DetailRequestEquipmentLocalPurchase::select(
                    DB::raw('SUM(amount) as total_products')
                )->where("request_equipment_local_purchase_id", "=", $id)->first();
                $total_invoice_registered = InvoiceLocalPurchase::select(DB::raw('SUM(amount) as total_products'))
                    ->join(
                        "details_invoice_local_purchases as dilp",
                        'dilp.invoice_local_purchase_id',
                        '=',
                        'invoices_local_purchases.id'
                    )
                    ->where("invoices_local_purchases.request_equipment_local_purchase_id", "=", $id)->first();

                if ($total_products_requested->total_products > $total_invoice_registered->total_products) {
                    $request_lp->invoice_registration = 'INCOMPLETE';
                } else {
                    $request_lp->invoice_registration = 'COMPLETED';
                }
                $request_lp->save();

            } else {

                //update invoice
                //if status invoice is PENDING_AUTHORIZATION and auth user is sadmin it is means that the invoice is being UPDATED
                $invoice_equipment_lp = InvoiceLocalPurchase::find($hdn_invoice);
                if (Auth::user()->profile_id == 1) {

                    $invoice_numbers = $data['invoice_number'];
                    $authorization_numbers = $data["authorization_number"];
                    $issue_dates = $data["issue_date"];
                    $subtotals = $data["subtotal"];
                    $discounts = $data["discount"];
                    $ice = $data["ice"];
                    $iva = $data["iva"];
                    $totals = $data["total"];

                    $unit_price_p = $data['unit_price_p'];
                    //equipment file
                    $engine_number = isset($data['engine_number']) ? $data['engine_number'] : null;
                    $chassis_number = isset($data['chassis_number']) ? $data['chassis_number'] : null;
                    $manufacturing = isset($data['manufacturing']) ? $data['manufacturing'] : null;
                    $color = isset($data['color']) ? $data['color'] : null;
                    $cyl = isset($data['cyl']) ? $data['cyl'] : null;
                    $tonnage = isset($data['tonnage']) ? $data['tonnage'] : null;
                    $cae_ram = isset($data['cae_ram']) ? $data['cae_ram'] : null;
                    $passengers_number = isset($data['passengers_number']) ? $data['passengers_number'] : null;

                    if ($invoice_equipment_lp->status == 'PENDING_AUTHORIZATION' || $invoice_equipment_lp->status == 'REGISTERED') {
                        $invoice_equipment_lp->user_id = Auth::user()->id;
                        $invoice_equipment_lp->invoice_number = $invoice_numbers;
                        $invoice_equipment_lp->authorization_number = $authorization_numbers;
                        $invoice_equipment_lp->issue_date = $issue_dates;
                        $invoice_equipment_lp->subtotal = $subtotals;
                        $invoice_equipment_lp->discount = $discounts ? $discounts : null;
                        $invoice_equipment_lp->ice = $ice ? $ice : null;
                        $invoice_equipment_lp->iva = $iva;
                        $invoice_equipment_lp->total = $totals;
                        $invoice_equipment_lp->save();

                        //get detail
                        $detail_invoice_lp = DetailInvoiceLocalPurchase::where(
                            "invoice_local_purchase_id",
                            "=",
                            $hdn_invoice
                        )->first();
                        $detail_invoice_lp->unit_price = $unit_price_p;
                        $detail_invoice_lp->save();

                        if ($invoice_equipment_lp->status == 'REGISTERED') {
                            $equipment_inv = EquipmentInventory::where(
                                "invoice_local_purchase_id",
                                "=",
                                $invoice_equipment_lp->id
                            )->first();
                            $equipment_inv->unit_price;
                            $equipment_inv->save();
                        }
                    }
                    //if exist manufacturing[k] it means that temp equipment file can be UPDATED
                    if ($invoice_equipment_lp->status == 'PENDING_AUTHORIZATION' && isset($manufacturing)) {
                        $temp_equipment_id = $detail_invoice_lp->temp_equipment_file_id;
                        $temp_equipment_file = TempEquipmentFile::find($temp_equipment_id);
                        $temp_equipment_file->engine_number = $engine_number;
                        $temp_equipment_file->chassis_number = $chassis_number;
                        $temp_equipment_file->manufacturing = $manufacturing;
                        $temp_equipment_file->color = $color;
                        $temp_equipment_file->cyl = $cyl;
                        $temp_equipment_file->tonnage = $tonnage;
                        $temp_equipment_file->cae_ram = $cae_ram;
                        $temp_equipment_file->passengers_number = $passengers_number;
                        $temp_equipment_file->save();
                    }

                    $message = Lang::get('messages.invoice_equipment_update_success');
                } else {
                    $message = Lang::get('messages.invoice_equipment_lp_action_not_allowed');
                    return redirect('invoice_equipment_local_purchase/index')
                        ->with('type_message', $message['alert'])
                        ->with('message', $message['text']);
                }
            }

            DB::commit();

            return redirect('invoice_equipment_local_purchase/invoice-registration/' . $id)
                ->with('type_message', $message['alert'])
                ->with('message', $message['text']);

        } catch (Exception $e) {
            DB::rollback();
            $message = Lang::get('messages.invoice_equipment_create_error');
            return redirect('invoice_equipment_local_purchase/index')
                ->with('type_message', $message['alert'])
                ->with('message', $message['text']);
        }
    }

    // create a invoice that belong to a order
    public function postFormInvoice()
    {
        $invoice_id = (Input::get('invoice'));
        $request_local_purchase_id = (Input::get('request_local_purchase'));
        $request_local_purchase = RequestEquipmentLocalPurchase::find($request_local_purchase_id);
        $equipments_list = null;
        $status_values = InvoiceLocalPurchase::$status_values;

        $equipments = DetailRequestEquipmentLocalPurchase::select(
            'details_request_equipment_local_purchases.id as detail_id',
            'model_equipments.name',
            'model_equipments.year',
            'model_equipments.type',
            'brand.name as brand',
            'details_request_equipment_local_purchases.unit_price as unit_price',
            'details_request_equipment_local_purchases.optionals as optionals'
        )
            ->join(
                "model_equipments",
                "model_equipments.id",
                "=",
                "details_request_equipment_local_purchases.model_equipment_id"
            )
            ->join('brand', 'brand.id', '=', 'model_equipments.brand_id')
            ->where('request_equipment_local_purchase_id', '=', $request_local_purchase_id)
            ->orderBy('model_equipments.name')
            ->get();
        if (count($equipments) > 0) {
            foreach ($equipments as $e) {
                $optionals = ($e->optionals) ? " - " . $e->optionals : "";
                $unit_price = ($e->unit_price) ? " - $" . $e->unit_price : "";
                $equipments_list[$e->detail_id] = $e->name . ' - ' . $e->brand . ' - ' . $e->type . ' - ' . $e->year . ' - ' . $unit_price . $optionals;
            }
        }

        $equipments_list = (new Globalfunctions())->unshift_select_item($equipments_list);

        //create a html for a new invoice
        if ($invoice_id == "new") {

            $invoice_html = View::make(
                'invoice_equipment_local_purchase.loads.main_invoice_block',
                [
                    'request_local_purchase' => $request_local_purchase,
                    'equipments_list' => $equipments_list,
                    'form_invoice_type' => 'new'
                ]

            )->render();

        } else {
            //create a html for update invoice

            $invoice_lp = InvoiceLocalPurchase::find($invoice_id);
            $file_equipment = null;

            //get detail equipment ... it is used fist() because a equipment invoice has one equipment only.
            $detail_invoice_equipment = DetailInvoiceLocalPurchase::where(
                "invoice_local_purchase_id",
                "=",
                $invoice_lp->id
            )->first();

            if ($invoice_lp->status == "REGISTERED") {
                $equip = EquipmentInventory::where(
                    "invoice_local_purchase_id",
                    "=",
                    $invoice_lp->id
                )->first();

                //this view shows data from equipment inventory
                $file_equipment = View::make(
                    'global_views.equipment_inventory.show_file_equipment',
                    [
                        'equipment' => $equip
                    ]
                )->render();
            } elseif ($invoice_lp->status == "PENDING_AUTHORIZATION") {
                //only superadmin can edit temp file equipments
                if (Auth::user()->profile_id == 1) {
                    //this view allow to edit temp file equipment
                    $file_equipment = View::make(
                        'invoice_equipment_local_purchase.loads.file_equipment',
                        [
                            'detail_equipment_lp' => $detail_invoice_equipment->detail_equipment,
                            'temp_equipment' => $detail_invoice_equipment->temp_equipment
                        ]
                    )->render();
                } else {
                    //this view DO NOT allow to edit temp file equipment
                    $file_equipment = View::make(
                        'invoice_equipment_local_purchase.loads.show_temp_file_equipment',
                        [
                            'detail_equipment_lp' => $detail_invoice_equipment->detail_equipment,
                            'temp_equipment' => $detail_invoice_equipment->temp_equipment
                        ]
                    )->render();
                }
            } else {
                //this view DO NOT allow to edit temp file equipment
                $file_equipment = View::make(
                    'invoice_equipment_local_purchase.loads.show_temp_file_equipment',
                    [
                        'detail_equipment_lp' => $detail_invoice_equipment->detail_equipment,
                        'temp_equipment' => $detail_invoice_equipment->temp_equipment
                    ]
                )->render();
            }

            //save in a variable a value to identify that just sadmin can edit a invoice
            if (Auth::user()->id == 1) {
                $form_invoice_type = 'update';
            } else {
                $form_invoice_type = 'show';
            }
            $invoice_html = View::make(
                'invoice_equipment_local_purchase.loads.main_invoice_block',
                [
                    'request_local_purchase' => $request_local_purchase,
                    'equipments_list' => $equipments_list,
                    'invoice_lp' => $invoice_lp,
                    'detail_invoice_equipment' => $detail_invoice_equipment,
                    'status_values' => $status_values,
                    'file_equipment' => $file_equipment,
                    'form_invoice_type' => $form_invoice_type
                ]
            )->render();

        }


        $this->layout = null;
        return $invoice_html;
    }

    public function postLoadDetail()
    {

        $detail_id = Input::get("detail_id");
        $detail_equipment_lp = DetailRequestEquipmentLocalPurchase::find($detail_id);

        //validate if the product amount required in the request equipment local purchase is complete.
        //if it is complete the new product can't be add in a invoice.
        $required_amount = $detail_equipment_lp->amount;
        $current_details_amount = DetailInvoiceLocalPurchase::where(
            "detail_request_equipment_local_purchase_id",
            "=",
            $detail_equipment_lp->id
        )->count();

        $data['total_request'] = 'COMPLETED';
        $data['detail'] = $detail_equipment_lp;

        if ($current_details_amount < $required_amount) {
            $data['total_request'] = 'INCOMPLETE';
        }

        $file_equipment = View::make(
            'invoice_equipment_local_purchase.loads.file_equipment',
            [
                'detail_equipment_lp' => $detail_equipment_lp
            ]
        )->render();

        $data['file_equipment'] = $file_equipment;
        return Response::json($data);
    }

    public function postIsEngineNumberUnique()
    {
        //Unique engine number
        $validation_a = Validator::make(
            Input::all(),
            ['engine_number' => 'unique:equipment_inventories,engine_number']
        );

        $validation_b = Validator::make(
            Input::all(),
            ['engine_number' => 'unique:temp_equipment_files,engine_number,' . Input::get('temp_equipment_id')]
        );

        $validation_result = null;
        if ($validation_a->passes() && $validation_b->passes()) {
            $validation_result = true;
        } else {
            $validation_result = false;
        }

        return Response::json($validation_result);
    }

    public function postIsChassisNumberUnique()
    {
        //Unique chassis number
        $validation_a = Validator::make(
            Input::all(),
            ['chassis_number' => 'unique:equipment_inventories,chassis_number']
        );

        $validation_b = Validator::make(
            Input::all(),
            ['chassis_number' => 'unique:temp_equipment_files,chassis_number,' . Input::get('temp_equipment_id')]
        );

        $validation_result = null;
        if ($validation_a->passes() && $validation_b->passes()) {
            $validation_result = true;
        } else {
            $validation_result = false;
        }

        return Response::json($validation_result);
    }

    public function postIsInvoiceNumberUnique()
    {
        //Unique chassis number

        $invoice_lp = InvoiceLocalPurchase::join(
            'request_equipment_local_purchases as rlp',
            'rlp.id',
            '=',
            'invoices_local_purchases.request_equipment_local_purchase_id'
        )
            ->where('invoice_number', '=', Input::get('invoice_number'))
            ->where('supplier_id', '=', Input::get('supplier_id'));

        if (Input::get('invoice_id')) {
            $invoice_lp = $invoice_lp->where("invoices_local_purchases.id", "<>", Input::get('invoice_id'));
        }

        $invoice_lp = $invoice_lp->get();

        if ($invoice_lp->count() > 0) {
            $validation_result = false;
        } else {
            $validation_result = true;
        }


        return Response::json($validation_result);
    }

    /*
     * End Invoices Registration Process Functions
     */

}