@section('content')
<div class="col-md-12">
    <h3 class="text-center"> Registro de facturas - Compra local de equipos </h3>
    <br>
    <div class="row ">
        <div class="col-md-offset-7 col-md-5 style_order_number">
            <div class="col-md-8 text-left ">
                Nro. de orden de compra:
            </div>
            <div class="col-md-4 text-left">
                {!! $request_local_purchase->id !!}
            </div>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-12">
            {!! Form::label(null,'Resumen de equipos solicitados en orden de compra'); !!}
            <a id="show_hide_resume" style="cursor: pointer"> Mostrar / Ocultar informaci&oacute;n</a>
        </div>
        <div class="col-md-offset-1 col-md-10 " id="data_resume" style="display: none">
            <br>
            <div class="col-md-12 alert alert-data">
                @if(count($resume_order)>0)
                <table class="table table-bordered alert">
                    <thead>
                    <tr class="style_title_th">
                        <th class="text-center">Equipo</th>
                        <th class="text-center">Color</th>
                        <th class="text-center">Opcionales</th>
                        <th class="text-center">Precio unitario</th>
                        <th class="text-center">Cantidad solicitada</th>
                        <th class="text-center">Cantidad registrada en facturas</th>
                        <th class="text-center">Cantidad por registrar en facturas</th>
                    </tr>
                    </thead>
                    <tbody style="background-color: white">
                    @foreach($resume_order as $item)
                    <tr>
                        <td>{!! $item->name !!}</td>
                        <td>{!! $item->color?$item->color:'-' !!}</td>
                        <td>{!! $item->optionals?$item->optionals:'-' !!}</td>
                        <td class="text-center">{!! $item->unit_price !!}</td>
                        <td class="text-center">{!! $item->request_amount !!}</td>
                        <td class="text-center">{!! isset($item->invoices_amount)?$item->invoices_amount:0 !!}</td>
                        <td class="text-danger text-center"><b>{!! isset($item->pending_invoice_amount)?$item->pending_invoice_amount:$item->request_amount !!}</b></td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
                <div class="text-right">
                    <div class="col-md-12">Para mayor detalle <a href="{{ url('invoice_equipment_local_purchase/show',array($request_local_purchase->id))}}"  class="btn btn-default" target="_blank"> Ir a la orden de compra <span class="glyphicon glyphicon-arrow-right"></span></a></div>
                </div>
            </div>

        </div>
    </div>

    <div class="row col-md-12">

        <div class="col-md-12">
            <legend class="legend_color">
                Facturas asociadas a la orden
            </legend>
        </div>
        @if($request_local_purchase->invoice_registration != 'COMPLETED')
        <div id="invoices" class="col-md-12 form-group">
            {!! Form::button('Agregar Nueva Factura  <span class="glyphicon glyphicon-plus"></span>', array('key'=>'new','type' => 'button', 'class' => 'invoice_btn btn btn-info')) !!}
        </div>
        @endif
        <div class="col-md-offset-1 col-md-10">
            <div class="col-md-12 ">
                @if(count($invoices_order)>0)
                <table class="table" id="invoices_table">
                    <thead>
                    <tr >
                        <th class="text-center">N&uacute;mero de factura</th>
                        <th class="text-center">Fecha de emisi&oacute;n de la factura</th>
                        <th class="text-center">Factura registrada por</th>
                        <th class="text-center">Fecha de registro de la factura</th>
                        <th class="text-center">Estado</th>
                        <th class="text-center">Aciones</th>
                    </tr>
                    </thead>
                    <tbody style="background-color: white">
                    @foreach($invoices_order as $item)
                    <tr>
                        <td>{!! $item->invoice_number !!}</td>
                        <td>{!! $item->issue_date !!}</td>
                        <td>{!! $item->user->employee->full_name !!}</td>
                        <td>{!! $item->created_at !!}</td>
                        <td>{!! $status_order[$item->status] !!}</td>
                        @if(Auth::user()->profile_id==1)
                            <td class="text-danger text-center"> <a  class="invoice_btn btn btn-warning" key="{{ $item->id }}" ><span class="glyphicon glyphicon-edit"></span> Editar</a></td>
                        @else
                            <td class="text-danger text-center"> <a  class="invoice_btn btn btn-info" key="{{ $item->id }}" ><span class="glyphicon glyphicon-eye-open"></span> Ver</a></td>
                        @endif
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
                {!! Form::hidden('previous_invoice_cont',count($invoices_order), array('class' => 'form-control', 'id'=>'previous_invoice_cont')) !!}
            </div>
        </div>
    </div>
    {!! Form::model(null, array('id' => 'invoice_purchase_equipment_form', 'method' => $method)) !!}

    <div class="col-md-12">
        <br>
        <div class="row" style="margin-bottom: 30px;">

            <div class="col-md-12" id="content_invoices_div" style="display: none">
                <div class="alert alert-invoice">
                    <div class="tab-content " id="tab_content_invoices" >
                    </div>
                    <div class="text-center row">
                        <br>
                        {!! Form::button('Guardar', array(''=>'save', 'type' => 'submit', 'class' => 'btn btn-info')) !!}
                        <a class="btn btn-danger" href="{{ url('invoice_equipment_local_purchase/index') }}">Cancelar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 text-left">
        <hr>
        <a href="{{ url('invoice_equipment_local_purchase/index') }}" onclick="self.close()" class="btn btn-info"><span class="glyphicon glyphicon-circle-arrow-left"></span> Regresar</a>
    </div>

        <!-- Update invoice -->
        <div class="modal fade" id="invoiceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                            <h4 class="modal-title disabled" id="invoice_modal_label"></h4>
                    </div>
                    <div id="container_invoice" class="modal-body form-group">
                    </div>
                    <div class="modal-footer">
                            <button type="submit" id="save_updated_invoice" class="btn btn-info">Guardar</button>
                            <button type="button" id="close_invoice" class="btn btn-default">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</div>



<script type="text/javascript">
    $(function () {


        $('#invoice_purchase_equipment_form')[0].reset();

        $("#show_hide_resume").click(function(){
            $("#data_resume").toggle();
        });

        datatable_invoices();

        $('#invoice_purchase_equipment_form').validate({
            ignore: [],
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error').removeClass('has-success');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                var msg = '';

                if($("#hdn_invoice").val()){
                    msg = "\xbfEst\xE1 seguro que desea guardar esta factura? Recuerde que el equipo en la factura comenzar&aacute;n a formar parte del inventario.<br>";
                }else{
                    msg ="\xbfEst\xE1 seguro que desea guardar los cambios en la(s) factura(s)?"
                }

                    bootbox.confirm({
                        title: "Confirmaci\xF3n",
                        message: msg,
                        buttons: {
                            'confirm': {
                                label: 'Aceptar',
                                className: 'btn-primary btn-margin-right'
                            },
                            'cancel': {
                                label: 'Cancelar',
                                className: ' btn btn-default pull-right margin-left'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                show_ajax_loader();
                                $('button[type="submit"]').html('Espere por favor...');
                                $('button[type="submit"]').prop('disabled', true);
                                form.submit();
                            }
                        }
                    });
            }
        });

        $('textarea').autosize();

        //create or update new equipment modal
        $(".invoice_btn").click(function () {
            var invoice_k = $(this).attr("key");
                $('#invoiceModal').modal({
                    show:true,
                    backdrop: 'static',
                    keyboard: false // to prevent closing with Esc button (if you want this too)
                });
                $("#close_invoice").html("Cancelar");
                //load new equipment view
                $("#container_invoice").html("");
                show_ajax_loader();
                $("#container_invoice").load('{{ action("InvoiceEquipmentLocalPurchaseController@postFormInvoice") }}',
                    {
                        _token: '{{ Session::token() }}',
                        invoice:invoice_k,
                        request_local_purchase:'{{ $request_local_purchase->id}}'
                    },
                    function(){
                        hide_ajax_loader();
                        if(invoice_k=="new"){
                            $("#invoice_modal_label").text("Nueva Factura");
                            $("#save_updated_invoice").show();
                            functions_new_invoice();
                        }else{
                            if($("#hdn_form_invoice_type").val()=='update'){
                                $("#invoice_modal_label").text("Actualizar Factura");
                                $("#save_updated_invoice").show();
                                functions_update_invoice();
                            }else{
                                $("#invoice_modal_label").text("Ver Factura");
                                $("#save_updated_invoice").hide();
                                $("#close_invoice").html("Cerrar");
                            }
                        }
                        $('#close_invoice').click(function () {
                            $("#container_invoice").html("");
                            $('#invoiceModal').modal('hide');
                        });

                    });
            });
    });

    //  BLOCKS  INVOICE
    function functions_new_invoice() {
        add_rules_invoice();
        $("#description_p").select2()
            .change(function(){
                if($(this).valid() && $(this).val()){
                    load_data_detail($("#description_p").val());
                    //file equipment
                    $("#file_p").show();
                }else{
                    $("#amount_p").val("");
                    $("#unit_price_p").val("");
                    $("#total_price_p").val("");
                    $("#discount").rules("remove","max");
                    $("#discount").valid();
                    clean_totals();
                    //file equipment
                    $("#file_p").hide();
                }
            });

        $('#issue_date').datepicker({
            dateFormat: "yy-mm-dd",
            maxDate: 0,
            defaultDate: 0,
            changeMonth: true,
            changeYear: true,
            yearRange: '1914:date("Y")',
            onSelect: function (dateText, inst) {
                $('#issue_date').valid();
            }
        });
        $('#issue_date').dblclick(function () {
            $(this).val("");
        });

        hide_ajax_loader()
    }

    function functions_update_invoice() {
        if ($("#manufacturing").length > 0) {
            add_rules_invoice();
            calculate_product_total();

            $("#unit_price_p").keyup(function(){
                calculate_product_total();
            });

            $("#unit_price_p").attr("readonly", false);
            $("#discount" + ", #ice" + ", #iva").keyup(function () {
                calculate_invoice_total();
            });

            add_rules_file_equipment();
        }
        else {
            if ($("#invoice_number").length > 0) {
                header_invoice_rules();
                $("#discount" + ", #ice" + ", #iva").keyup(function () {
                    calculate_invoice_total();
                });
            }
        }

        if ($('#issue_date').length > 0) {
            $('#issue_date').datepicker({
                dateFormat: "yy-mm-dd",
                maxDate: 0,
                defaultDate: 0,
                changeMonth: true,
                changeYear: true,
                yearRange: '1914:date("Y")',
                onSelect: function (dateText, inst) {
                    $('#issue_date').valid();
                }
            });
            $('#issue_date').dblclick(function () {
                $(this).val("");
            });
        }
    }

    function load_data_detail(detail_id){
        show_ajax_loader();
        $.ajax({
            type: 'POST',
            url: '{{ action("InvoiceEquipmentLocalPurchaseController@postLoadDetail") }}',
            async: false,
            data: {
                _token: '{{ Session::token() }}',
                detail_id: detail_id
            },
            success: function (data) {
                if(data.total_request=="INCOMPLETE"){
                    $("#unit_price_p").val(data.detail.unit_price);
                    $("#amount_p").val($("#hdn_amount_p").val());
                    calculate_product_total();
                    $("#unit_price_p").attr("readonly",false);

                    //calculate total
                    $("#discount, #ice, #iva").keyup(function(){
                        if( $("#description_p").valid()){
                            calculate_invoice_total();
                        }
                    });

                    $("#unit_price_p").keyup(function(){
                        calculate_product_total();
                    });

                    //file equipment
                    $("#file_p").html("");
                    $("#file_p").append(data.file_equipment);
                    add_rules_file_equipment();
                }else{
                    $("#description_p").select2('val','');
                    bootbox.confirm({
                        title: "Aviso",
                        message: "Ya se han registrado en facturas todas las unidades solicitadas del equipo seleccionado.",
                        buttons: {
                            'confirm': {
                                label: 'Aceptar',
                                className: 'btn-primary btn-margin-right'
                            },
                            'cancel': {
                                label: 'Cancelar',
                                className: ' btn btn-default pull-right margin-left'
                            }
                        },
                        callback: function (result) {
                            $("#description_p").valid();
                        }
                    });
                }
                hide_ajax_loader();
            }
        });
    }

    function calculate_product_total() {
        var total = 0;
        if ($("#unit_price_p").valid() && $("#amount_p").valid()) {
            total = (parseFloat($("#amount_p").val()) * parseFloat($("#unit_price_p").val()));
            $("#total_price_p").val(parseFloat(total).toFixed(2));
            $("#discount").rules("add", {max: total});
            $("#discount").valid();
            calculate_invoice_total();
        } else {
            $("#total_price_p").val("");
            $("#discount").rules("remove",'max');
            $("#discount").valid();
            clean_totals();
        }
        $("#total_price_p").valid();

    }

    function calculate_invoice_total() {
        var total_invoice = 0;

        total_invoice = $("#total_price_p").val();
        $("#subtotal").val(parseFloat(total_invoice).toFixed(2));
        $("#subtotal").valid();

        if ($("#discount").val() && $("#discount").valid()) {
            if (parseFloat(total_invoice) >= parseFloat($("#discount").val())) {
                total_invoice = parseFloat(total_invoice) - parseFloat($("#discount").val());
            }
        }

        //ice
        if ($("#ice").val() && $("#ice").valid()) {
            total_invoice = parseFloat(total_invoice) + parseFloat($("#ice").val());
        }
        //iva
        if ($("#iva").val() && $("#iva").valid()) {
            total_invoice = parseFloat(total_invoice) + parseFloat($("#iva").val());
        }
        //total
        $("#total").val(parseFloat(total_invoice).toFixed(2));
        $("#total").valid();
        $("#discount").valid();
    }

    function add_rules_invoice() {

        //header
       header_invoice_rules();

        //totals
        $("#subtotal").rules("add", {required: true, number: true, minStrict: 0, decimals: true});
        $("#discount").rules("add", {number: true, decimals: true});
        $("#ice").rules("add", {number: true, decimals: true});
        $("#iva").rules("add", {required: true, number: true, decimals: true});
        $("#total").rules("add", {required: true, number: true, minStrict: 0, decimals: true});

        //equipment
        $("#amount_p").rules("add", {required: true, digits: true, minStrict: 0});
        $("#unit_price_p").rules("add", {required: true, number: true, minStrict: 0, decimals: true});
        $("#total_price_p").rules("add", {required: true});

        //validation not necessary in update
        if (!$("#hdn_invoice").val()) {
            $("#description_p").rules("add", {required: true});
        }
    }

    function header_invoice_rules(){
        $("#invoice_number").rules("add", {required: true,
            remote: {
                url: '{{ action("InvoiceEquipmentLocalPurchaseController@postIsInvoiceNumberUnique") }}',
                type: 'post',
                data: {
                    _token: '{{ Session::token() }}',
                    invoice_id: $("#hdn_invoice").val(),
                    supplier_id: ' {{ $request_local_purchase->supplier_id }}',
                    invoice_number: function () {
                        return ($("#invoice_number").val()).trim();
                    }
                }
            }, messages: {
                remote: "Ya existe una equipo con el mismo n&uacute;mero de factura para el actual proveedor."
            }
        });
        $("#issue_date").rules("add", {required: true});
    }

    function add_rules_file_equipment() {
        //file equipment rules
        $("#manufacturing").rules("add", {required: true, maxlength: 50});
        $("#color").rules("add", {required: true, maxlength: 50});
        $("#engine_number").rules("add",{required:true, maxlength: 50,
            notEqualToGroup: ['.temp_equipment_engine'],
            remote: {
                url: '{{ action("InvoiceEquipmentLocalPurchaseController@postIsEngineNumberUnique") }}',
                type: 'post',
                data: {
                    _token: '{{ Session::token() }}',
                    temp_equipment_id:$("#hdn_temp_equipment_id").val(),
                    engine_number: function () {
                        return ($("#engine_number").val()).trim();
                    }
                }
            },messages:{
                notEqualToGroup: "El n&uacute;mero de motor ya se est&aacute; usando.",
                remote:"Ya existe un equipo con el mismo n&uacute;mero de motor."
            }
        });
        $("#chassis_number").rules("add",{required:true, maxlength: 50,
            notEqualToGroup: ['.temp_equipment_chassis'],
            remote: {
                url: '{{ action("InvoiceEquipmentLocalPurchaseController@postIsChassisNumberUnique") }}',
                type: 'post',
                data: {
                    _token: '{{ Session::token() }}',
                    temp_equipment_id: $("#hdn_temp_equipment_id").val(),
                    chassis_number: function () {
                        return ($("#chassis_number").val()).trim();
                    }
                }
            },messages:{
                notEqualToGroup: "El n&uacute;mero de chasis ya se est&aacute; usando.",
                remote:"Ya existe un equipo con el mismo n&uacute;mero de chasis."
            }
        });
        $("#cyl").rules("add", {required: true, digits: true,minStrict: 0, maxlength: 10});
        $("#tonnage").rules("add", {required: true, number: true, minStrict: 0,maxlength: 5});
        $("#cae_ram").rules("add", {required: true, maxlength: 50});
        $("#passengers_number").rules("add", {required: true, digits: true, minStrict: 0,maxlength: 3});
    }

    function clean_totals(){
    $("#subtotal").val("");
    $("#discount").val("");
    $("#ice").val("");
    $("#iva").val("");
    $("#total").val("");
}

    function datatable_invoices(){

    $('#invoices_table').DataTable({
        "processing": true,
        "bPaginate": true,
        "bLengthChange": false,
        "bInfo": true,
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "oPaginate": {
                "sFirst": "Primera",
                "sLast": "&Uacute;ltima",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "sZeroRecords": "No se encontraron resultados",
            "sInfo": "_START_ - _END_ de _TOTAL_",
            "sInfoEmpty": "0 - 0 de 0",
            "sInfoFiltered": "(de _MAX_ en total)",
            "sSearch": "Buscar:",
            "sProcessing": '<div class="row"> <img  src="../media/img/loading.gif"></div>' +
                '<div class="row"> <h3>Procesando datos..</h3></div>'
        }
    });
    $('#invoices_table')
        .removeClass('display')
        .addClass('table table-striped table-bordered');
}



</script>
@stop