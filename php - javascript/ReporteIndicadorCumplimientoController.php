<?php
namespace UDP;

use \Auth;
use Symfony\Component\Process\Exception\ProcessTimedOutException;
use \View;
use \Redirect;
use \Response;
use \Request;
use \Input;
use \DB;
use \Reclamo;
use \Provincia;
use \Globalfunctions;
use \Empresa;
use \Corporacion;
use \Parametro;
use yajra\Datatables\Facades\Datatables;


class ReporteIndicadorCumplimientoController extends BaseController
{

    public function getIndex()
    {
        $provincias = Provincia::where('pro_estado', '=', 'ACTIVO')->orderby('pro_nombre')->lists('pro_nombre', 'pro_id');
        $provincias = (new Globalfunctions())->unshift_select_item($provincias, '- Todos -');

        $rangos_tiempo = (new Globalfunctions())->unshift_select_item(\Reclamo::$rangos_tiempo, '- Todos -');
        $estados_reclamo = (new Globalfunctions())->unshift_select_item(\Reclamo::$estados_reclamo, '- Todos -');
        $tipo_empresa = array('' => '- Todos -') + Empresa::$types_array;
        $corporaciones = array('' => '-  Todos  -') + Corporacion::where('cor_estado', 'ACTIVO')->lists('cor_nombre', 'cor_id');

        $tipos_bien_servicio = array('' => '-  Todos  -') + Reclamo::$tipos_producto;

        $cantones[''] = '- Todos -';

        $this->layout->contenido = View::make('UDP.reporte-indicadores-cumplimiento.index',
            ['provincias' => $provincias,
                'cantones' => $cantones,
                'rangos_tiempo' => $rangos_tiempo,
                'estados_reclamo' => $estados_reclamo,
                'tipos_bien_servicio' => $tipos_bien_servicio,
                'tipo_empresa' => $tipo_empresa,
                'corporaciones' => $corporaciones
            ]
        );

    }

    public function getProveedores()
    {

        $proveedores = \Empresa::where('emp_estado', '=', 'ACTIVO')
            ->whereNull('emp_padre_id')
            ->where(function ($query) {
                $query->orWhere('emp_numero_ruc', 'like', '%' . Input::get('proveedor') . '%')
                    ->orWhere('emp_razon_social', 'like', '%' . Input::get('proveedor') . '%')
                    ->orWhere('emp_numero_defensorial', 'like', '%' . Input::get('proveedor') . '%')
                    ->orWhere('emp_nombre_fantasia', 'like', '%' . Input::get('proveedor') . '%');
            });

        if (Input::get('corporacion_id'))
            $proveedores = $proveedores->where("emp_corporacion_id", "=", Input::get('corporacion_id'));

        if (Input::get('tipo_empresa'))
            $proveedores = $proveedores->where("emp_tipo_empresa", "=", Input::get('tipo_empresa'));

        $proveedores = $proveedores->skip(0)->take(10)->get();

        if (count($proveedores) > 0) {

            foreach ($proveedores as $proveedor) {
                $proveedores_lista[] = array('id' => $proveedor->emp_id, 'text' => $proveedor->emp_numero_ruc . ' - ' . $proveedor->emp_razon_social . ' - ' . $proveedor->emp_nombre_fantasia . ' - ' . $proveedor->emp_numero_defensorial);
            }

        } else {
            $proveedores_lista[] = array("id" => "", "text" => "No se encontraron resultados");
        }

        return Response::json($proveedores_lista);
    }

    public function getSucursales()
    {

        $proveedores = \Empresa::where('emp_estado', '=', 'ACTIVO')
            ->where(function($query){
                $query->where('emp_padre_id',Input::get('proveedor'))
                    ->orWhere('emp_id',Input::get('proveedor'));
            })
            ->where(function ($query){
            $query->Where('emp_calle','like','%'.Input::get('sucursal').'%')
                ->orWhere('emp_numero_defensorial', 'like', '%' . Input::get('sucursal') . '%')
                ->orWhere('emp_razon_social','like','%'.Input::get('sucursal').'%')
                ->orWhere('emp_numero','like','%'.Input::get('sucursal').'%')
                ->orWhere('emp_interseccion','like','%'.Input::get('sucursal').'%');
            })->skip(0)->take(10)->get();

        if (count($proveedores) > 0) {
            foreach ($proveedores as $proveedor) {
                $proveedores_lista[] = array('id' => $proveedor->emp_id, 'text' => $proveedor->sucursal());
            }
        } else {
            $proveedores_lista[] = array("id" => "", "text" => "No se encontraron resultados");
        }

        return Response::json($proveedores_lista);
    }


    public function getCantones($id)
    {

        if (Request::ajax()) {

            $cantones = \Canton::where("can_provincia_id", '=', $id)->orderBy('can_nombre')->lists('can_nombre', 'can_id');
            $cantones = (new Globalfunctions())->unshift_select_item($cantones, '- Todos -');
            $this->layout = NULL;

            return View::make('UDP.reporte-indicadores-cumplimiento.cantones',
                array('cantones' => $cantones
                ));
        }
    }

    public function postSeleccionarCorporacion()
    {
        if (Request::ajax()) {

            $data = Input::all();

            $corporaciones=Corporacion::where("cor_estado","=","ACTIVO");
            if($data["tipo_empresa"]){
                $corporaciones=$corporaciones->where("cor_tipo_empresa", '=', $data["tipo_empresa"]);
            }
            $corporaciones =$corporaciones->orderBy('cor_nombre')->lists('cor_nombre', 'cor_id');

            $corporaciones = array(""=>"- Todas -")+ $corporaciones;

            $this->layout = NULL;

            return View::make('UDP.carga.selectCorporacion',
                array('corporaciones' => $corporaciones
                ));
        }
    }



    public function getListaCumplimiento()
    {
        if (Request::ajax()) {
            return View::make('UDP.reporte-indicadores-cumplimiento.lista', array('parametros' => http_build_query(Input::all())));
        }
    }

    public function getDatatableCumplimiento()
    {
        if (Request::ajax()) {
            $data = Input::all();

            $canton = $data['canton'];
            $provincia = $data['provincia'];
            $sucursal = $data['sucursal'];
            $proveedor = $data['proveedor'];
            $tipo_bien_servicio = $data['tipo_bien_servicio'];
            $corporacion = $data["corporacion"];
            $tipo_empresa = $data["tipo_empresa"];
            $reclamo_desde = $data["reclamo_desde"];
            $reclamo_hasta = $data["reclamo_hasta"];
            $agrupacion= $data["agrupacion"];

            $tipo_row="";
            if($agrupacion=='SUCURSAL'){
                $tipo_row=  DB::raw("if(id_empresa_padre_asignacion IS NULL,'MATRIZ','Sucursal') as tipo");
            }else{
                $tipo_row=  DB::raw("'MATRIZ'  as tipo");
            }

            $parametro_tiempo = Parametro::obtener_parametro("tiempo_respuesta_proveedor");



            $reclamos = DB::table('reclamos_consolidados')
                ->select([
                    DB::raw("'ranking' AS ranking"),
                    DB::raw("sum(DATEDIFF(fecha_atencion,fecha_creacion)) as temporal_tiempo_atencion"),
                    DB::raw("count(DATEDIFF(fecha_atencion,fecha_creacion)) as temporal_cantidad_atencion"),
                    DB::raw("count(if( rec_reclamo_atendido ='ATENDIDO' || rec_reclamo_atendido ='ATENDIDO_PREGUNTA_ADICIONAL' || rec_reclamo_atendido ='NO_ATENDIDO_PREGUNTA_ADICIONAL' ,1,NULL)) as temporal_sin_importar_estado_atendidos"),
                    $tipo_row,
                    DB::raw("if(id_empresa_padre_creacion IS NULL,empresas.emp_corporacion_id,empresa_padre.emp_corporacion_id) as id_corporacion"),
                    DB::raw("if(id_empresa_padre_creacion IS NULL,empresas.emp_razon_social,empresa_padre.emp_razon_social) as nombre_empresa"),
                    DB::raw("count(if(rec_estado ='PENDIENTE',1,NULL))  as temporal_en_tramite_pendiente"),
                    DB::raw("count(if(rec_estado ='PENDIENTE_PREGUNTA',1,NULL))  as temporal_en_tramite_pendiente_pregunta"),
                    DB::raw("count(if(rec_estado ='RESPONDIDO',1,NULL))  as temporal_en_tramite_respondidos"),
                    DB::raw("count(if( rec_estado='CERRADO' && (rec_reclamo_atendido ='ATENDIDO' || rec_reclamo_atendido ='ATENDIDO_PREGUNTA_ADICIONAL') ,1,NULL))  as temporal_cerrados_atendidos"),
                    DB::raw("count(if( rec_estado='CERRADO' && rec_calidad_servicio ='SATISFECHO' && (rec_reclamo_atendido ='ATENDIDO' || rec_reclamo_atendido ='ATENDIDO_PREGUNTA_ADICIONAL') ,1,NULL))  as temporal_cerrados_atendidos_satisfechos"),
                    DB::raw("count(if( rec_estado='CERRADO' && rec_calidad_servicio ='INSATISFECHO' && (rec_reclamo_atendido ='ATENDIDO' || rec_reclamo_atendido ='ATENDIDO_PREGUNTA_ADICIONAL') ,1,NULL))  as temporal_cerrados_atendidos_insatisfechos"),
                    DB::raw("count(if( rec_estado='CERRADO' && rec_reclamo_atendido ='NUNCA_ATENDIDO' ,1,NULL))  as temporal_cerrados_nunca_atendidos"),
                    DB::raw("count(if( rec_estado='CERRADO' && rec_reclamo_atendido ='NO_ATENDIDO_PREGUNTA_ADICIONAL' ,1,NULL))  as temporal_cerrados_no_respodidos_pregunta_adicional"),
                    DB::raw("count( rec_id ) as total_reclamos"),
                    DB::raw("count(if(rec_redireccionado_dpe='SI',1,NULL)) as reclamos_escalados_dpe"),
                    DB::raw("count(if(rec_calidad_servicio ='SATISFECHO',1,NULL))  as temporal_calificacion_satisfecho"),
                    DB::raw("count(if(rec_calidad_servicio ='INSATISFECHO',1,NULL))  as temporal_calificacion_insatisfecho"),
                    DB::raw("count(if(rec_calidad_servicio ='SIN_CALIFICACION',1,NULL))  as temporal_cerrados_nunca_calificacion"),
                    DB::raw("`id_empresa_padre_creacion`"),
                    DB::raw("`id_empresa_creacion`"),"id_empresa_asignacion",
                    DB::raw("if(id_empresa_padre_creacion IS NULL,id_empresa_creacion,id_empresa_padre_creacion) as temporal_empresa_creacion")
                ])
                ->join('empresas', 'reclamos_consolidados.id_empresa_creacion', '=', 'empresas.emp_id')
                ->leftJoin('empresas AS empresa_padre', 'reclamos_consolidados.id_empresa_padre_creacion', '=', 'empresa_padre.emp_id')
                ->leftJoin('corporaciones', 'corporaciones.cor_id', '=', 'empresas.emp_corporacion_id')
                ->whereNotNull('rec_id');

            if($agrupacion=="PROVEEDOR") {
                $reclamos = $reclamos->groupBy(DB::raw("if(id_empresa_padre_creacion IS NULL,id_empresa_creacion,id_empresa_padre_creacion)"));
                $reclamos = $reclamos->orderBy(DB::raw("if(id_empresa_padre_creacion IS NULL,id_empresa_creacion,id_empresa_padre_creacion)"));
            }

            if($agrupacion=="SUCURSAL" || !$agrupacion) {
                $reclamos = $reclamos->groupBy(DB::raw("if(id_empresa_asignacion = (if(id_empresa_padre_creacion IS NULL,id_empresa_creacion,id_empresa_padre_creacion)),NULL,id_empresa_asignacion)"),DB::raw("if(id_empresa_padre_creacion IS NULL,id_empresa_creacion,id_empresa_padre_creacion)"));
                $reclamos = $reclamos->orderBy(DB::raw("if(id_empresa_padre_creacion IS NULL,id_empresa_creacion,id_empresa_padre_creacion)"));

                $tipo_row_order="";
                if($agrupacion=='SUCURSAL'){
                    $tipo_row_order=  DB::raw("if(id_empresa_padre_asignacion IS NULL,'MATRIZ','Sucursal') ");
                }else{
                    $tipo_row_order=  DB::raw("'MATRIZ' ");
                }

                $reclamos = $reclamos->orderBy($tipo_row_order);
            }


            //SI FILTRO CANTON
            if ($canton) {
                $reclamos =$reclamos->where("id_canton_creacion","=",$canton);
            } elseif ($provincia) {
                //SI FILTRO PROVINCIA
                $reclamos =$reclamos->where("id_provincia_creacion","=",$provincia);
            }

            //SI FILTRO CORPORACION
            if ($corporacion) {
                $reclamos = $reclamos->
                    where(function ($query) use ($corporacion) {
                        $query->where('empresas.emp_corporacion_id','=',$corporacion )
                            ->orWhere('empresa_padre.emp_corporacion_id', '=', $corporacion );
                    });
            }

            //SI FILTRO TIPO EMPRESA
            if ($tipo_empresa) {
                $reclamos = $reclamos->
                    where(function ($query) use ($tipo_empresa) {
                        $query->where('empresas.emp_tipo_empresa','=',$tipo_empresa )
                            ->orWhere('empresa_padre.emp_tipo_empresa', '=', $tipo_empresa );
                    });
            }

            $reclamos = $this->_manejar_query_fechas_datatable($reclamos,$reclamo_desde,$reclamo_hasta,'fecha_creacion');


            if ($tipo_bien_servicio) {

                $reclamos =$reclamos->where("rec_tipo_producto", "=", $tipo_bien_servicio);
            }


            if($agrupacion=="PROVEEDOR"){
                if ($proveedor) {
                    //SI FILTRO PROVEEDOR
                    $reclamos =$reclamos->where(DB::raw("if(id_empresa_padre_creacion IS NULL,id_empresa_creacion,id_empresa_padre_creacion)"),"=",$proveedor);
                }
            }

            if($agrupacion=="SUCURSAL" || !$agrupacion){

                //SI FILTRO SUCURSAL
                if ($sucursal) {
                  //  $reclamos =$reclamos->where("id_empresa_creacion","=",$sucursal);
                    $sucursal_es_matriz= Empresa::find($sucursal);
                    if($sucursal_es_matriz->emp_padre_id){
                        $reclamos = $reclamos->where('id_empresa_asignacion', (int)$sucursal);
                    }else{
                        $reclamos = $reclamos->where(function($query) use ($sucursal){
                            $query->whereNull('id_empresa_asignacion')
                                ->where(function($query_b) use ($sucursal){
                                    $query_b->where('id_empresa_creacion',"=",$sucursal)
                                        ->orWhere('id_empresa_padre_creacion',"=",$sucursal);
                                });
                        });
                    }
                }
                if ($proveedor) {
                    //SI FILTRO PROVEEDOR
                    $reclamos =$reclamos->where(DB::raw("if(id_empresa_padre_creacion IS NULL,id_empresa_creacion,id_empresa_padre_creacion)"),"=",$proveedor);
                }

            }

            if($agrupacion=="CORPORACION"){
                $reclamos =$reclamos->whereNotNull("empresas.emp_corporacion_id")
                       ->groupBy("empresas.emp_corporacion_id")
                       ->orderBy("empresas.emp_corporacion_id");
            }


            $dt_reclamos = Datatables::of($reclamos);

            $dt_reclamos->editColumn('ranking', function ($model) use($parametro_tiempo) {
                $ranking=0;
                $primer_parametro_evaluacion=0;
                $segundo_parametro_evaluacion=0;
                $tercer_parametro_evaluacion=0;
                $ranking_porcentaje=0;
                if($model->temporal_tiempo_atencion==='0' || $model->temporal_tiempo_atencion  ){
                    $divisor_p1=$model->temporal_tiempo_atencion;
                    $dividendo_p1=$model->temporal_sin_importar_estado_atendidos + $model->temporal_calificacion_satisfecho + $model->temporal_calificacion_insatisfecho +$model->temporal_en_tramite_pendiente_pregunta;
                    if($dividendo_p1)
                        $primer_parametro_evaluacion=1-(($divisor_p1/$dividendo_p1 )/$parametro_tiempo);
                    else
                        $primer_parametro_evaluacion=0;


                    $divisor_p2=$model->temporal_calificacion_satisfecho;
                    $divindendo_p2=$model->temporal_calificacion_satisfecho+$model->temporal_calificacion_insatisfecho;

                    if($divindendo_p2)
                        $segundo_parametro_evaluacion=$divisor_p2/$divindendo_p2;
                    else
                        $segundo_parametro_evaluacion=0;

                    $divisor_p3=$model->temporal_cerrados_nunca_atendidos + $model->temporal_cerrados_no_respodidos_pregunta_adicional;
                    $divindendo_p3=$model->temporal_cerrados_atendidos+$model->temporal_cerrados_nunca_atendidos+$model->temporal_cerrados_no_respodidos_pregunta_adicional;

                    if($divindendo_p3)
                        $tercer_parametro_evaluacion=$divisor_p3/$divindendo_p3;
                    else
                        $tercer_parametro_evaluacion=0;

                    $ranking=($primer_parametro_evaluacion+$segundo_parametro_evaluacion)-$tercer_parametro_evaluacion;
                    $ranking_porcentaje= ($ranking*100)/2;
                }
                if($ranking_porcentaje>=0){
                    return number_format((float)$ranking_porcentaje,2,".",'') . ' %';
                }else{
                    return "0.00 %";
                }


            });

            $dt_reclamos->editColumn('temporal_tiempo_atencion', function ($model) use($parametro_tiempo) {
                return ($model->temporal_tiempo_atencion==='0' ||$model->temporal_tiempo_atencion>0)?number_format(($model->temporal_tiempo_atencion/$model->temporal_cantidad_atencion),2,'.',''):"";
            });

            $dt_reclamos->editColumn('pro_cor_nombre', function ($model) {
                return $model->id_corporacion ? Corporacion::find($model->id_corporacion)->cor_nombre : "";
            });

            if($agrupacion=='SUCURSAL'){
                $dt_reclamos->editColumn('id_empresa_creacion', function ($model) {
                    if($model->id_empresa_asignacion){
                        return Empresa::find($model->id_empresa_asignacion)->direccion();
                    }else{
                        if($model->id_empresa_padre_creacion)
                            return Empresa::find($model->id_empresa_padre_creacion)->direccion();
                        else
                            return Empresa::find($model->id_empresa_creacion)->direccion();
                    }

                });
            }else{
                $dt_reclamos->editColumn('id_empresa_creacion', function ($model) {
                    return $model->id_empresa_padre_creacion ? Empresa::find($model->id_empresa_padre_creacion)->direccion() : Empresa::find($model->id_empresa_creacion)->direccion();
                });
            }
            return $dt_reclamos->make(TRUE);
        }
    }

    private function _manejar_query_fechas_datatable ($reclamos,$fecha_inicio,$fecha_fin,$campo_inicio) {

        if ($fecha_inicio and $fecha_fin ) {
            $reclamo_desde = date_create_from_format('Y-m-d H:i:s', $fecha_inicio . ' 00:00:00')->format('Y-m-d H:i:s');
            $reclamo_hasta = date_create_from_format('Y-m-d H:i:s', $fecha_fin . ' 23:59:59')->format('Y-m-d H:i:s');

            return $reclamos->where($campo_inicio, '>=', $reclamo_desde)->where($campo_inicio, '<=', $reclamo_hasta);
        }elseif($fecha_inicio and !$fecha_fin){
            $reclamo_desde = date_create_from_format('Y-m-d H:i:s', $fecha_inicio . ' 00:00:00')->format('Y-m-d H:i:s');

            return $reclamos->where($campo_inicio, '>=', $reclamo_desde);
        }elseif(!$fecha_inicio and $fecha_fin){
            $reclamo_hasta = date_create_from_format('Y-m-d H:i:s', $fecha_fin . ' 23:59:59')->format('Y-m-d H:i:s');

            return $reclamos->where($campo_inicio, '<=', $reclamo_hasta);
        }else
            return $reclamos;
    }

    private function _consultar_reclamos($data)
    {
        try {
            $canton = $data['canton'];
            $provincia = $data['provincia'];
            $sucursal = $data['sucursal'];
            $proveedor = $data['proveedor'];
            $tipo_bien_servicio = $data['tipo_bien_servicio'];
            $corporacion = $data["corporacion"];
            $tipo_empresa = $data["tipo_empresa"];
            $reclamo_desde = $data["reclamo_desde"];
            $reclamo_hasta = $data["reclamo_hasta"];
            $agrupacion= $data["agrupacion"];

            $filtros = array();

            if($data['provincia']){
                $prov = Provincia::find($data['provincia']);
                $filtros['Provincia'] = $prov->pro_nombre;
                if($canton){
                    $can = \Canton::find($canton);
                    $filtros['Cant&oacute;n'] = htmlentities($can->can_nombre);
                }
            }
            if($sucursal){
                $emp = Empresa::find($sucursal);
                $filtros['Sucursal/Direcci&oacute;n'] = $emp->sucursal();
            }elseif($proveedor){
                $emp = Empresa::find($data['proveedor']);
                $filtros['Proveedor/Prestador'] = $emp->emp_razon_social;
            }
            if($tipo_bien_servicio){
                $filtros['Tipo Bien/Servicio'] = $data['tipo_bien_servicio'] == 'BIEN' ? 'Bien' : ($data['tipo_bien_servicio'] == 'SERVICIO_PRIVADO' ? 'Servicio Privado' : 'Servicio P&uacute;blico');
            }
            if($corporacion){
                $cor = \Corporacion::find($data['corporacion']);
                $filtros['Corporaci&oacute;n'] = htmlentities($cor->cor_nombre);
            }
            if($tipo_empresa){
                $filtros['Tipo de empresa'] = $data['tipo_empresa'] == 'PRIVADO' ? 'Privada' : 'P&uacute;blica';
            }
            if($reclamo_desde){
                $filtros['Fecha de reclamo desde'] = $data['reclamo_desde'];
            }
            if($reclamo_hasta){
                $filtros['Fecha de reclamo hasta'] = $data['reclamo_hasta'];
            }
            $filtros['Agrupar por'] = $agrupacion;


                $reclamos_resultado = $this->reclamos_agrupacion_proveedor($agrupacion,$corporacion, $proveedor,$sucursal,$provincia, $canton, $reclamo_desde, $reclamo_hasta, $tipo_bien_servicio, $tipo_empresa);

            return array('reclamos'=>$reclamos_resultado,'filtros'=>$filtros);
        } catch (\Exception $e) {
           return false;
        }
    }

    private function _manejar_query_fechas($fecha_inicio, $fecha_fin, $campo_inicio)
    {

        $fecha_inicio= ($fecha_inicio)?$fecha_inicio." 00:00:00":"";
        $fecha_fin=  ($fecha_fin)?$fecha_fin." 00:00:00":"";

        if ($fecha_inicio && $fecha_fin) {

            return  $campo_inicio . " >= '" . $fecha_inicio . "' and " . $campo_inicio . " <= '" . $fecha_fin . "' ";
        } elseif ($fecha_inicio && !$fecha_fin) {

            return  $campo_inicio . " >= '" . $fecha_inicio . "' ";
        } elseif (!$fecha_inicio && $fecha_fin) {

            return $campo_inicio . " <= '" . $fecha_fin . "' ";
        } else
            return "";
    }

    private function reclamos_agrupacion_proveedor($agrupacion,$corporacion, $proveedor, $sucursal,$provincia, $canton, $reclamo_desde, $reclamo_hasta, $tipo_bien_servicio, $tipo_empresa)
    {

        $array_params = array();


        $tipo_row="";
        if($agrupacion=='SUCURSAL'){
            $tipo_row=  DB::raw("if(id_empresa_padre_asignacion IS NULL,'MATRIZ','Sucursal') as tipo");
            $agrupacion_row= DB::raw("'SUCURSAL' as agrupacion");
        }else{
            $tipo_row=  DB::raw("'MATRIZ'  as tipo");
            $agrupacion_row= DB::raw("'PROVEEDOR' as agrupacion");
        }

        $parametro_tiempo = Parametro::obtener_parametro("tiempo_respuesta_proveedor");



        $reclamos = DB::table('reclamos_consolidados')
            ->select([
                DB::raw("'ranking' AS ranking"),
                DB::raw("sum(DATEDIFF(fecha_atencion,fecha_creacion)) as temporal_tiempo_atencion"),
                DB::raw("count(DATEDIFF(fecha_atencion,fecha_creacion)) as temporal_cantidad_atencion"),
                DB::raw("count(if( rec_reclamo_atendido ='ATENDIDO' || rec_reclamo_atendido ='ATENDIDO_PREGUNTA_ADICIONAL' || rec_reclamo_atendido ='NO_ATENDIDO_PREGUNTA_ADICIONAL' ,1,NULL)) as temporal_sin_importar_estado_atendidos"),
                $tipo_row,
                $agrupacion_row,
                DB::raw("if(id_empresa_padre_creacion IS NULL,empresas.emp_corporacion_id,empresa_padre.emp_corporacion_id) as id_corporacion"),
                DB::raw("if(id_empresa_padre_creacion IS NULL,empresas.emp_razon_social,empresa_padre.emp_razon_social) as nombre_empresa"),
                DB::raw("count(if(rec_estado ='PENDIENTE',1,NULL))  as temporal_en_tramite_pendiente"),
                DB::raw("count(if(rec_estado ='PENDIENTE_PREGUNTA',1,NULL))  as temporal_en_tramite_pendiente_pregunta"),
                DB::raw("count(if(rec_estado ='RESPONDIDO',1,NULL))  as temporal_en_tramite_respondidos"),
                DB::raw("count(if( rec_estado='CERRADO' && (rec_reclamo_atendido ='ATENDIDO' || rec_reclamo_atendido ='ATENDIDO_PREGUNTA_ADICIONAL') ,1,NULL))  as temporal_cerrados_atendidos"),
                DB::raw("count(if( rec_estado='CERRADO' && rec_calidad_servicio ='SATISFECHO' && (rec_reclamo_atendido ='ATENDIDO' || rec_reclamo_atendido ='ATENDIDO_PREGUNTA_ADICIONAL') ,1,NULL))  as temporal_cerrados_atendidos_satisfechos"),
                DB::raw("count(if( rec_estado='CERRADO' && rec_calidad_servicio ='INSATISFECHO' && (rec_reclamo_atendido ='ATENDIDO' || rec_reclamo_atendido ='ATENDIDO_PREGUNTA_ADICIONAL') ,1,NULL))  as temporal_cerrados_atendidos_insatisfechos"),
                DB::raw("count(if( rec_estado='CERRADO' && rec_reclamo_atendido ='NUNCA_ATENDIDO' ,1,NULL))  as temporal_cerrados_nunca_atendidos"),
                DB::raw("count(if( rec_estado='CERRADO' && rec_reclamo_atendido ='NO_ATENDIDO_PREGUNTA_ADICIONAL' ,1,NULL))  as temporal_cerrados_no_respodidos_pregunta_adicional"),
                DB::raw("count( rec_id ) as total_reclamos"),
                DB::raw("count(if(rec_redireccionado_dpe='SI',1,NULL)) as reclamos_escalados_dpe"),
                DB::raw("count(if(rec_calidad_servicio ='SATISFECHO',1,NULL))  as temporal_calificacion_satisfecho"),
                DB::raw("count(if(rec_calidad_servicio ='INSATISFECHO',1,NULL))  as temporal_calificacion_insatisfecho"),
                DB::raw("count(if(rec_calidad_servicio ='SIN_CALIFICACION',1,NULL))  as temporal_cerrados_nunca_calificacion"),
                DB::raw("`id_empresa_padre_creacion`"),
                DB::raw("`id_empresa_creacion`"),"id_empresa_asignacion",
                DB::raw("if(id_empresa_padre_creacion IS NULL,id_empresa_creacion,id_empresa_padre_creacion) as temporal_empresa_creacion")
            ])
            ->join('empresas', 'reclamos_consolidados.id_empresa_creacion', '=', 'empresas.emp_id')
            ->leftJoin('empresas AS empresa_padre', 'reclamos_consolidados.id_empresa_padre_creacion', '=', 'empresa_padre.emp_id')
            ->leftJoin('corporaciones', 'corporaciones.cor_id', '=', 'empresas.emp_corporacion_id')
            ->whereNotNull('rec_id');

        if($agrupacion=="PROVEEDOR") {
            $reclamos = $reclamos->groupBy(DB::raw("if(id_empresa_padre_creacion IS NULL,id_empresa_creacion,id_empresa_padre_creacion)"));
            $reclamos = $reclamos->orderBy(DB::raw("if(id_empresa_padre_creacion IS NULL,id_empresa_creacion,id_empresa_padre_creacion)"));
        }

        if($agrupacion=="SUCURSAL" || !$agrupacion) {
            $reclamos = $reclamos->groupBy(DB::raw("if(id_empresa_asignacion = (if(id_empresa_padre_creacion IS NULL,id_empresa_creacion,id_empresa_padre_creacion)),NULL,id_empresa_asignacion)"),DB::raw("if(id_empresa_padre_creacion IS NULL,id_empresa_creacion,id_empresa_padre_creacion)"));
            $reclamos = $reclamos->orderBy(DB::raw("if(id_empresa_padre_creacion IS NULL,id_empresa_creacion,id_empresa_padre_creacion)"));
            $reclamos = $reclamos->orderBy("tipo");
        }


        //SI FILTRO CANTON
        if ($canton) {
            $reclamos =$reclamos->where("id_canton_creacion","=",$canton);
        } elseif ($provincia) {
            //SI FILTRO PROVINCIA
            $reclamos =$reclamos->where("id_provincia_creacion","=",$provincia);
        }

        //SI FILTRO CORPORACION
        if ($corporacion) {
            $reclamos = $reclamos->
                where(function ($query) use ($corporacion) {
                    $query->where('empresas.emp_corporacion_id','=',$corporacion )
                        ->orWhere('empresa_padre.emp_corporacion_id', '=', $corporacion );
                });
        }

        //SI FILTRO TIPO EMPRESA
        if ($tipo_empresa) {
            $reclamos = $reclamos->
                where(function ($query) use ($tipo_empresa) {
                    $query->where('empresas.emp_tipo_empresa','=',$tipo_empresa )
                        ->orWhere('empresa_padre.emp_tipo_empresa', '=', $tipo_empresa );
                });
        }

        $reclamos = $this->_manejar_query_fechas_datatable($reclamos,$reclamo_desde,$reclamo_hasta,'fecha_creacion');


        if ($tipo_bien_servicio) {

            $reclamos =$reclamos->where("rec_tipo_producto", "=", $tipo_bien_servicio);
        }


        if($agrupacion=="PROVEEDOR"){
            if ($proveedor) {
                //SI FILTRO PROVEEDOR
                $reclamos =$reclamos->where(DB::raw("if(id_empresa_padre_creacion IS NULL,id_empresa_creacion,id_empresa_padre_creacion)"),"=",$proveedor);
            }
        }

        if($agrupacion=="SUCURSAL" || !$agrupacion){

            //SI FILTRO SUCURSAL
            if ($sucursal) {
                //  $reclamos =$reclamos->where("id_empresa_creacion","=",$sucursal);
                $sucursal_es_matriz= Empresa::find($sucursal);
                if($sucursal_es_matriz->emp_padre_id){
                    $reclamos = $reclamos->where('id_empresa_asignacion', (int)$sucursal);
                }else{
                    $reclamos = $reclamos->where(function($query) use ($sucursal){
                        $query->whereNull('id_empresa_asignacion')
                            ->where(function($query_b) use ($sucursal){
                                $query_b->where('id_empresa_creacion',"=",$sucursal)
                                    ->orWhere('id_empresa_padre_creacion',"=",$sucursal);
                            });
                    });
                }
            }
            if ($proveedor) {
                //SI FILTRO PROVEEDOR
                $reclamos =$reclamos->where(DB::raw("if(id_empresa_padre_creacion IS NULL,id_empresa_creacion,id_empresa_padre_creacion)"),"=",$proveedor);
            }

        }

        if($agrupacion=="CORPORACION"){
            $reclamos =$reclamos->whereNotNull("empresas.emp_corporacion_id")
                ->groupBy("empresas.emp_corporacion_id")
                ->orderBy("empresas.emp_corporacion_id");
        }

        $reclamos= $reclamos->get();

        return $reclamos;
    }


    public function getExportar()
    {
        $data = Input::all();
        $lista_reclamos = $this->_consultar_reclamos($data);
        $parametro_tiempo= Parametro::obtener_parametro("tiempo_respuesta_proveedor");
        $headers=array(
            'Content-type'=>'application/vnd.ms-excel',
            'Content-Disposition'=>'attachment; filename=cumplimiento.xls',
            'Pragma'=>'no-cache',
            'Expires'=>'0',
        );
        set_time_limit(36000);
        ini_set('memory_limit', '512M');
        $view= View::make('UDP.reporte-indicadores-cumplimiento.lista_exportar', array('reclamos' => $lista_reclamos['reclamos'],'filtros'=>$lista_reclamos['filtros'],'parametro_tiempo'=>$parametro_tiempo));
        return Response::make($view, 200, $headers);
    }

}
